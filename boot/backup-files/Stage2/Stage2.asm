
;*******************************************************
;
;	Stage2.asm
;		Stage2 Bootloader
;
;	OS Development Series
;*******************************************************

;bits	16 ; fix
;org 0x7e00
; Remember the memory map-- 0x500 through 0x7bff is unused above the BIOS data area.
; We are loaded at 0x500 (0x50:0)

;org 0x0000 ; fix

;jmp	main				; go to start
jmp EnterProtectedMode
;*******************************************************
;	Preprocessor directives
;*******************************************************

;%include "stdio.inc"			; basic i/o routines
%include "Gdt.asm"			; Gdt routines
;%include "A20.asm"

;*******************************************************
;	Data Section
;*******************************************************

LoadingMsg db "Preparing to load operating system...", 0x0D, 0x0A, 0x00

;*******************************************************
;	STAGE 2 ENTRY POINT
;
;		-Store BIOS information
;		-Load Kernel
;		-Install GDT; go into protected mode (pmode)
;		-Jump to Stage 3
;*******************************************************




EnterProtectedMode:
	call EnableA20
	cli ; disable interrupt
	lgdt [gdt_descriptor]
	mov eax, cr0
	or eax, 1
	mov cr0, eax

	jmp CODE_SEG:StartProtectedMode ; jmp far jump
	
	
; need to search the meaning of this function
EnableA20:
	in al, 0x92
	or al, 2
	out 0x92, al
	ret


[bits 32]

%include "CPUID.asm"
%include "SimplePaging.asm"

StartProtectedMode:
	mov ax, DATA_SEG
	mov ds, ax
	mov ss, ax
	mov es, ax
	mov fs, ax
	mov gs, ax

	;mov [0xb8000], byte 'b'
	
	; mov [0xb8004], byte 'l'
	; mov [0xb8006], byte 'l'
	; mov [0xb8008], byte 'o'
	; mov [0xb800a], byte ' '
	; mov [0xb800c], byte 'W'
	; mov [0xb800e], byte 'o'
	; mov [0xb8010], byte 'r'
	; mov [0xb8012], byte 'l'
	; mov [0xb8014], byte 'd'

	call DetectCPUID
	mov [0xb8002], byte 'e'
	call DetectLongMode
	mov [0xb8004], byte 'l'
	call SetUpIdentityPaging
	mov [0xb8006], byte 'l'
	call EditGDT
	mov [0xb8008], byte 'o'
	jmp CODE_SEG:Start64Bit

[bits 64]
[extern _start]

;%include "IDT.asm"

Start64Bit:
	mov edi, 0xb8000 ; start address of video memory vector 
	mov rax, 0x1f201f201f201f20
	mov ecx, 1000
	mov [0xb800a], byte 'D'
	
	;draw the screen in blue color
	; .ChnageColor:
        ; mov QWORD [edi], rax 
        ; add edi, 8
		; dec ecx
        ; loop .ChnageColor
	call _start
	jmp $
times 2048-($-$$) db 0