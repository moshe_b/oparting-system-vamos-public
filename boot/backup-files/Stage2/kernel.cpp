
#include "vga_codes.h"

#define VGA_MAMORY (unsigned char*)0xb8000
#define VGA_WIDTH 80
#define STR "hello from kernel"

void printString(const char* str);
void clearScreen(unsigned long long clearColor);
void write_string(int colour, const char *string);

extern "C" void _start() {
	char* vga_p = (char*)0xb8000;
	//*vga_p = 'I';
	//clearScreen(BACKGROUND_BLACK | FORGROUND_WHITE);
	char* s = "Tello from kernel";
	//printString(s);
	printString(s);
	*vga_p = 'M';
	*(vga_p + 2) = (*s);
	printString(s);
	return;
}


void write_string(int colour, const char *string)
{
	volatile char *video = (volatile char*)0xB8000;
	while (*string != 0)
	{
		*video++ = *string++;
		*video++ = colour;
	}
}

void clearScreen(unsigned long long clearColor = BACKGROUND_BLACK | FORGROUND_WHITE)
{
	unsigned long long value = 0;
	value += clearColor << 8;
	value += clearColor << 24;
	value += clearColor << 40;
	value += clearColor << 56;

	for (unsigned long long* i = (unsigned long long*)VGA_MAMORY; i < (unsigned long long*)(VGA_MAMORY + 4000); i++)
	{
		*i = value;
	}
}

void printString(const char* str)
{
	unsigned char* charPtr = (unsigned char*)str;
	unsigned short index = 0; //for moving the cursor with the text
//	unsigned char* vga_p = (unsigned char*)0xb8000;
	//	*(VGA_MAMORY) = 'f';

	//*(VGA_MAMORY + 1) = BACKGROUND_BLACK | FORGROUND_WHITE;
	//*(vga_p + 1) = 0x00 | 0X0F;

	while (index < 10)
	{
		*(VGA_MAMORY + index * 2) = str[index]; //double by two because in vga format is character and is font formating
		unsigned char color = BACKGROUND_BLACK | FORGROUND_WHITE;
		*(VGA_MAMORY + index * 2 + 1) = color;
	
		index++;
		//charPtr++;
	}

}