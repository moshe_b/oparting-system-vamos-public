nasm ..\bootloader.asm -f bin -o bootloader.bin

nasm Stage2.asm -f elf64 -o Stage2.o

wsl $WSLENV/x86_64-elf-gcc -Ttext 0x8000 -ffreestanding -mno-red-zone -m64 -c "kernel.cpp" -o "kernel.o"

wsl $WSLENV/x86_64-elf-ld -T"link.ld"

copy /b bootloader.bin+kernel.bin bootloader2.flp
::nasm Stage2.asm -f elf64 -o Stage2.o

::wsl $WSLENV/x86_64-elf-gcc -Ttext 0x8000 -ffreestanding -c "kernel.cpp" -o "kernel.o"

::wsl $WSLENV/x86_64-elf-ld -T"link.ld"

pause