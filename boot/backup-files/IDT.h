#ifndef IDT_H
#define IDT_H

#include "Typedefs.h"

#define MAX_INT_HANDLE_NUM 256

#define IDT_DESC_BIT16		0x06	//00000110
#define IDT_DESC_BIT32		0x0E	//00001110
#define IDT_DESC_RING1		0x40	//01000000
#define IDT_DESC_RING2		0x20	//00100000
#define IDT_DESC_RING3		0x60	//01100000
#define IDT_DESC_PRESENT		0x80	//10000000

typedef void(*I86_IRQ_HANDLER)(void);
void setvect(int intno, uint_64 routine);

typedef struct IDT64{
	
	uint_16 offset_low;
	uint_16 selector;
	uint_8  ist;
	uint_8  types_attr;
	uint_16 offset_mid;
	uint_32 offset_high;
	uint_32 zero;
} IDT64;

void initializeIDT();
void isr1_handler();

const char* hexToString(uint_8 value);
#endif
