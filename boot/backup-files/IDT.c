#include "IDT.h"
#include "inputOutput.h"

//IMPORTANT : IN LINKER WE CAN FIND THE ADDR IN 
//.idt BLOCK(0x1000) : ALIGN(0x1000) -> _idt
extern IDT64 _idt[MAX_INT_HANDLE_NUM];
extern uint_64 isr1;
extern uint_64 isr2;
extern LoadIDT();


//KEYBOARD IO FUNCTIONS **END**

char hexToStringOutput[128];

#define tokenpaster(isr_number) isr##(isr_number)

int install_isr(uint_32 i, uint_16 flags, uint_16 sel, uint_64 irq) {

	if (i > MAX_INT_HANDLE_NUM)
		return 0;

	if (!irq)
		return 0;

	//! get base address of interrupt handler
//	uint_64 uiBase = (uint_64)&(*irq);

	//! store base address into idt
	_idt[i].zero = 0;
	_idt[i].offset_low = (uint_16)(((uint_64)&isr2 & 0x000000000000ffff));
	_idt[i].offset_mid = (uint_16)(((uint_64)&isr2 & 0x00000000ffff0000) >> 16);
	_idt[i].offset_high = (uint_32)(((uint_64)&isr2 & 0xffffffff00000000) >> 32);
	_idt[i].ist = 0;
	_idt[i].selector = sel;
	_idt[i].types_attr = flags;

	return	0;
}


void setvect(int intno, uint_64 routine) {

	//! install interrupt handler! This overwrites prev interrupt descriptor
	install_isr(intno, IDT_DESC_PRESENT | IDT_DESC_BIT32,
		0x8, routine);
}


const char* hexToString(uint_8 value) {
	uint_8* valPtr = &value;
	uint_8* ptr;
	uint_8 temp;
	uint_8 size = (sizeof(uint_8) * 2 - 1);
	uint_8 i;
	for (i = 0; i < size; i++)
	{
		ptr = ((uint_8*)valPtr + i);
		temp = ((*ptr & 0xF0) >> 4);
		hexToStringOutput[size - (i * 2 + 1)] = temp + (temp > 9 ? 55 : 48);
		temp = ((*ptr & 0x0F));
		hexToStringOutput[size - (i * 2 + 0)] = temp + (temp > 9 ? 55 : 48);
	}

	hexToStringOutput[size + 1] = 0;
	return hexToStringOutput;
}
void initializeIDT()
{
	for(uint_64 INT_INDEX = 0; INT_INDEX < MAX_INT_HANDLE_NUM; INT_INDEX++)
	{
		_idt[INT_INDEX].zero = 0;
		_idt[INT_INDEX].offset_low = (uint_16)(((uint_64)&isr1 & 0x000000000000ffff));
		_idt[INT_INDEX].offset_mid = (uint_16)(((uint_64)&isr1 & 0x00000000ffff0000) >> 16);
		_idt[INT_INDEX].offset_high = (uint_32)(((uint_64)&isr1 & 0xffffffff00000000) >> 32);
		_idt[INT_INDEX].ist = 0;
		_idt[INT_INDEX].selector = 0x08;
		_idt[INT_INDEX].types_attr = 0x8e;
	}
	outb(0x21, 0xfd);
	outb(0xa1, 0xff);
	LoadIDT();
	
}

static int old_val = 0;
void isr1_handler()
{
	/*int val = inb(0x60);
	if (old_val != val)
	{
		printString(hexToString(val));
		outb(0x20, 0x20);
		outb(0xa0, 0x20);
		old_val = val;
	}*/
	
}