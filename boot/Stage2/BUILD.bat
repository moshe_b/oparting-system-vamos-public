nasm src/Stage2ForKernel.asm -f elf32 -o ObjectFiles/Stage2ForKernel.o

nasm src/IDT.asm -f elf32 -o ObjectFiles/IDT_assembly.o

nasm src/user.asm -f elf32 -o ObjectFiles/user_assembly.o

nasm src/tss.asm -f elf32 -o ObjectFiles/tss_assembly.o


wsl /usr/local/xi686-elfgcc/bin/i686-elf-gcc -Ttext 0x00100000 -ffreestanding -mno-red-zone -m32 -c "src/inputOutput.c" -o "ObjectFiles/inputOutput.o"

wsl /usr/local/xi686-elfgcc/bin/i686-elf-gcc -Ttext 0x00100000 -ffreestanding -mno-red-zone -m32 -c "src/pic.c" -o "ObjectFiles/pic.o"

wsl /usr/local/xi686-elfgcc/bin/i686-elf-gcc -Ttext 0x00100000 -ffreestanding -mno-red-zone -m32 -c "src/pit.c" -o "ObjectFiles/pit.o"

wsl /usr/local/xi686-elfgcc/bin/i686-elf-gcc -Ttext 0x00100000 -ffreestanding -mno-red-zone -m32 -c "src/IDT.c" -o "ObjectFiles/IDT.o"

wsl /usr/local/xi686-elfgcc/bin/i686-elf-gcc -Ttext 0x00100000 -ffreestanding -mno-red-zone -m32 -c "src/gdt.c" -o "ObjectFiles/gdt.o"

wsl /usr/local/xi686-elfgcc/bin/i686-elf-gcc -Ttext 0x00100000 -ffreestanding -mno-red-zone -m32 -c "src/kernel.c" -o "ObjectFiles/kernel.o"

wsl /usr/local/xi686-elfgcc/bin/i686-elf-gcc -Ttext 0x00100000 -ffreestanding -mno-red-zone -m32 -c "src/vga.c" -o "ObjectFiles/vga.o"

wsl /usr/local/xi686-elfgcc/bin/i686-elf-gcc -Ttext 0x00100000 -ffreestanding -mno-red-zone -m32 -c "src/keyboard.c" -o "ObjectFiles/keyboard.o"

wsl /usr/local/xi686-elfgcc/bin/i686-elf-gcc -Ttext 0x00100000 -ffreestanding -mno-red-zone -m32 -c "src/sound.c" -o "ObjectFiles/sound.o"

wsl /usr/local/xi686-elfgcc/bin/i686-elf-gcc -Ttext 0x00100000 -ffreestanding -mno-red-zone -m32 -c "src/physicalMemory.c" -o "ObjectFiles/PhysicalMemory.o"

wsl /usr/local/xi686-elfgcc/bin/i686-elf-gcc -Ttext 0x00100000 -ffreestanding -mno-red-zone -m32 -c "src/string.c" -o "ObjectFiles/string.o"

wsl /usr/local/xi686-elfgcc/bin/i686-elf-gcc -Ttext 0x00100000 -ffreestanding -mno-red-zone -m32 -c "src/pte.c" -o "ObjectFiles/pte.o"

wsl /usr/local/xi686-elfgcc/bin/i686-elf-gcc -Ttext 0x00100000 -ffreestanding -mno-red-zone -m32 -c "src/pde.c" -o "ObjectFiles/pde.o"

wsl /usr/local/xi686-elfgcc/bin/i686-elf-gcc -Ttext 0x00100000 -ffreestanding -mno-red-zone -m32 -c "src/vmm.c" -o "ObjectFiles/vmm.o"

wsl /usr/local/xi686-elfgcc/bin/i686-elf-gcc -Ttext 0x00100000 -ffreestanding -mno-red-zone -m32 -c "src/disk.c" -o "ObjectFiles/disk.o"

wsl /usr/local/xi686-elfgcc/bin/i686-elf-gcc -Ttext 0x00100000 -ffreestanding -mno-red-zone -m32 -c "src/fs.c" -o "ObjectFiles/fs.o"

wsl /usr/local/xi686-elfgcc/bin/i686-elf-gcc -Ttext 0x00100000 -ffreestanding -mno-red-zone -m32 -c "src/mouse.c" -o "ObjectFiles/mouse.o"

wsl /usr/local/xi686-elfgcc/bin/i686-elf-gcc -Ttext 0x00100000 -ffreestanding -mno-red-zone -m32 -c "src/timeDate.c" -o "ObjectFiles/timeDate.o"

wsl /usr/local/xi686-elfgcc/bin/i686-elf-gcc -Ttext 0x00100000 -ffreestanding -mno-red-zone -m32 -c "src/user.c" -o "ObjectFiles/user.o"

wsl /usr/local/xi686-elfgcc/bin/i686-elf-gcc -Ttext 0x00100000 -ffreestanding -mno-red-zone -m32 -c "src/tss.c" -o "ObjectFiles/tss.o"

wsl /usr/local/xi686-elfgcc/bin/i686-elf-ld -T"./link.ld"

pause
