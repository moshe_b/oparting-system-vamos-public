#ifndef STRING_H
#define STRING_H

#include "Typedefs.h"
#include "string.h"
#include "vga.h"
#define NPOS -1

uint_32 digit_count(int num);
//strings
int strncmp(char* s1, char* s2, uint_32 n);
int isEmptyString(const char* s);
int strCmp(const char* s1, const char* s2);
uint_32 strlen(const char* str);
void strcpy(char* dest, const char* source);
void reverse(char str[], int length);
char* itoa(int num, char* str, int base);
void* memset(void* s, int c, uint_32 count);
int find(char* s, char chr);
void strncpy(char* s1, char* s2, int n);
int isspace(char ch);
int findFirstNotSpace(char* str);
int strcmp(char* str1, char* str2);
int atoi(char* strNum);
void rtrim(char* str);

#endif // !STRING_H
