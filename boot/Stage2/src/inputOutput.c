#include "inputOutput.h"

void outb(uint_16 port, uint_8 val)
{
	asm volatile ("outb %0, %1" : : "a"(val), "Nd"(port));
}

uint_8 inb(uint_16 port)
{
	uint_8 returnVal;
	asm volatile ("inb %1, %0" : "=a"(returnVal) : "Nd"(port));
	return returnVal;
}

void outw(uint_16 port, uint_16 val)
{
	asm volatile ("outw %0, %1" : : "a"(val), "Nd"(port));
}

uint_16 inw(uint_16 port)
{
	uint_16 returnVal;
	asm volatile ("inw %1, %0" : "=a"(returnVal) : "Nd"(port));
	return returnVal;
}

