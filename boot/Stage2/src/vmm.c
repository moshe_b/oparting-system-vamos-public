#include "vmm.h"

pdirectory* m_current_directory = 0;

ptable first_page_table;
int vmm_allocate_page(pt_entry* e)
{
	void* address = pmmngr_alloc_block();
	if (address == 0)
		return 0; // failed to allocate block of memory

	pt_entry_set_frame(e, (physical_addr)address);
	pt_entry_add_attrib(e, PTE_PRESENT_FLAG); 
	return 1;
}

void vmm_free_page(pt_entry* e)
{
	physical_addr* address = pt_entry_pfn(*e); // get physical address of page

	if(address) // the page tabke entry may not exist yet
		pmmngr_free_block((void*)address);

	pt_entry_del_attrib(e, PTE_PRESENT_FLAG);
}

pt_entry* vmm_get_pt_entry(ptable* p_table, virtual_address address)
{
	int index_table = PAGE_TABLE_INDEX(address);
	if (index_table < 0 || index_table >= PAGES_PER_TABLE)
		return NULL; // virtual address is not valid

	if (p_table)
		return &p_table->m_entries[index_table];

	return NULL; 
}

pd_entry* vmm_get_pd_entry(pdirectory* p_table, virtual_address address)
{
	int index_table = PAGE_DIRECTORY_INDEX(address);
	if (index_table < 0 || index_table >= PAGES_PER_TABLE)
		return NULL; // virtual address is not valid

	if (p_table)
		return &p_table->m_entries[index_table];

	return NULL;
}

pdirectory* get_current_directory()
{
	return &m_current_directory;
}


int switch_directory(pdirectory* new_dir)
{
	if (!new_dir)
		return 0;
	
	m_current_directory = new_dir;
	loadPBDR(new_dir->m_entries); //install the directory in cr3 register
	return 1;
}

void vmm_map_page(void* phys, void* virt)
{
	//check that virtual memory is valid
	pdirectory* dir = get_current_directory();

	pd_entry* directory_entry = dir->m_entries[PAGE_DIRECTORY_INDEX(*((int*)virt))];
	
	if (!directory_entry)
		return;

	//table don't exist for this entry so create this table
	if (!pd_entry_is_present(*directory_entry))
	{
		ptable* page_table = pmmngr_alloc_block(); //allocate 4 KB of memory

		if (!page_table)
			return;

		pd_entry_add_attrib(directory_entry, PDE_PRESENT_FLAG);
		pd_entry_add_attrib(directory_entry, PDE_IS_WRITEBLE);
		pd_entry_set_frame(directory_entry, (physical_addr)page_table);
	}

	ptable* page_table = (ptable*)PAGE_GET_PHYSICAL_ADDRESS(directory_entry);

	pt_entry_add_attrib(page_table, PTE_PRESENT_FLAG);
	pt_entry_set_frame(page_table, phys);
	
}

int vmm_initialize()
{
	// Create a default page directory
	pdirectory* dir = (pdirectory*)pmmngr_alloc_blocks(1);

	if (!dir) return false; // Out of memory

	// Clear page directory and set as current
	memset(dir, 0, sizeof(dir));
	for (uint_32 i = 0; i < 1024; i++)
		dir->m_entries[i] = 0x06; // Supervisor, read/write, not present

	// Allocate page table for 0-4MB
	ptable* table = (ptable*)pmmngr_alloc_blocks(1);

	if (!table) return false;   // Out of memory

	// Allocate a 3GB page table
	ptable* table3G = (ptable*)pmmngr_alloc_blocks(1);

	if (!table3G) return false;   // Out of memory

	// Clear page tables
	memset(table, 0, sizeof(ptable));
	memset(table3G, 0, sizeof(ptable));

	// Identity map 1st 4MB of memory
	for (uint_32 i = 0, frame = 0x0, virt = 0x0; i < 1024; i++, frame += PAGE_SIZE, virt += PAGE_SIZE) {
		// Create new page
		pt_entry page = 0;
		page = 0x7;
		SET_FRAME(&page, frame);

		// Add page to 3GB page table
		table3G->m_entries[PAGE_TABLE_INDEX(virt)] = page;
	}

	// Map kernel to 3GB+ addresses (higher half kernel)
	for (uint_32 i = 0, frame = 0x100000, virt = 0xC0000000; i < 1024; i++, frame += PAGE_SIZE, virt += PAGE_SIZE) {
		// Create new page
		pt_entry page = 0;
		page = 0x7;
		SET_FRAME(&page, frame);

		// Add page to 0-4MB page table
		table->m_entries[PAGE_TABLE_INDEX(virt)] = page;
	}

	pd_entry* entry = &dir->m_entries[PAGE_DIRECTORY_INDEX(0xC0000000)];

	*entry = 0x7;
	SET_FRAME(entry, (physical_addr)table); // 3GB directory entry points to default page table

	pd_entry* entry2 = &dir->m_entries[PAGE_DIRECTORY_INDEX(0x00000000)];
	printf("entry2 = %d ", *entry2);
	*entry2 = 0x7;
	printf("entry2 = %d ", *entry2);
	SET_FRAME(entry2, (physical_addr)table3G);    // Default dir entry points to 3GB page table

	// Switch to page directory
	m_current_directory = dir;
	__asm__ volatile("movl %%EAX, %%CR3" : : "a"(m_current_directory));

	// Enable paging: Set PG (paging) bit 31 and PE (protection enable) bit 0 of CR0
	__asm__ volatile("movl %CR0, %EAX; orl $0x80000001, %EAX; movl %EAX, %CR0");

	printf("enable paging");
	BochsConsolePrintChar('A');

	return true;
}


