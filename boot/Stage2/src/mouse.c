#include "mouse.h"

uint_8 packet[4] = {0}; //all three packets from mouse - port 0x60
bool isPacketReady = false;
Point mousePos;
Point prevMousePos;
char prevChars[5] = { 0 };
uint_8 mouseCycle = 0;
extern void isrMouseHandler();
extern idt_descriptor _idt[I86_MAX_INTERRUPTS];
extern unsigned short cursorPosition;


void processMouseData()
{
	if (!isPacketReady)
	{
		return;
	}

	prevMousePos.x = mousePos.x;
	prevMousePos.y = mousePos.y;

	bool xNegative, yNegative, xOverflow, yOverflow;

	if (packet[0] & MACRO_X_SIGN_BIT) {
		xNegative = true;
	}
	else xNegative = false;

	if (packet[0] & MACRO_Y_SIGN_BIT) {
		yNegative = true;
	}
	else yNegative = false;

	if (packet[0] & MACRO_X_OVERFLOW) {
		xOverflow = true;
	}
	else xOverflow = false;

	if (packet[0] & MACRO_Y_OVERFLOW) {
		yOverflow = true;
	}
	else
		yOverflow = false;

	if (!xNegative) {
		mousePos.x += packet[1];
		if (xOverflow) {
			mousePos.x += 255;
		}
	}
	else
	{
		packet[1] = 256 - packet[1];
		mousePos.x -= packet[1];
		if (xOverflow) {
			mousePos.x -= 255;
		}
	}

	if (!yNegative) {
		mousePos.y -= packet[2];
		if (yOverflow) {
			mousePos.y -= 255;
		}
	}
	else
	{
		packet[2] = 256 - packet[2];
		mousePos.y += packet[2];
		if (yOverflow) {
			mousePos.y += 255;
		}
	}

	if (mousePos.x < 1) mousePos.x = 1;
	if (mousePos.x > 78) mousePos.x = 78;
	if (mousePos.y < 1) mousePos.y = 1;
	if (mousePos.y > 23) mousePos.y = 23;
	
	if (packet[0] & MACRO_LEFT_BTN) {
		setCursorPosition(positionFromCoords(mousePos.x, mousePos.y));
	}
	if (packet[0] & MACRO_MIDDLE_BTN) {
		printCharInLoc('M', BACKGROUND_BLINKINGMAGENTA | FORGROUND_GREEN);
	}

	if (packet[0] & MACRO_RIGHT_BTN) {
		printCharInLoc('M', BACKGROUND_BLINKINGRED | FORGROUND_GREEN);
		
	}
	printMouse('*', BACKGROUND_BLACK | FORGROUND_WHITE);
	isPacketReady = false;
}

void mouseHandler()
{
		uint_8 status = inb(DATA_REG);
		switch (mouseCycle)
		{
		case 0:
			if (isPacketReady) break;
			if (status & MACRO_ALWAYSE_ONE == 0) break;
			packet[0] = status;
			mouseCycle++;
			break;
		case 1:
			if (isPacketReady) break;
			packet[1] = status;
			mouseCycle++;
			break;
		case 2:
			if (isPacketReady) break;
			packet[2] = status;
			isPacketReady = true;
			mouseCycle = 0;
			break;
		}

		interruptdone(MOUSE_IRQ);
		
}


void mouseInputWait()
{
	uint_16 waitTime = 10000;
	while (waitTime--)
	{
		if (inb(COMMAND_STATUS_REG) & 0x1)
			return;
	}
}

void mouseOutputWait()
{
	uint_16 waitTime = 10000;
	while (waitTime--)
	{
		if (inb(COMMAND_STATUS_REG) & 2 == 0)
			return;
	}
}


void mouseWriteCommand(uint_8 command)
{
	mouseOutputWait();
	outb(COMMAND_STATUS_REG, 0xD4);
	mouseOutputWait();
	outb(DATA_REG, command);
}

uint_8 mouseRead()
{
	mouseInputWait();
	return inb(DATA_REG);
}

extern char prevChars[5];

void mouseInit()
{
	outb(COMMAND_STATUS_REG, COMMAND_ENABLE_MOUSE_PORT); //enabling the auxiliary device - mouse
	mouseOutputWait();
	outb(COMMAND_STATUS_REG, COMMAND_GET_COMMAND_BYTE); 
	mouseInputWait();
	uint_8 statusVal = inb(DATA_REG);

	statusVal |= MACRO_MOUSE_INTERRUPT_ENABLE;

	mouseOutputWait();
	outb(COMMAND_STATUS_REG, COMMAND_WRITE_COMMAND_BYTE);
	mouseOutputWait();
	outb(DATA_REG, statusVal);
	mouseWriteCommand(COMMAND_SET_DEFAULT_ATTRIB);
	mouseRead(); //read ACK response (0XFA)

	mouseWriteCommand(COMMAND_ENABLE_PACKET_STREAMING);
	mouseRead(); //read ACK response (0XFA)
	_idt[44].baseLo = (uint_16)((uint_32)&isrMouseHandler & 0xffff);
	_idt[44].baseHi = (uint_16)(((uint_32)&isrMouseHandler >> 16) & 0xffff);
	_idt[44].reserved = 0;
	_idt[44].flags = (uint_8)(I86_IDT_DESC_PRESENT | I86_IDT_DESC_BIT32);
	_idt[44].sel = 0x8;

	prevChars[0] = *(VGA_MEMORY_ADDRESS + 0 * 2);
	prevChars[1] = *(VGA_MEMORY_ADDRESS + (0 + 1) * 2);
	prevChars[2] = *(VGA_MEMORY_ADDRESS + (0 + 80) * 2);
	prevChars[3] = *(VGA_MEMORY_ADDRESS + (0 + 81) * 2);
}
