
DetectCPUID:
	pushfd
	pop eax ; flag register is noew in eax

	mov ecx, eax ; for comparing it later

	xor eax, 1 << 21 ; if cpuid(bit 21) is on so the 21 bit in eax will be zero (0 xor 0 = 0)

	push eax
	popfd

	pushfd
	pop eax

	push ecx
	popfd

	xor eax, ecx ; check if the cpuid flag is on
	jz NoCPUID
	ret

DetectLongMode:
	mov eax, 0x80000001    ; Set the A-register to 0x80000001.
    cpuid                  ; CPU identification.
    test edx, 1 << 29      ; Test if the LM-bit, which is bit 29, is set in the D-register. test = and without saving the result
    jz NoLongMode         ; They aren't, there is no long mode.
	ret

NoLongMode:
	hlt ; No Long mode support

NoCPUID:
	hlt ; No CpuID support