#ifndef PHYSICAL_MEMORY_H
#define PHYSICAL_MEMORY_H

#include "Typedefs.h"
#include "string.h"
#include "vga.h"

// 8 blocks per byte
#define PMMNGR_BLOCKS_PER_BYTE 8

// block size (4k)
#define PMMNGR_BLOCK_SIZE	4096

// block alignment
#define PMMNGR_BLOCK_ALIGN	PMMNGR_BLOCK_SIZE

typedef struct multiboot_info
{
	uint_32 memoryLo;
	uint_32 memoryHi;
	uint_32 bootDevice;
	uint_32 mmap_length;
	uint_32 mmap_addr;
}multiboot_info;

// format of a memory region
typedef struct memory_region {

	uint_32	startLo;
	uint_32	startHi;
	uint_32	sizeLo;
	uint_32	sizeHi;
	uint_32	type;
	uint_32	acpi_3_0;
}memory_region;

typedef	uint_32 physical_addr;
// size of physical memory
static	uint_32	_mmngr_memory_size = 0;

// number of blocks currently in use
static	uint_32	_mmngr_used_blocks = 0;

// maximum number of available memory blocks
static	uint_32	_mmngr_max_blocks = 0;

// memory map bit array. Each bit represents a memory block
static uint_32* _mmngr_memory_map = 0;

/* mmap functions */

/*******************************
	  Interface Functions
/*******************************/

// initialize function to out memory map
void pmmngr_init(uint_32 memSize, physical_addr bitmap);
// initialize a region
void pmmngr_init_region(physical_addr base, uint_32 size);
// deinitalize a region
void pmmngr_deinit_region(physical_addr base, uint_32 size);
// allocates a single block of physical memory
void* pmmngr_alloc_block();
void* pmmngr_alloc_blocks(uint_32 size);
//  releases a block of physical memory
void pmmngr_free_block(void* p);
void pmmngr_free_blocks(void* p, uint_32 size);

/*******************************
	 Implementation Functions
/*******************************/

// set a 1 to a specific bit place using the (bit / 32) to get the index
void mmap_set(int bit);

// set a 0 to a specific bit place using the (bit / 32) to get the index
void mmap_unset(int bit);

// return what inside a specific bit in specific memory map index
int mmap_test(int bit);

// return the number of blocks in our memory map
uint_32 pmmngr_get_block_count();
uint_32 pmmngr_get_use_block_count();
uint_32 pmmngr_get_free_block_count();
// return index of first free bit in bit map
int mmap_first_free();
int mmap_first_free_s(uint_32 size);
uint_32 pmmngr_get_memory_size();




#endif // !PHYSICAL_MEMORY_H


