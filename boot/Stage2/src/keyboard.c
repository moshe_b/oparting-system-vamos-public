
#include "keyboard.h"
//start from line 8 (because of the printings)
int linesCounter = 8;

//buffer commands
char userBuffer[MAX_BUFFER_COMMAND] = { 0 };
// Prevent Buffer overflow
int countChars = 0;
//! default handler to catch unhandled system interrupts.
static int old_val = 0;
extern unsigned short cursorPosition;
extern bool showTimeState;
// Nano flag cheker
static bool firstNanoEnter = true;
static bool isNanoState = false;


/* SCAN CODE TABLES START*/
const char scanCodeLookupTable[] =
{
	0, 0, '1', '2',
	'3', '4', '5', '6',
	'7', '8', '9', '0',
	'-', '=', 0, 0,
	'q', 'w', 'e', 'r',
	't', 'y', 'u', 'i',
	'o', 'p', '[', ']',
	0, 0, 'a', 's',
	'd', 'f', 'g', 'h',
	'j', 'k', 'l', ';',
	'\'', '`', '0', '\\',
	'z', 'x', 'c', 'v',
	'b', 'n', 'm', ',',
	'.', '/', 0, '*',
	0, ' '
};

const char shiftScanCodeLookupTable[] =
{
	0, 0, '!', '@', '#', '$',
	'%', '^', '&', '*', '(', ')',
	'-', '=', 0, 0,
	'q', 'w', 'e', 'r',
	't', 'y', 'u', 'i',
	'o', 'p', '{', '}',
	0, 0, 'a', 's',
	'd', 'f', 'g', 'h',
	'j', 'k', 'l', ':',
	'"', '`', '0', '|',
	'z', 'x', 'c', 'v',
	'b', 'n', 'm', '<',
	'>', '?', 0, '*', 
	0, ' '
};

const char capsLockScanCodeLookupTable[] =
{
	0, 0, '1', '2',
	'3', '4', '5', '6',
	'7', '8', '9', '0',
	'-', '=', 0, 0,
	'Q', 'W', 'E', 'R',
	'T', 'Y', 'U', 'I',
	'O', 'P', '[', ']',
	0, 0, 'A', 'S',
	'D', 'F', 'G', 'H',
	'J', 'K', 'L', ';',
	'\'', '`', '0', '\\',
	'Z', 'X', 'C', 'V',
	'B', 'N', 'M', ',',
	'.', '/', 0, '*',
	0, ' '
};
/* SCAN CODE TABLES END*/

int commandIndex = 0;
ShellCommand commandsShell;
Params globalParams;
currPath m_currPath;
historyCommands m_history;

/*Special scanCodes enum Table*/
enum {
	left_shift_pressed = 0x2A, right_shift_pressed = 0x36,
	left_shift_released = 0xB6, right_shift_released = 0xAA,
	capsLock_pressed = 0x3A, capsLock_released = 0xBA,
	left_ctrl_pressed = 0x1D, left_ctrl_released = 0x9D
};

typedef struct
{
	bool m_shift;
	bool m_capsLock;
	bool m_ctrl
}specialKeysStatus;

specialKeysStatus m_keysStatus;
extern uint_32 colors[NUM_COLORS];
extern COLOR m_OsColor;

void initUserInterface()
{
	initShellCommands();
	strcpy(m_currPath.pathDir, "C://");
	m_keysStatus.m_capsLock = false;
	m_keysStatus.m_shift = false;
	m_keysStatus.m_ctrl = false;

	m_history.sizeCommands = 0;
	m_history.commandIndex = 0;

	printPath();
}

int handleCommand(char* userBuff)
{
	int isFunctionExist = 0; // 0 - not exist. 1 - exist
	char command[MAX_LEN_COMMAND] = { 0 };
	int i = 0;
	int indexFunction = 0;

	for (i = 0; i < MAX_BUFFER_COMMAND && userBuff[i] != ' ' && userBuff[i] != 0; i++)
		command[i] = userBuff[i];

	for (i = 0; i < commandsShell.currNumShellCommands && strcmp(commandsShell.functions[i].functionName, command); i++);

	bool isEmptyCommand = isEmptyString(command);
	if (i == commandsShell.currNumShellCommands && !isEmptyCommand) // invalid command name
	{
		return INVALID_COMMAND_NAME;
	}
	else if (isEmptyCommand)
	{
		printString("\n\r", BACKGROUND_BLACK | FORGROUND_WHITE);
		return VALID_COMMAND; //user enter enter only
	}
	indexFunction = i;
	//get params after command 
	int numParamsCounter = 0;
	int conIndex = strlen(command) + 1;

	//insert the parameters to a globalParams
	while (conIndex < strlen(userBuff))
	{
		int indexFirstNotSpace = findFirstNotSpace(userBuff + conIndex);
		if (indexFirstNotSpace == NPOS)
			break;

		// +strlen for not consider the first space
		int indexSpace = find(userBuff + conIndex + indexFirstNotSpace, ' ');
		indexSpace = indexSpace == NPOS ? strlen(userBuff) - 1 : indexSpace;

		int offset = *(userBuff + conIndex + indexFirstNotSpace + indexSpace + 1) == ' ' ? 0 : 1;
		strncpy(globalParams[numParamsCounter],
			userBuff + conIndex + indexFirstNotSpace,
			indexSpace - indexFirstNotSpace  + offset);
		conIndex += indexFirstNotSpace + indexSpace + 1;
		
		numParamsCounter++;
	}

	//number of given parameters is not correct
	if (numParamsCounter != commandsShell.functions[indexFunction].numParams && numParamsCounter != 0)
	{
		return NUMBER_PARAMS_NOT_VALID;
	}

	commandsShell.functions[indexFunction].function(numParamsCounter); //calling the shell function 
	clean(userBuffer);
	//command executed

	if (strCmp(command, "nano") == 0 && globalParams[0][0] != 0)
		return NANO_COMMAND;

	return VALID_COMMAND;
}


void initShellCommands()
{
	strcpy(commandsShell.functions[0].functionName, "tickcounter");
	commandsShell.functions[0].numParams = 0;
	commandsShell.functions[0].function = (ShellFunction)shellTickCounter;

	strcpy(commandsShell.functions[1].functionName, "whoami");
	commandsShell.functions[1].numParams = 0;
	commandsShell.functions[1].function = (ShellFunction)shellWhoami;

	strcpy(commandsShell.functions[2].functionName, "echo");
	commandsShell.functions[2].numParams = 3;
	commandsShell.functions[2].function = (ShellFunction)shellEcho;

	strcpy(commandsShell.functions[3].functionName, "beep");
	commandsShell.functions[3].numParams = 0;
	commandsShell.functions[3].function = (ShellFunction)shellBeep;

	strcpy(commandsShell.functions[4].functionName, "help");
	commandsShell.functions[4].numParams = 0;
	commandsShell.functions[4].function = (ShellFunction)shellHelp;

	strcpy(commandsShell.functions[5].functionName, "cd");
	commandsShell.functions[5].numParams = 1;
	commandsShell.functions[5].function = (ShellFunction)shellCd;
	
	strcpy(commandsShell.functions[6].functionName, "ls");
	commandsShell.functions[6].numParams = 1;
	commandsShell.functions[6].function = (ShellFunction)shellListDir;

	strcpy(commandsShell.functions[7].functionName, "cls");
	commandsShell.functions[7].numParams = 0;
	commandsShell.functions[7].function = (ShellFunction)shellCleanScrean;

	strcpy(commandsShell.functions[8].functionName, "touch");
	commandsShell.functions[8].numParams = 1;
	commandsShell.functions[8].function = (ShellFunction)shellTouch;
	
	strcpy(commandsShell.functions[9].functionName, "mkdir");
	commandsShell.functions[9].numParams = 1;
	commandsShell.functions[9].function = (ShellFunction)shellMkdir;

	strcpy(commandsShell.functions[10].functionName, "cat");
	commandsShell.functions[10].numParams = 1;
	commandsShell.functions[10].function = (ShellFunction)shellCat;

	strcpy(commandsShell.functions[11].functionName, "tree");
	commandsShell.functions[11].numParams = 0;
	commandsShell.functions[11].function = (ShellFunction)shellTree;

	strcpy(commandsShell.functions[12].functionName, "nano");
	commandsShell.functions[12].numParams = 1;
	commandsShell.functions[12].function = (ShellFunction)shellNano;
	
	strcpy(commandsShell.functions[13].functionName, "rm");
	commandsShell.functions[13].numParams = 1;
	commandsShell.functions[13].function = (ShellFunction)shellRemoveFile;
	
	strcpy(commandsShell.functions[14].functionName, "cp");
	commandsShell.functions[14].numParams = 2;
	commandsShell.functions[14].function = (ShellFunction)shellCopyFile;

	strcpy(commandsShell.functions[15].functionName, "mv");
	commandsShell.functions[15].numParams = 2;
	commandsShell.functions[15].function = (ShellFunction)shellMoveFile;

	strcpy(commandsShell.functions[16].functionName, "color");
	commandsShell.functions[16].numParams = 1;
	commandsShell.functions[16].function = (ShellFunction)shellColor;

	strcpy(commandsShell.functions[17].functionName, "rmdir");
	commandsShell.functions[17].numParams = 1;
	commandsShell.functions[17].function = (ShellFunction)shellRemoveDir;

	commandsShell.currNumShellCommands = 18;  // currently we have 14 shell functions in our os 
}

void printListDir(char* path)
{
	FILE files[16];

	if (!listDir(path, files))
	{
		printf("\n\r%s is not directory\n\r", path);
		return;
	}
	int i = 0;
	while (files[i].m_fileName[0] != 0)
	{
		DATE dateFile;
		getCreateDateFile(files[i], &dateFile);
		printf("\n\r| %02d.%02d.%02d  %02d:%02d:%02d", dateFile.m_day, dateFile.m_month,
					dateFile.m_year+1980, dateFile.m_hour, dateFile.m_minute, 
					dateFile.m_second);
		COLOR colortemp;
		colortemp.m_backColor = m_OsColor.m_backColor;
		colortemp.m_forColor = m_OsColor.m_forColor;

		if (isDir(files[i]))
		{
			setOsColor(FORGROUND_LIGHTGREEN, BACKGROUND_BLACK);
			printf(" d");
		}
		else
		{
			setOsColor(FORGROUND_RED, BACKGROUND_BLACK);
			printf(" f");
		}

		setOsColor(FORGROUND_BLUE, BACKGROUND_BLACK);
		printf(" %s ", files[i].m_fileName);
		setOsColor(FORGROUND_WHITE, BACKGROUND_BLACK);
		printf("|\n\r");

		setOsColor(colortemp.m_forColor, colortemp.m_backColor);
		i++;
	}

	if (i == 0)
	{
		printf("\n\r");
	}
}

void insertCommand()
{
	if (m_history.sizeCommands > 0 && m_history.sizeCommands < 100 && !isEmptyString(userBuffer))
	{
		if (strcmp(userBuffer, m_history.commands[m_history.sizeCommands - 1]) != 0) //not equales commands so continue increase
		{
			strcpy(m_history.commands[m_history.sizeCommands], userBuffer);
			m_history.sizeCommands++;
		}
	}
	else if(!isEmptyString(userBuffer))
	{
		strcpy(m_history.commands[0], userBuffer);
		m_history.sizeCommands = 1;
	}
}

void textShiftHandling()
{
	for (int i = cursorPosition % 80; i < strlen(m_currPath.pathDir) + 3 + strlen(userBuffer) + 1; i++)
	{
		return;
	}

}

void printPath()
{
	COLOR colortemp;
	colortemp.m_backColor = m_OsColor.m_backColor;
	colortemp.m_forColor = m_OsColor.m_forColor;

	setOsColor(FORGROUND_GREEN, BACKGROUND_BLACK);
	printf("%s", m_currPath.pathDir);
	setOsColor(FORGROUND_BLUE, BACKGROUND_BLACK);
	printf(" #");
	setOsColor(FORGROUND_WHITE, BACKGROUND_BLACK);
	printf(">");

	m_OsColor.m_backColor = colortemp.m_backColor;
	m_OsColor.m_forColor = colortemp.m_forColor;
}

void printChar(char chr)
{

}


//helper functions
void getFullPath(char* pathInput, char* fullPathSave)
{
	if (strncmp(pathInput, "C://", 4) == 0)
	{
		strcpy(fullPathSave, pathInput);
	}
	else
	{
		strcpy(fullPathSave, m_currPath.pathDir);
		if(fullPathSave[4] != 0)
			fullPathSave[strlen(fullPathSave)] = '/';
		
		strcpy(fullPathSave + strlen(fullPathSave), pathInput);
	}
}

void printColorsMenu()
{
	printf("----- colors supported is VAMOS: -----\n\r");
	printf("0. black font         16. blue background\n\r");
	printf("1. blue font          17. green font\n\r");
	printf("2. green font         18. cyan background \n\r");
	printf("3. cyan font          19. red background \n\r");
	printf("4. red font           20. magenta background\n\r");
	printf("5. magenta font       21. brown background \n\r");
	printf("6. brown font         22. light gray background\n\r");
	printf("7. light gray font    23. blinking black background\n\r");
	printf("8. dark gray font     24. blinking blue background\n\r");
	printf("9. light blue font    25. blinking green background\n\r");
	printf("10. light green font  26. blinking cyan background \n\r");
	printf("11. light cyan font   27. blinking red background \n\r");
	printf("12. light read font   28. blinking mageta background\n\r");
	printf("13 light magenta font 29. blinking yellow background\n\r");
	printf("14. yellow color      30. blinking white background\n\r");
	printf("15. white color       31. black background\n\r");
}

void deleteDirRecursive(char* path)
{
	FILE files[30] = { 0 };
	listDir(path, files);
	int size = 0;
	while (files[size].m_fileName[0])
		size++;

	for (int i = 0; i < size; i++) {

			char newPath[250] = { 0 };
			strcpy(newPath, path);
			if (strcmp(newPath, "C://") != 0)
			{
				strcpy(newPath + strlen(newPath), "/");
			}

			strcpy(newPath + strlen(newPath), files[i].m_fileName);
			deleteFile(newPath);
			deleteDirRecursive(newPath);
		
	}
}


//*********shell commands***********
/*
	shell commands of file system
*/

void shellTouch(int numParams)
{
	//not parameters given
	if (globalParams[0][0] == 0)
	{
		printf("\n\rtouch: missing operand\n\r");
		return;
	}
	else if (globalParams[1][0])
	{
		printf("\n\rtouch: too much operands\n\r");
		return;
	}


	char fileFullPath[150] = { 0 };
	getFullPath(globalParams[0], fileFullPath);
	
	if (!isPathExist(m_currPath.pathDir))
	{
		printf("\n\rtouch: cannot touch '%s': No such file or directory\n\r", globalParams[0]);
		return;
	}
	
	createFile(fileFullPath);
	printf("\n\r");
}

void shellTree(int numParams)
{
	char path[200] = { 0 };
	char prefix[200] = { 0 };
	strcpy(path, m_currPath.pathDir);
	printf("\n\r");
	recursive_print(path, prefix);
}

void shellMkdir(int numParams)
{
	if (globalParams[0][0] == 0)
	{
		printf("\n\mkdir: missing operand\n\r");
		return;
	}
	else if (globalParams[1][0])
	{
		printf("\n\mkdir: too much operands\n\r");
		return;
	}

	char fileFullPath[150] = { 0 };
	char dirPath[150] = { 0 };
	getFullPath(globalParams[0], fileFullPath);
	getDirPath(fileFullPath, dirPath);

	if (!isPathExist(dirPath))
	{
		printf("\n\rmkdir: cannot create '%s': No such file or directory\n\r", fileFullPath);
		return;
	}

	createDirectory(fileFullPath);
	printf("\n\r");
}

void shellRemoveFile(int numParams)
{
	if (globalParams[0][0] == 0)
	{
		printf("\n\rrm: missing operand\n\r");
		return;
	}
	else if (globalParams[1][0])
	{
		printf("\n\rrm: too much operands\n\r");
		return;
	}

	char fileFullPath[150] = { 0 };
	getFullPath(globalParams[0], fileFullPath);

	if (!isPathExist(fileFullPath))
	{
		printf("\n\rrm: cannot rm '%s': No such file or directory\n\r", globalParams[0]);
		return;
	}

	FILE file;
	openFile(fileFullPath, &file);

	if (isDir(file))
	{
		printf("\n\rrm: cannot remove '%s': Is a directory\n\r", file.m_fileName);
		return;
	}

	deleteFile(fileFullPath);
	printf("\n\r");
}

void shellRemoveDir(int numParams)
{
	if (globalParams[0][0] == 0)
	{
		printf("\n\r");
		return;
	}

	char fullPath[250] = { 0 };
	getFullPath(globalParams, fullPath);

	if (!isPathExist(fullPath))
	{
		printf("\n\rrmdir: path doesn't exist\n\r");
		return;
	}

	//rmdir myDir
	DIR mDir;
	openFile(fullPath, &mDir);

	if (!isDir(mDir))
	{
		printf("\n\rrmdir: cannot remove '%s': Is not a directory\n\r", mDir.m_fileName);
		return;
	}

	deleteDirRecursive(fullPath);
	deleteFile(fullPath);
	printf("\n\r");
}

int shellCopyFile(int numParams)
{
	if (globalParams[1][0] == 0)
	{
		printf("\n\rcp: missing operand\n\r");
		return 0;
	}
	else if (globalParams[2][0])
	{
		printf("\n\rcp: too much operands\n\r");
		return 0;
	}

	char fullPathFileSource[150] = { 0 };
	getFullPath(globalParams[0], fullPathFileSource);
	rtrim(fullPathFileSource);

	FILE file;
	if (!isPathExist(fullPathFileSource))
	{
		printf("\n\rcp: cannot cp '%s': No such file or directory\n\r", globalParams[0]);
		return 0;
	}

	openFile(fullPathFileSource, &file);

	char fullPathDirDest[150] = { 0 };
	getFullPath(globalParams[1], fullPathDirDest);
	rtrim(fullPathDirDest);
	DIR dir;
	
	if (!openDir(fullPathDirDest, &dir))
	{
		printf("\n\rcp: '%s': No such directory\n\r", globalParams[1]);
		return 0;
	}

	if (!isDir(dir))
	{
		printf("cp: '%s': is not a directory", globalParams[1]);
		return 0;
	}

	char fileName[150] = { 0 };
	getFileName(fullPathFileSource, fileName);

	uint_16 clusters[20] = { 0 };
	getClusters(getCluster(dir.m_firstCluster), clusters);

	//allocated new cluster for file (preventing linking two files)
	uint_16 fileCluster = getFreeCluster();
	initFirstCluster(&file, fileCluster);

	//read file content
	char bufferFile[512] = { 0 };
	readFile(fullPathFileSource, bufferFile);
	//write file content to the new copy of the file
	char fileDestPath[150] = { 0 };
	strcpy(fileDestPath, fullPathDirDest);
	fileDestPath[strlen(fullPathDirDest)] = '/';
	strcpy(fileDestPath + strlen(fullPathDirDest)  + 1, fileName);
	
	addFileToDir(clusters, NULL, &file, true);
	printf("\n\r");

	int writeCode = writeFile(fileDestPath, bufferFile);

	return writeCode;
}

void shellMoveFile(int numParams)
{
	if (!shellCopyFile(numParams))
		return;

	char fileFullPath[150] = { 0 };
	getFullPath(globalParams[0], fileFullPath);

	rtrim(fileFullPath);
	int deleteCode = deleteFile(fileFullPath);
	printf("\n\r");
}

void shellCat(int numParams)
{
	if (globalParams[0][0] == 0)
	{
		printf("\n\r");
		return;
	}

	char path[150] = { 0 };
	getFullPath(globalParams[0], path);

	if (!isPathExist(path))
	{
		printf("\n\rcat: %s: No such file or directory\n\r", globalParams[0]);
		return;
	}

	char buff[1024];
	readFile(path, buff);
	printf("\n\r%s\n\r", buff);
}

void shellCd(int numParams)
{
	if (globalParams[0][0] == 0)
	{
		printf("\n\r");
		return;
	}
	if (strcmp("C://", globalParams[0]) == 0 || strcmp("C:/", globalParams[0]) == 0)
	{
		initFile("C", "", 1, true, &m_currPath.pathDir);
		strcpy(m_currPath.pathDir, "C://");
		printString("\n\r", BACKGROUND_BLACK | FORGROUND_WHITE);
		return;
	}


	char path[150] = { 0 };

	if (strcmp(globalParams[0], "..") == 0)
	{
		if (strcmp(m_currPath.pathDir, "C://") != 0)
		{
			getDirPath(m_currPath.pathDir, path);
		}
		else
		{
			printf("\n\r");
			return;
		}
	}
	else if (strcmp(globalParams[0], ".") == 0)
	{
		strcpy(path, m_currPath.pathDir);
	}
	else if (strcmp(globalParams[0], "/") == 0)
	{
		initFile("C", "", 1, true, &m_currPath.pathDir);
		strcpy(m_currPath.pathDir, "C://");
		printString("\n\r", BACKGROUND_BLACK | FORGROUND_WHITE);
		return;
	}
	else
		getFullPath(globalParams[0], path);

	//handle cd inside directory without the full path

	if (!isPathExist(path))
	{
		printf("\n\rcd: %s: No such file or directory\n\r", globalParams[0]);
		return;
	}
	DIR dir;
	openDir(path, &dir);

	if (!isDir(dir))
	{
		printf("\n\rcd: %s: Not a directory\n\r", globalParams[0]);
		return;
	}


	strncpy((char*)&m_currPath.currDir, (char*)&dir, sizeof(FILE));
	for (int i = 0; i < 150; i++) m_currPath.pathDir[i] = 0;
	strcpy(m_currPath.pathDir, path);
	printString("\n\r", BACKGROUND_BLACK | FORGROUND_WHITE);
	
}

void shellListDir(int numParams)
{
	if (globalParams[0][0] == 0)
	{
		char path[150] = { 0 };
		strcpy(path, m_currPath.pathDir);
		printListDir(path);
		printf("\n\r");
		return;
	}
	char path[150] = { 0 };
	getFullPath(globalParams[0], path);
	if (!isPathExist(path))
	{
		printf("\n\rcd: %s: No such file or directory\n\r", path);
		return;
	}

	printListDir(path);
	printf("\n\r");
}


void shellTickCounter(int numParams)
{
	//num Params is zero
	printString("\n\rTick count = ", BACKGROUND_BLACK | FORGROUND_WHITE);
	printInt(get_tick_count());
	printString("\n\r", BACKGROUND_BLACK | FORGROUND_WHITE);
}

void shellWhoami(int NumParams)
{
	printString("\n\rKernel Mode\n\r", BACKGROUND_BLACK | FORGROUND_WHITE);
}

void shellEcho(int NumParams)
{
	if (globalParams[0][0] != 0 && NumParams == 1)
	{
		printString("\n\r", BACKGROUND_BLACK | FORGROUND_WHITE);
		printString(globalParams[0], BACKGROUND_BLACK | FORGROUND_WHITE);
		printString("\n\r", BACKGROUND_BLACK | FORGROUND_WHITE);
	}
	else if (NumParams == 3 && strcmp(globalParams[1], ">"))
	{
		char path[150] = { 0 };
		getFullPath(globalParams[2], path);

		if (!isPathExist(path))
		{
			printf("\n\echo: %s: No such file\n\r", globalParams[0]);
			return;
		}
		int codeWrite = writeFile(path, globalParams[0]);
		printf("\n\r");
	}
	else
	{
		printString("\n\recho using: echo <buffer output>\n\r", BACKGROUND_BLACK | FORGROUND_WHITE);
	}
}

void shellBeep(int NumParams)
{
	beep();
	printString("\n\r", FORGROUND_WHITE | BACKGROUND_BLACK);
}

void shellHelp(int numParams)
{
	printString("\n\rVAM-OS Commands:\n\r-------------------------------------\n\rtickcounter - Return a PIC tick count\n\rwhoami - Displays the username of the current user \n\recho - used to display line of text/string that are passed as an argument \n\rbeep - Just make a sound\n\rcd - Known as change directory command. Used to change working directory.\n\rls - Command is used to list files or directories in VAMOS\n\rcls - Clean screen\n\r", BACKGROUND_BLACK | FORGROUND_WHITE);
	printf("touch - Is used to create, change and modify timestamps of a file\n\rmkdir - This Command allows the user to create directories\n\rcat - reads data from the file and gives their content as output\n\rtree - Lists all the files or directories found in the given directories\n\rnano - opens a Nano File editor V.1.0\n\r");
	printf("rm - Delete files\n\rcp - copy file\n\rmv - move file\n\rcolor - set forground and background color\n\r-------------------------------------\n\r");
}

void shellNano(int numParams)
{
	//not parameters given
	if (globalParams[0][0] == 0)
	{
		printf("\n\rnano: missing filename\n\r");
		return;
	}
	else if (globalParams[1][0])
	{
		printf("\n\rnano: too much operands\n\r");
		return;
	}

	char fullPath[150] = { 0 };
	getFullPath(globalParams[0], fullPath);
	bool fileExistsFlag = false;
	uint_16* vga_buffer = (uint_16*)VGA_MEMORY_ADDRESS;
	setCursorPosition(0);
	clear_vga_buffer(&vga_buffer, FORGROUND_WHITE, BACKGROUND_BLACK);
	char fileBuffer[VGA_HEIGHT * VGA_WIDTH] = { 0 };

	COLOR colortemp;
	colortemp.m_backColor = m_OsColor.m_backColor;
	colortemp.m_forColor = m_OsColor.m_forColor;

	setOsColor(FORGROUND_GREEN, BACKGROUND_BLACK);

	if (isPathExist(fullPath))
	{
		fileExistsFlag = true;
		readFile(fullPath, fileBuffer);
		printf("Existing file:                     %s\n\r", fullPath);
	}
	else
	{
		printf("New file:                         %s\n\r", fullPath);
	}

	setOsColor(FORGROUND_CYAN, BACKGROUND_BLACK);
	printf("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ NANO EDITOR V.2.0 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
	setOsColor(colortemp.m_forColor, colortemp.m_backColor);
	if (fileExistsFlag)
		printf("%s", fileBuffer);
}

void shellColor(int numParams)
{
	if (globalParams[0][0] == 0)
	{
		//print color help
		printf("\n\r");
		printColorsMenu();
		printf("\n\r");
		return;
	}

	int colorInd = 0;

	if ((colorInd = atoi(globalParams[0])) == -1)
	{
		printf("\n\rColor: Only integers allowed\n\r");
		return;
	}

	uint_8 bgColor = m_OsColor.m_backColor;
	uint_8 frColor = m_OsColor.m_forColor;

	if (colorInd < 0 || colorInd > 32)
	{
		printf("\n\rColor: Only colors between 1 to 32 are allowed\n\r");
		return;
	}

	printf("color Ind = %d ", colorInd);
	printf("frColor = %d bgColor = %d ", frColor, bgColor);
	if (colorInd == NUM_COLORS-1)
	{
		printf("black color");
		bgColor = 0;
	}
	else
	{
		if (colorInd >= 0 && colorInd <= 0XF)
		{
			frColor = colors[colorInd];
		}
		else
		{
			bgColor = colors[colorInd];
		}
	}

	printf("frColor = %d bgColor = %d ", frColor, bgColor);
	changeScreenColor(frColor, bgColor);
	printf("\n\r");
}

void shellCleanScrean(int numParams)
{
	uint_16* vga_buffer = (uint_16*)VGA_MEMORY_ADDRESS;
	setCursorPosition(0);
	clear_vga_buffer(&vga_buffer, m_OsColor.m_forColor, m_OsColor.m_backColor);
}

void shellReadDisk(int numParams)
{
	if (globalParams[0][0] != 0)
	{
		int num = atoi(globalParams[0]);
		if (num == -1)
		{
			printf("\n\rInvalid parameter. must be numbers\n\r");
			return;
		}
		else if (num < 0)
		{
			printf("\n\rInvalid parameter. sector must be positive\n\r");
			return;
		}

		char* buff = pmmngr_alloc_block();
		ataRead(0, num, 2, buff);
		printf("\n\rsector number %d = %s\n\r", num, buff);
		clean(buff);
		pmmngr_free_block(buff);
	}
	else
	{
		printString("\n\r\\read using: read <sector number>\n\r", BACKGROUND_BLACK | FORGROUND_WHITE);
	}
}

void shellWriteDisk(int numParams)
{
}

void enterHandler(char userBuffer[])
{
	if (linesCounter == 25 && isEmptyString(&userBuffer[0]))  //last line of the screen
	{
		screenScroll(1);
		linesCounter--;
		cursorPosition -= 81;
		setCursorPosition(cursorPosition);
		return;
	}

	int validCode = handleCommand(userBuffer);

	if (validCode == INVALID_COMMAND_NAME)
	{
		printf("\n\rCommand doesn't exist...\n\r");
	}
	else if (validCode == NUMBER_PARAMS_NOT_VALID)
	{
		printf("\n\rIncorrect number of parameters\n\r");
	}
	if (validCode == NANO_COMMAND)
	{
		isNanoState = true;
		showTimeState = !isNanoState;
		return;
	}

	//clean parameters
	for (int i = 0; i < MAX_PARAMS; i++)
		clean(globalParams[i]);
	linesCounter++;
	setCursorPosition(cursorPosition);
	printPath();
}

void clean(char* var) {
	int i = 0;
	while (var[i] != 0) {
		var[i] = 0;
		i++;
	}
}

void printCharHandler(uint_8 scanCode)
{
	if (m_keysStatus.m_shift)
	{
		print_char(shiftScanCodeLookupTable[scanCode]);
		userBuffer[countChars] = shiftScanCodeLookupTable[scanCode];
		countChars++;
	}
	else if (m_keysStatus.m_capsLock)
	{
		print_char(capsLockScanCodeLookupTable[scanCode]);
		userBuffer[countChars] = capsLockScanCodeLookupTable[scanCode];
		countChars++;
	}
	else
	{
		print_char(scanCodeLookupTable[scanCode]);
		userBuffer[countChars] = scanCodeLookupTable[scanCode];
		countChars++;
	}
}
int counter = 0;
void isrKeyboardHandler() {

	uint_8 outputPort = 0;
	if (inb(0x64) & 1)
	{
		uint_8 scanCode = inb(0x60);
		static int tickPrev = 0;

		if (scanCode == 0x9C) // Enter pressed
		{
			if (!isNanoState)
			{
				unsigned char kbb = 0;
				while (((kbb = inb(0x64)) & 1) == 1)
				{
					uint_8 chr = inb(0x60);
				}

				insertCommand();
				enterHandler(userBuffer);


				while (((kbb = inb(0x64)) & 1) == 1)
				{
					uint_8 chr = inb(0x60);
				}
				clean(userBuffer);
				countChars = 0;
			}
			else
			{
				if (firstNanoEnter)
				{
					setLineIndex(3);
					firstNanoEnter = false;
				}
				newline();
			}
			tickPrev = get_tick_count();

			if (get_tick_count() - tickPrev > 5)
			{
				
			}
		}
		else if (scanCode < 0x3A && scanCode != right_shift_pressed
			&& countChars < MAX_BUFFER_COMMAND && scanCode != left_shift_pressed && scanCode != left_ctrl_pressed) // Limit to only ALPHA CHARS
		{
			if(!m_keysStatus.m_ctrl)
				printCharHandler(scanCode);
		}
		else if (scanCode == 0x8E) // Backspace pressed
		{
			if (countChars > 0)
			{
				countChars -= 2;
				userBuffer[countChars] = '\0';
				if (isNanoState)
					printBackspace(0, false, VGA_WIDTH * 2);
				else
					printBackspace(strlen(m_currPath.pathDir), true, 0);
			}
		}
		else if (scanCode == 0x4B && !isNanoState) //left key
		{
			if (cursorPosition % 80 > strlen(m_currPath.pathDir) + 3)
			{
				setCursorPosition(cursorPosition - 1);
			}
		}
		else if (scanCode == 0x4D && !isNanoState) //right key
		{
			if (cursorPosition % 80 < (strlen(m_currPath.pathDir) + 3 + strlen(userBuffer)))
			{
				setCursorPosition(cursorPosition + 1);
			}
		}
		else if (scanCode == 0x48 && !isNanoState) //up key
		{
			if (m_history.sizeCommands > 0)
			{
				for (int i = cursorPosition - strlen(userBuffer); i < cursorPosition + 1; i++)
				{
					*(VGA_MEMORY_ADDRESS + i * 2) = 0;
				}

				setCursorPosition(cursorPosition - strlen(userBuffer));
				
				int i = 0;

				//print command
				for (i = cursorPosition; i < cursorPosition + strlen(m_history.commands[m_history.commandIndex]); i++)
				{
					*(VGA_MEMORY_ADDRESS + i*2) = m_history.commands[m_history.commandIndex][i - cursorPosition];
				}

				setCursorPosition(i);
				clean(userBuffer);
				strcpy(userBuffer, m_history.commands[m_history.commandIndex]);
				countChars = strlen(userBuffer);
				m_history.commandIndex = m_history.commandIndex == m_history.sizeCommands ? 0 : m_history.commandIndex + 1;
			}
		}
		else if (scanCode == 0x50 && !isNanoState) //down key
		{
			if (m_history.sizeCommands > 0)
			{
				for (int i = cursorPosition - strlen(userBuffer); i < cursorPosition + 1; i++)
				{
					*(VGA_MEMORY_ADDRESS + i * 2) = 0;
				}

				setCursorPosition(cursorPosition - strlen(userBuffer));
				int i = 0;
				for (i = cursorPosition; i < cursorPosition + strlen(m_history.commands[m_history.commandIndex]); i++)
				{
					*(VGA_MEMORY_ADDRESS + i * 2) = m_history.commands[m_history.commandIndex][i - cursorPosition];
				}

				setCursorPosition(i);
				clean(userBuffer);
				strcpy(userBuffer, m_history.commands[m_history.commandIndex]);
				countChars = strlen(userBuffer);
				m_history.commandIndex = m_history.commandIndex == 0 ? m_history.sizeCommands-1 : m_history.commandIndex - 1;
			}
		}
		else if (scanCode == 0x8F) // TAB pressed
		{
			printf("    ");
		}
		else if (scanCode == 0xC8 && isNanoState && (cursorPosition - VGA_WIDTH - 1) > VGA_WIDTH) // U ARROW pressed
		{
			setCursorPosition(cursorPosition - VGA_WIDTH);
		}
		else if (scanCode == 0xD0 && isNanoState && (cursorPosition - VGA_WIDTH - 1) > VGA_WIDTH) // D ARROW pressed
		{
			setCursorPosition(cursorPosition + VGA_WIDTH);
		}
		else if (scanCode == 0xCD && isNanoState && (cursorPosition - VGA_WIDTH - 1) > VGA_WIDTH) // R ARROW pressed
		{
			setCursorPosition(cursorPosition + 1);
		}
		else if (scanCode == 0xCB && isNanoState && (cursorPosition - VGA_WIDTH - 1) > VGA_WIDTH) // L ARROW pressed
		{
			setCursorPosition(cursorPosition - 1);
		}
		else
		{
			if (scanCode == left_shift_pressed || scanCode == right_shift_pressed)
			{
				m_keysStatus.m_shift = true;
			}
			else if (scanCode == left_shift_released || scanCode == right_shift_released)
			{
				m_keysStatus.m_shift = false;
			}
			else if (scanCode == capsLock_pressed)
			{
				if (m_keysStatus.m_capsLock)
					m_keysStatus.m_capsLock = false;
				else
					m_keysStatus.m_capsLock = true;
			}
			else if (scanCode == capsLock_released)
			{

			}
			else if (scanCode == left_ctrl_pressed)
			{
				m_keysStatus.m_ctrl = true;
			}
			else if (scanCode == left_ctrl_released)
			{
				m_keysStatus.m_ctrl = false;
			}
			else if (m_keysStatus.m_ctrl && (scanCode == 0x9F) && isNanoState)
			{
				isNanoState = false;
				showTimeState = !isNanoState;
				createFileUsingNano(globalParams[0]);
				shellCleanScrean(0);
				clean(userBuffer);
				printf("File saved successfully...\n\r");
			}
			else if (m_keysStatus.m_ctrl && (scanCode == 0xAD) && isNanoState)
			{
				isNanoState = false;
				showTimeState = !isNanoState;
				shellCleanScrean(0);
				clean(userBuffer);
				printf("Exit without saving file...");
			}
		}
	}

	outb(0x20, 0x20);
	outb(0xa0, 0x20);
}

bool createFileUsingNano(char* fileName)
{
	char fullPath[150] = { 0 };
	char VGAbuffer[VGA_HEIGHT * VGA_WIDTH] = { 0 };
	FILE fileHandler;
	getFullPath(fileName, fullPath);

	createVGAbuffer(VGAbuffer, (2 * VGA_WIDTH) * 2);
	if (!isPathExist(fullPath))
	{
		// If it is a new file we create one new
		createFile(fullPath);
	}
	writeFile(fullPath, VGAbuffer);
	return true;
}
