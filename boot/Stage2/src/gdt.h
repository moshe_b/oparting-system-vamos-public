#ifndef _GDT_H_INCLUDED
#define _GDT_H_INCLUDED

#include "Typedefs.h"
#include "string.h"
#include "tss.h"
#include "vga.h"
#include "user.h"

//! maximum amount of descriptors allowed
#define MAX_DESCRIPTORS					10

//! set access bit
#define I86_GDT_DESC_ACCESS			0x0001			//00000001

//! descriptor is readable and writable. default: read only
#define I86_GDT_DESC_READWRITE			0x0002			//00000010

//! set expansion direction bit
#define I86_GDT_DESC_EXPANSION			0x0004			//00000100

//! executable code segment. Default: data segment
#define I86_GDT_DESC_EXEC_CODE			0x0008			//00001000

//! set code or data descriptor. defult: system defined descriptor
#define I86_GDT_DESC_CODEDATA			0x0010			//00010000

//! set dpl bits
#define I86_GDT_DESC_DPL			0x0060			//01100000

//! set "in memory" bit
#define I86_GDT_DESC_MEMORY			0x0080			//10000000

/**	gdt descriptor grandularity bit flags	***/

//! masks out limitHi (High 4 bits of limit)
#define I86_GDT_GRAND_LIMITHI_MASK		0x0f			//00001111

//! set os defined bit
#define I86_GDT_GRAND_OS			0x10			//00010000

//! set if 32bit. default: 16 bit
#define I86_GDT_GRAND_32BIT			0x40			//01000000

//! 4k grandularity. default: none
#define I86_GDT_GRAND_4K			0x80			//10000000

//! gdt descriptor. A gdt descriptor defines the properties of a specific
//! memory block and permissions.
#pragma pack (push, 1)
typedef struct gdt_descriptor {

	//! bits 0-15 of segment limit
	uint_16		limit;

	//! bits 0-23 of base address
	uint_16		baseLo;
	uint_8			baseMid;

	//! descriptor access flags
	uint_8			flags;

	uint_8			grand;

	//! bits 24-32 of base address
	uint_8			baseHi;
}gdt_descriptor;
#pragma pack (pop, 1)

//! Setup a descriptor in the Global Descriptor Table
void gdt_set_descriptor(uint_32 i, uint_64 base, uint_64 limit, uint_8 access, uint_8 grand);

//! returns descritor
gdt_descriptor* i86_gdt_get_descriptor (int i);

//! initializes gdt
int i86_gdt_initialize ();

#endif
