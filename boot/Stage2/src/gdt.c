
#include "gdt.h"

extern void tss_flush();

//! processor gdtr register points to base of gdt. This helps
//! us set up the pointer
#pragma pack (push, 1)
typedef struct{

	//! size of gdt
	uint_16		m_limit;

	//! base address of gdt
	uint_32		m_base;
}gdtr;
#pragma pack (pop, 1)
//! global descriptor table is an array of descriptors
struct gdt_descriptor	_gdt[MAX_DESCRIPTORS];

//! gdtr data
gdtr	_gdtr;

//! install gdtr
void gdt_install();

void InstallGDT(uint_32 gdtr);
//! install gdtr
void gdt_install() {
	InstallGDT((uint_32)&_gdtr);
}

//! Setup a descriptor in the Global Descriptor Table
void gdt_set_descriptor(uint_32 i, uint_64 base, uint_64 limit, uint_8 access, uint_8 grand)
{
	if (i > MAX_DESCRIPTORS)
		return;

	//! null out the descriptor
	memset((void*)&_gdt[i], 0, sizeof(gdt_descriptor));

	//! set limit and base addresses
	_gdt[i].baseLo = (uint_16)(base & 0xffff);
	
_gdt[i].baseMid = (uint_8)((base >> 16) & 0xff);
	_gdt[i].baseHi = (uint_8)((base >> 24) & 0xff);
	_gdt[i].limit = (uint_16)(limit & 0xffff);
	//! set flags and grandularity bytes
	_gdt[i].flags = access;
	_gdt[i].grand = (uint_8)((limit >> 16) & 0x0f);
	_gdt[i].grand |= grand & 0xf0;
}


//! returns descriptor in gdt
gdt_descriptor* i86_gdt_get_descriptor(int i) {

	if (i > MAX_DESCRIPTORS)
		return 0;

	return &_gdt[i];
}


//! initialize gdt
int i86_gdt_initialize() {

	//! set up gdtr
	_gdtr.m_limit = (sizeof(gdt_descriptor) * MAX_DESCRIPTORS) - 1;
	_gdtr.m_base = (uint_32)&_gdt[0];


	gdt_set_descriptor(0, 0, 0, 0, 0);                // Null segment
	gdt_set_descriptor(1, 0, 0xFFFFFFFF, 0x9A, 0xCF); // Code segment
	gdt_set_descriptor(2, 0, 0xFFFFFFFF, 0x92, 0xCF); // Data segment
	gdt_set_descriptor(3, 0, 0xFFFFFFFF, 0xFA, 0xCF); // User mode code segment
	gdt_set_descriptor(4, 0, 0xFFFFFFFF, 0xF2, 0xCF); // User mode data segment
	
	//! install gdtr
	gdt_install();
	return 0;
}
