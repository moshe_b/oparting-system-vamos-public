#ifndef DISK_H
#define DISK_H

#include "Typedefs.h"
#include "vga.h"
#include "inputOutput.h"

#define ATA_DEVICES_MAX 3
#define IDENTIFY_PORT 0xEC

#define ATA_PRIMARY_DATA                0x1F0
#define ATA_PRIMARY_ERR_OFFSET          1
#define ATA_PRIMARY_SECCOUNT_OFFSET     2
#define ATA_PRIMARY_LBA_LO_OFFSET       3
#define ATA_PRIMARY_LBA_MID_OFFSET      4
#define ATA_PRIMARY_LBA_HI_OFFSET       5
#define ATA_PRIMARY_DRIVE_HEAD_OFFSET   6
#define ATA_PRIMARY_COMM_REGSTAT_OFFSET 7
#define ATA_PRIMARY_ALTSTAT_DCR         0x3F6

#define PIO_SECTOR_WRITE_CMD 0x30
#define PIO_SECTOR_READ_CMD  0x20
#define SLAVE_BIT_CHECK      0xA0
#define MAX_ATA_META_BUFFER 256

#define STAT_ERR  (1 << 0) // Indicates an error occurred. Send a new command to clear it
#define STAT_DRQ  (1 << 3) // Set when the drive has PIO data to transfer, or is ready to accept PIO data.
#define STAT_SRV  (1 << 4) // Overlapped Mode Service Request.
#define STAT_DF   (1 << 5) // Drive Fault Error (does not set ERR).
#define STAT_RDY  (1 << 6) // Bit is clear when drive is spun down, or after an error. Set otherwise.
#define STAT_BSY  (1 << 7) // Indicates the drive is preparing to send/receive data (wait for it to clear).
// In case of 'hang' (it never clears), do a software reset.
#define TRANSFER_DATA_CHUNCK 256
#define BSY_PORT 0x80

typedef struct tATA {
	uint_16 port;
	uint_16 slavebit;
	uint_32 LBA28;
	uint_64 LBA48;
	uint_8  isLBA48Supported
} ATA;



/*Initialize ATA drivers and their types*/
void ATAinit();
void detectATA();
void detectDevtype(int port, int slavebit);

/*Read and Write Functions that devide r/w 28/48 mode*/
void ataRead(int driveID, uint_32 LBA, int countSector, char* addr);
void ataWrite(int driveID, uint_32 LBA, int bufferSize, char* addr);

/*LBA 28 R/W mode*/
void ataRead28(int driveID, uint_32 LBA, int countSector, char* addr);
void ataWrite28(int driveID, uint_32 LBA, int bufferSize, char* addr);

/*LBA 48 R/W mode*/
void ataRead48(int driveID, uint_64 LBA, int countSector, char* addr);
void ataWrite48(int driveID, uint_64 LBA, int countSector, char* addr);




#endif // !DISK_H



