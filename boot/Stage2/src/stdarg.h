#ifndef VA_ARG_H
#define VA_ARG_H

#include "Typedefs.h"

#define STACKITEM int

#define	VA_SIZE(TYPE)					\
	((sizeof(TYPE) + sizeof(STACKITEM) - 1)	\
		& ~(sizeof(STACKITEM) - 1))

#define va_start(ap, param) \
	(ap=((va_list)&(param) + VA_SIZE(param)))

#define va_end(ap) (void)(ap = NULL)

#define va_arg(ap, type) \
	(ap += VA_SIZE(type), *((type*)(ap - VA_SIZE(type))))

#endif // !VA_ARG_H
