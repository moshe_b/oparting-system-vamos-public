#ifndef TSS_H
#define TSS_H

#include "Typedefs.h"
#include "string.h"
#include "gdt.h"

void createTss(uint_32 indexGdt, uint_16 ss0, uint_32 esp0);

struct tssEntryStruct
{
	uint_32 prev_tss;   // The previous TSS - if we used hardware task switching this would form a linked list.
	uint_32 esp0;       // The stack pointer to load when we change to kernel mode.
	uint_32 ss0;        // The stack segment to load when we change to kernel mode.
	uint_32 esp1;       // Unused...
	uint_32 ss1;
	uint_32 esp2;
	uint_32 ss2;
	uint_32 cr3;
	uint_32 eip;
	uint_32 eflags;
	uint_32 eax;
	uint_32 ecx;
	uint_32 edx;
	uint_32 ebx;
	uint_32 esp;
	uint_32 ebp;
	uint_32 esi;
	uint_32 edi;
	uint_32 es;         // The value to load into ES when we change to kernel mode.
	uint_32 cs;         // The value to load into CS when we change to kernel mode.
	uint_32 ss;         // The value to load into SS when we change to kernel mode.
	uint_32 ds;         // The value to load into DS when we change to kernel mode.
	uint_32 fs;         // The value to load into FS when we change to kernel mode.
	uint_32 gs;         // The value to load into GS when we change to kernel mode.
	uint_32 ldt;        // Unused...
	uint_16 trap;
	uint_16 iomap_base;
};

typedef struct tssEntryStruct tssEntry;

#endif