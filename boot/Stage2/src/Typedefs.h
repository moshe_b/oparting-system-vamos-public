#ifndef TYPE_DEF
#define TYPE_DEF

typedef unsigned char uint_8;
typedef unsigned short uint_16;
typedef unsigned int uint_32;
typedef unsigned long long uint_64;

#define NULL 0

typedef enum {false, true} bool;

typedef struct
{
	uint_32 x;
	uint_32 y;
}Point;


#endif