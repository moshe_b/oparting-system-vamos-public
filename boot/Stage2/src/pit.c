#include "IDT.h"
#include "pit.h"
#include "pic.h"
#include "kernel.h"
//-----------------------------------------------
//	Controller Registers
//-----------------------------------------------

#define		I86_PIT_REG_COUNTER0		0x40
#define		I86_PIT_REG_COUNTER1		0x41
#define		I86_PIT_REG_COUNTER2		0x42
#define		I86_PIT_REG_COMMAND			0x43

extern void isr_pit();

//! Global Tick count
static uint_32			_pit_ticks=0;

//! Test if pit is initialized
static int _pit_bIsInit=0;
extern idt_descriptor	_idt[I86_MAX_INTERRUPTS];

void timer_wait(int ticks)
{
	unsigned int eticks;

	eticks = _pit_ticks + ticks*1000;
	while (_pit_ticks < eticks);
}

void interruptdone(unsigned int intno) {

	//! insure its a valid hardware irq
	if (intno > 16)
		return;

	//! test if we need to send end-of-interrupt to second pic
	if (intno >= 8)
		i86_pic_send_command(I86_PIC_OCW2_MASK_EOI, 1);

	//! always send end-of-interrupt to primary pic
	i86_pic_send_command(I86_PIC_OCW2_MASK_EOI, 0);
}

extern void i86_pit_irq();
//!	pit timer interrupt handler
void i86_pit_irq () {
	//! increment tick count
	_pit_ticks++;

	//! tell hal we are done
	outb(0x20, 0x20);
	outb(0xa0, 0x20);
}

//! Sets new pit tick count and returns prev. value
uint_32 i86_pit_set_tick_count (uint_32 i) {

	uint_32 ret = _pit_ticks;
	_pit_ticks = i;
	return ret;
}


//! returns current tick count
uint_32 get_tick_count () {
	return _pit_ticks;
}


//! send command to pic
void i86_pit_send_command (uint_8 cmd) {

	outb (I86_PIT_REG_COMMAND, cmd);
}


//! send data to a counter
void i86_pit_send_data (uint_16 data, uint_8 counter) {

	uint_8	port= (counter==I86_PIT_OCW_COUNTER_0) ? I86_PIT_REG_COUNTER0 :
		((counter==I86_PIT_OCW_COUNTER_1) ? I86_PIT_REG_COUNTER1 : I86_PIT_REG_COUNTER2);

	outb (port, (uint_8)data);
}


//! read data from counter
uint_8 i86_pit_read_data (uint_16 counter) {

	uint_8	port= (counter==I86_PIT_OCW_COUNTER_0) ? I86_PIT_REG_COUNTER0 :
		((counter==I86_PIT_OCW_COUNTER_1) ? I86_PIT_REG_COUNTER1 : I86_PIT_REG_COUNTER2);

	return inb (port);
}


//! starts a counter
void i86_pit_start_counter (uint_32 freq, uint_8 counter, uint_8 mode) {

	if (freq==0)
		return;

	uint_16 divisor = (uint_16)(1193181 / (uint_16)freq);

	//! send operational command
	uint_8 ocw=0;
	ocw = (ocw & ~I86_PIT_OCW_MASK_MODE) | mode;
	ocw = (ocw & ~I86_PIT_OCW_MASK_RL) | I86_PIT_OCW_RL_DATA;
	ocw = (ocw & ~I86_PIT_OCW_MASK_COUNTER) | counter;
	i86_pit_send_command (ocw);

	//! set frequency rate
	i86_pit_send_data (divisor & 0xff, 0);
	i86_pit_send_data ((divisor >> 8) & 0xff, 0);

	//! reset tick count
	_pit_ticks=0;
}

//! initialize minidriver
void i86_pit_initialize () {

	_idt[32].baseLo = (uint_16)((uint_32)&isr_pit & 0xffff);
	_idt[32].baseHi = (uint_16)(((uint_32)&isr_pit >> 16) & 0xffff);
	_idt[32].reserved = 0;
	_idt[32].flags = (uint_8)(I86_IDT_DESC_PRESENT | I86_IDT_DESC_BIT32 | I86_IDT_DESC_RING3);
	_idt[32].sel = 0x8;
	_pit_bIsInit = 1;
}