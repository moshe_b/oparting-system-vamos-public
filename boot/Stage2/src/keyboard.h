
#ifndef  KEYBOARD_H
#define KEYBOARD_H

#include "Typedefs.h"
#include "vga_codes.h"
#include "va_list.h"
#include "stdarg.h"
#include "sound.h"
#include "string.h"
#include "disk.h"
#include "physicalMemory.h"
#include "fs.h"
#include "IDT.h"
#include "pit.h"

//constants
#define MAX_BUFFER_COMMAND 256
#define MAX_PARAMS 10  //maximum 10 parameters in a command
#define MAX_LEN_PARAM 50  //maximum 50 characters per parameter
#define MAX_LEN_COMMAND 50 //commands like echo, whoami... 
#define MAX_NUMBER_SHELL_COMMANDS 30

// error/response command
#define INVALID_COMMAND_NAME 1
#define NUMBER_PARAMS_NOT_VALID 2
#define VALID_COMMAND 3
#define NANO_COMMAND 4

//structs and typedefs
typedef char Params[MAX_PARAMS][MAX_LEN_PARAM];

// Shell Function pointers
typedef void (*ShellFunction)(int);

// function Struct
struct functionStruct
{
	char functionName[100];
	int numParams;
	ShellFunction function;
};

typedef struct functionStruct functionStruct;

typedef struct ShellCommand
{
	int currNumShellCommands;
	functionStruct functions[MAX_NUMBER_SHELL_COMMANDS];
}ShellCommand;

typedef struct
{
	DIR currDir;
	char pathDir[150];
}currPath;


typedef struct
{
	char commands[100][MAX_BUFFER_COMMAND];
	uint_32 sizeCommands;
	uint_32 commandIndex;
}historyCommands;

//private functions
static void clean(char* var);
static void enterHandler(char userBuffer[]);
void initShellCommands();
static void printListDir(char* path);
static void insertCommand();
static void textShiftHandling();
static void printPath();
void getFullPath(char* pathInput, char* fullPathSave);
void printColorsMenu();
void deleteDirRecursive(char* path);

//shell commands
static void shellCd(int numParams);
static void shellListDir(int numParams);
static void shellTouch(int numParams);
static void shellTree(int numParams);
static void shellMkdir(int numParams);
static void shellRemoveFile(int numParams);
static void shellRemoveDir(int numParams);
static int shellCopyFile(int numParams);
static void shellMoveFile(int numParams);
static void shellMoveFile(int numParams);
static void shellCat(int numParams);
static void shellTickCounter(int NumParams);
static void shellWhoami(int NumParams);
static void shellEcho(int NumParams);
static void shellBeep(int NumParams);
static void shellHelp(int numParams);
static void shellCleanScrean(int numParams);
static void shellReadDisk(int numParams);
static void shellWriteDisk(int numParams);
static void shellNano(int numParams);
static void shellColor(int numParams);

//shell commands handler
static int handleCommand(char* userBuff);

//public functions
void isrKeyboardHandler();
void initUserInterface();
bool createFileUsingNano(char* fileName);

#endif // ! KEYBOARD_H
