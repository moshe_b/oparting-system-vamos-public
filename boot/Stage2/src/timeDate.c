#include "timeDate.h"

#define CURRENT_YEAR        2022

int century_register = 0x00;                                // Set by ACPI table parsing code if possible

DATE vamosDate;
unsigned int year;

int get_update_in_progress_flag() {
    outb(cmos_address, 0x0A);
    return (inb(cmos_data) & 0x80);
}

unsigned char get_RTC_register(int reg) {
    outb(cmos_address, reg);
    return inb(cmos_data);
}

void readRTC() {
    unsigned char century;
    unsigned char last_second;
    unsigned char last_minute;
    unsigned char last_hour;
    unsigned char last_day;
    unsigned char last_month;
    unsigned int last_year;
    unsigned char last_century;
    unsigned char registerB;

    // Note: This uses the "read registers until you get the same values twice in a row" technique
    //       to avoid getting dodgy/inconsistent values due to RTC updates

    while (get_update_in_progress_flag());                // Make sure an update isn't in progress
    vamosDate.m_second = get_RTC_register(0x00);
    vamosDate.m_minute = get_RTC_register(0x02);
    vamosDate.m_hour = get_RTC_register(0x04);
    vamosDate.m_day = get_RTC_register(0x07);
    vamosDate.m_month = get_RTC_register(0x08);
    year = get_RTC_register(0x09);
    if (century_register != 0) {
        century = get_RTC_register(century_register);
    }

    do {
        last_second = vamosDate.m_second;
        last_minute = vamosDate.m_minute;
        last_hour = vamosDate.m_hour;
        last_day = vamosDate.m_day;
        last_month = vamosDate.m_month;
        last_year = year;
        last_century = century;

        while (get_update_in_progress_flag());           // Make sure an update isn't in progress
        vamosDate.m_second = get_RTC_register(0x00);
        vamosDate.m_minute = get_RTC_register(0x02);
        vamosDate.m_hour = get_RTC_register(0x04);
        vamosDate.m_day = get_RTC_register(0x07);
        vamosDate.m_month = get_RTC_register(0x08);
        year = get_RTC_register(0x09);
        if (century_register != 0) {
            century = get_RTC_register(century_register);
        }
    } while ((last_second != vamosDate.m_second) || (last_minute != vamosDate.m_minute) || (last_hour != vamosDate.m_hour) ||
        (last_day != vamosDate.m_day) || (last_month != vamosDate.m_month) || (last_year != year) ||
        (last_century != century));

    registerB = get_RTC_register(0x0B);

    // Convert BCD to binary values if necessary

    if (!(registerB & 0x04)) {
        vamosDate.m_second = (vamosDate.m_second & 0x0F) + ((vamosDate.m_second / 16) * 10);
        vamosDate.m_minute = (vamosDate.m_minute & 0x0F) + ((vamosDate.m_minute / 16) * 10);
        vamosDate.m_hour = ((vamosDate.m_hour & 0x0F) + (((vamosDate.m_hour & 0x70) / 16) * 10)) | (vamosDate.m_hour & 0x80);
        vamosDate.m_day = (vamosDate.m_day & 0x0F) + ((vamosDate.m_day / 16) * 10);
        vamosDate.m_month = (vamosDate.m_month & 0x0F) + ((vamosDate.m_month / 16) * 10);
        year = (year & 0x0F) + ((year / 16) * 10);
        if (century_register != 0) {
            century = (century & 0x0F) + ((century / 16) * 10);
        }
    }

    // Convert 12 hour clock to 24 hour clock if necessary

    if (!(registerB & 0x02) && (vamosDate.m_hour & 0x80)) {
        vamosDate.m_hour = ((vamosDate.m_hour & 0x7F) + 12) % 24;
    }

    // Calculate the full (4-digit) year

    if (century_register != 0) {
        year += century * 100;
    }
    else {
        year += (CURRENT_YEAR / 100) * 100;
        if (year < CURRENT_YEAR) year += 100;
    }

    vamosDate.m_year = year;
}