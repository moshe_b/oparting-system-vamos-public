; Remember the memory map-- 0x500 through 0x7bff is unused above the BIOS data area.
; We are loaded at 0x500 (0x50:0)
;[SECTION .text]


jmp EnterProtectedMode
;*******************************************************
;	Preprocessor directives
;*******************************************************

;%include "stdio.inc"			; basic i/o routines
%include "src/Gdt.asm"			; Gdt routines
%include "src/memory.asm"
;%include "src/Stage2_1.asm"
;%include "A20.asm"


;*******************************************************
;	Data Section
;*******************************************************

LoadingMsg db "Preparing to load operating system...", 0x0D, 0x0A, 0x00

;*******************************************************
;	STAGE 2 ENTRY POINT
;
;		-Store BIOS information
;		-Load Kernel
;		-Install GDT; go into protected mode (pmode)
;*******************************************************

printNum:
	mov bh, 10
	xor cx, cx
	getDigits:
		div bh
		push ax
		
		xor ah, ah
		inc cx
		cmp al, 0
		jne getDigits
	
	printBios:
		pop dx
		mov ah, 02h
		int 21h
		loop printBios
	
	ret
		
	


; multiboot structure
; important variables in it:
; 1. memoryLo, memoryHi - contain the size of the memory
;    memoryLo - size of memory in kb between 1Mb address to 16MB address.
; 	 memoryHi - size of memory in 64kb blocks above 16MB address
; 2. bootDevice - contain the device that did the boot process. 0x80 for hard drive.
; 3. mmap_length  - the length of the memory mapping buffer store in es:di
; 4. mmap_addr  - the address of the begining of the mapping memory buffer (es:di)
boot_info:
istruc multiboot_info
	; at multiboot_info.flags,			dd 0
	at multiboot_info.memoryLo,			dd 0
	at multiboot_info.memoryHi,			dd 0
	at multiboot_info.bootDevice,		dd 0
	; at multiboot_info.cmdLine,			dd 0
	; at multiboot_info.mods_count,		dd 0
	; at multiboot_info.mods_addr,		dd 0
	; at multiboot_info.syms0,			dd 0
	; at multiboot_info.syms1,			dd 0
	; at multiboot_info.syms2,			dd 0
	at multiboot_info.mmap_length,		dd 0
	at multiboot_info.mmap_addr,		dd 0
	; at multiboot_info.drives_length,	dd 0
	; at multiboot_info.drives_addr,		dd 0
	; at multiboot_info.config_table,		dd 0
	; at multiboot_info.bootloader_name,	dd 0
	; at multiboot_info.apm_table,		dd 0
	; at multiboot_info.vbe_control_info,	dd 0
	; at multiboot_info.vbe_mode_info,	dw 0
	; at multiboot_info.vbe_interface_seg, dw 0
	;at multiboot_info.vbe_interface_off, dw 0
	;at multiboot_info.vbe_interface_len, dw 0
iend


EnterProtectedMode: 
	call EnableA20
	cli ; disable interrupt
	lgdt [gdt_descriptor]
	
	; ; 0x80 for hard drives
	 mov ax, 0x80
	 mov     [boot_info+multiboot_info.bootDevice], ax
	
	; get the memory size on the system from bios interrupts
	 xor		eax, eax
	 xor		ebx, ebx
	 call	BiosGetMemorySize64MB

	; ; memoryLo and memoryHi contain the size of the memory in the computer
	 mov		word [boot_info+multiboot_info.memoryHi], bx
	 mov		word [boot_info+multiboot_info.memoryLo], ax

	; ; get mapping of all the memory in the computer.
	; ; the mapping is in address 0x0:0x1000 and contain MemoryMapEntry
	; ; to describe the memory
	 mov		ax, 0x100
	 mov		es, ax
	 mov		di, 0x5000
	 call	BiosGetMemoryMap
	
	; mov ax, bp
	; call printNum
	
	 mov eax, cr0
	 or eax, 1
	 mov cr0, eax

	jmp CODE_SEG:StartProtectedMode ; jmp far jump
	
; Line 20 At CPU using port
EnableA20:
	in al, 0x92
	or al, 2
	out 0x92, al
	ret


[bits 32]
StartProtectedMode:
	mov	ax, DATA_SEG		; set data segments to data selector (0x10)
	mov	ds, ax
	mov	ss, ax
	mov	es, ax
	mov	esp, 9000h	
	
	mov ebx, 0xb8002
	mov cl, 'H'
	mov [ebx], cl
   ; [extern _start]
;	mov ax, [boot_info+multiboot_info.memoryHi]
	[extern loadStage2]
	push	dword boot_info
	call loadStage2
	;call _start             	      ; Execute Kernel
	add		esp, 4

	
	jmp $


; loadStage2:
	; ;mov eax, 1000
	; ;mov cl, 70
	; ;mov rdi, 0x9000
	; ;call ata_lba_read
	; mov ebx, 0xb8000
	; mov cl, 'H'
	; mov [ebx], cl
	; call loadkernelEntry
	; jmp $
	; ;Global loadStage2
	
loadKernel:
	push 0x9000
	ret
	Global loadKernel
	
times 2048 - ($ - $$)  db 0
	