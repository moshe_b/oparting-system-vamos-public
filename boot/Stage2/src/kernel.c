#include "kernel.h"

#define DEMO_OFFSET 250
#define MAX_BOOT_INFO_CHECKS 20
#define MEM_BASE_KERNEL_ADDR 0x100000
#define SECTOR_SIZE 512
#define MEM_HIGH_MUL 64
#define MEM_REGION_ADDR 0x6000
#define MIN_TICK_COUNTER 5000
#define REGION_FOR_USE 1
#define MAX_SECONDS_IN_MIN 60

extern idt_descriptor _idt[I86_MAX_INTERRUPTS];
extern void isr2();
extern char* strMemoryTypes[];

extern currPath m_currPath;
extern DATE vamosDate;
extern COLOR m_OsColor;
extern int cPositionPrinting;
extern void tss_flush();

bool showTimeState = true;

void _start(multiboot_info* boot_info) {
	uint_32 memSize = 1024 + boot_info->memoryLo + boot_info->memoryHi * MEM_HIGH_MUL;
	pmmngr_init(memSize, MEM_BASE_KERNEL_ADDR + DEMO_OFFSET * SECTOR_SIZE);
	memory_region* region = (memory_region*)MEM_REGION_ADDR;

	/*Physical memory check from boot info*/
	for (int i = 0; i < MAX_BOOT_INFO_CHECKS; i++)
	{
		if (region[i].type > 4)
			region[i].type = REGION_FOR_USE;

		if (i > 0 && region[i].startLo == 0)
			break;

		// if region is avilable memory, initialize the region for use
		if (region[i].type == REGION_FOR_USE)
			pmmngr_init_region(region[i].startLo, region[i].sizeLo);
		else
			pmmngr_deinit_region(region[i].startLo, region[i].sizeLo); //reserved memory or apci memory
	}

	vmm_initialize();


    init_vga(FORGROUND_WHITE, BACKGROUND_BLACK);
	i86_gdt_initialize();
	i86_idt_initialize(0x8);
	i86_pic_initialize(0x20, 0x28);
	i86_pit_initialize();
	i86_pit_start_counter(200, I86_PIT_OCW_COUNTER_0, I86_PIT_OCW_MODE_SQUAREWAVEGEN);
	
	disable();
	_idt[0].baseLo = (uint_16)((uint_32)&isr2 & 0xffff);
	_idt[0].baseHi = (uint_16)(((uint_32)&isr2 >> 16) & 0xffff);
	_idt[0].reserved = 0;
	_idt[0].flags = (uint_8)(I86_IDT_DESC_PRESENT | I86_IDT_DESC_BIT32 | I86_IDT_DESC_RING3);
	_idt[0].sel = 0x8;
	enable();

	printf("VAM-OS 32-bit [Version 1.1.0]\n\r");
	printf("(c) VAM-OS Corporation.All rights reserved. Command shell V1.1.0\n\r");

	ATAinit();
	initFS();
	initUserInterface();
	readRTC();
	createTss(5, 0x10, 0x9000);
	tss_flush();

	int prevTickCounter = 0;

	while (true)
	{
		if (get_tick_count() - prevTickCounter > MIN_TICK_COUNTER)
		{
			prevTickCounter = get_tick_count();
			
			if (vamosDate.m_second < MAX_SECONDS_IN_MIN - 1)
			{
				vamosDate.m_second++;
			}
			else
			{
				vamosDate.m_second = 0;
				vamosDate.m_minute += 1;
			}

			if (vamosDate.m_minute == MAX_SECONDS_IN_MIN)
			{
				vamosDate.m_minute = 0;
				vamosDate.m_hour++;
			}

			cPositionPrinting = positionFromCoords(70, 1); // {x = 70 y = 1} VGA(X,Y)
			for (int i = cPositionPrinting; i < cPositionPrinting + 8; i++)
				*(VGA_MEMORY_ADDRESS + i) = 0;

			COLOR colortemp;
			colortemp.m_backColor = m_OsColor.m_backColor;
			colortemp.m_forColor = m_OsColor.m_forColor;

			setOsColor(FORGROUND_CYAN, BACKGROUND_BLACK);
			if(showTimeState)
				printf("%02d:%02d:%02d", vamosDate.m_hour, vamosDate.m_minute, vamosDate.m_second);
			setOsColor(colortemp.m_forColor, colortemp.m_backColor);
			cPositionPrinting = -1;
		}
	}
	
	return;	
}





