#ifndef MOUSE_H
#define MOUSE_H

#include "Typedefs.h"
#include "inputOutput.h"
#include "vga.h"
#include "pit.h"
#include "IDT.h"
#include "sound.h"

/*** defines ***/
#define MOUSE_IRQ 12

//macros compaq status (command ps2)
#define MACRO_MOUSE_INTERRUPT_ENABLE 0x2
#define MACRO_ENABLE_MOUSE 0x20

//macros first packet of mouse
#define MACRO_LEFT_BTN 1
#define MACRO_RIGHT_BTN 2
#define MACRO_MIDDLE_BTN 4
#define MACRO_ALWAYSE_ONE 8  //indicate that the first packet value is valid 
#define MACRO_X_SIGN_BIT 0x10
#define MACRO_Y_SIGN_BIT 0x20
#define MACRO_X_OVERFLOW 0x40
#define MACRO_Y_OVERFLOW 0x80

//ps2 mouse commands
#define COMMAND_GET_COMMAND_BYTE 0x20
#define COMMAND_ENABLE_MOUSE_PORT 0xA8
#define COMMAND_WRITE_COMMAND_BYTE 0x60
#define COMMAND_SET_DEFAULT_ATTRIB 0xF6 // Disables streaming, sets the packet rate to
										//100 per second, and resolution to 4 pixels per mm

#define COMMAND_ENABLE_PACKET_STREAMING 0XF4 // The mouse starts sending automatic packets 
											//when the mouse moves or is clicked

//registers

#define COMMAND_STATUS_REG 0x64
#define DATA_REG 0x60

/*  functions */
void mouseInit();
void mouseHandler();
void mouseInputWait();
void mouseOutputWait();
void mouseWriteCommand(uint_8 command);
void mouseHandler();
void processMouseData();
uint_8 mouseRead();
#endif // !MOUSE_H
