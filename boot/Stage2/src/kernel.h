#ifndef KERNEL_H
#define KERNEL_H


#include "inputOutput.h"
#include "vga_codes.h"
#include "idt.h"
#include "pic.h"
#include "pit.h"
#include "gdt.h"
#include "vga.h"
#include "Typedefs.h"
#include "physicalMemory.h"
#include "vmm.h"
#include "fs.h"
#include "mouse.h"
#include "timeDate.h"
#include "tss.h"
#include "user.h"

#endif