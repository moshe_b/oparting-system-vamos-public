#ifndef VMM_H
#define VMM_H

//includes
#include "Typedefs.h"
#include "pde.h"
#include "pte.h"
#include "physicalMemory.h"
#include "vga.h"

//externs variables

//typedefs and defines
typedef uint_32 virtual_address;
//! i86 architecture defines 1024 entries per table--do not change
#define PAGES_PER_TABLE 1024
#define PAGES_PER_DIR	1024

//two macros for the virtual address.
// virtual adress = [10 bits directory table index] [10 bits page table index] [12 bits offset to the page table]
#define PAGE_DIRECTORY_INDEX(x) (((x) >> 22))
#define PAGE_TABLE_INDEX(x) (((x) >> 12) & 0x3ff)

//get the physical address from page table entry/ directory table entry
// directory and page table contain 20 bits of physical address and 12 bits of permissions
//every entry is int (32 bits)
#define PAGE_GET_PHYSICAL_ADDRESS(x) ((*x) & ~0xfff)

//! page table represents 4mb address space
#define PTABLE_ADDR_SPACE_SIZE 0x400000

//! directory table represents 4gb address space
#define DTABLE_ADDR_SPACE_SIZE 0x100000000

//! page sizes are 4KB - there is not support in huge pages (4 MB pages)
#define PAGE_SIZE 4096

#define SET_ATTRIBUTE(entry, attr) (*entry |= attr)
#define CLEAR_ATTRIBUTE(entry, attr) (*entry &= ~attr)
#define TEST_ATTRIBUTE(entry, attr) (*entry & attr)
#define SET_FRAME(entry, address) (*entry = (*entry & ~0x7FFFF000) | address)   // Only set address/frame, not flags


//! page table - contain 1024 entries
typedef struct ptable {
	pt_entry m_entries[PAGES_PER_TABLE];
}ptable;

//! page directory - contain 1024 entries
typedef struct pdirectory {

	pd_entry m_entries[PAGES_PER_DIR];
}pdirectory;

/*
the memory is calculated by:
1024*1024*(4*1024) = 4GB of virtual memory for every process
*/

//function declerations
int vmm_allocate_page(pt_entry* e);
void vmm_free_page(pt_entry* e);

//gets
pt_entry* vmm_get_pt_entry(ptable*	p_table, virtual_address address);
pd_entry* vmm_get_pd_entry(pdirectory* p_table, virtual_address address);
pdirectory* get_current_directory();

int switch_directory(pdirectory* new_dir);
void vmm_map_page(void* phys, void* virt);
int vmm_initialize();
#endif // !VMM_H
