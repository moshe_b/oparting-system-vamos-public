; Remember the memory map-- 0x500 through 0x7bff is unused above the BIOS data area.
; We are loaded at 0x500 (0x50:0)
[SECTION .text]

[bits 32]
jmp StartProtectedMode 

;*******************************************************
;	Preprocessor directives
;*******************************************************

;%include "stdio.inc"			; basic i/o routines
%include "src/Gdt.asm"			; Gdt routines
%include "src/memory.asm"
;%include "A20.asm"


;*******************************************************
;	Data Section
;*******************************************************

LoadingMsg db "Preparing to load operating system...", 0x0D, 0x0A, 0x00

;*******************************************************
;	STAGE 2 ENTRY POINT
;
;		-Store BIOS information
;		-Load Kernel
;		-Install GDT; go into protected mode (pmode)
;*******************************************************

printNum:
	mov bh, 10
	xor cx, cx
	getDigits:
		div bh
		push ax
		
		xor ah, ah
		inc cx
		cmp al, 0
		jne getDigits
	
	printBios:
		pop dx
		mov ah, 02h
		int 21h
		loop printBios
	
	ret
		
	


; multiboot structure
; important variables in it:
; 1. memoryLo, memoryHi - contain the size of the memory
;    memoryLo - size of memory in kb between 1Mb address to 16MB address.
; 	 memoryHi - size of memory in 64kb blocks above 16MB address
; 2. bootDevice - contain the device that did the boot process. 0x80 for hard drive.
; 3. mmap_length  - the length of the memory mapping buffer store in es:di
; 4. mmap_addr  - the address of the begining of the mapping memory buffer (es:di)
boot_info:
istruc multiboot_info
	; at multiboot_info.flags,			dd 0
	at multiboot_info.memoryLo,			dd 0
	at multiboot_info.memoryHi,			dd 0
	at multiboot_info.bootDevice,		dd 0
	; at multiboot_info.cmdLine,			dd 0
	; at multiboot_info.mods_count,		dd 0
	; at multiboot_info.mods_addr,		dd 0
	; at multiboot_info.syms0,			dd 0
	; at multiboot_info.syms1,			dd 0
	; at multiboot_info.syms2,			dd 0
	at multiboot_info.mmap_length,		dd 0
	at multiboot_info.mmap_addr,		dd 0
	; at multiboot_info.drives_length,	dd 0
	; at multiboot_info.drives_addr,		dd 0
	; at multiboot_info.config_table,		dd 0
	; at multiboot_info.bootloader_name,	dd 0
	; at multiboot_info.apm_table,		dd 0
	; at multiboot_info.vbe_control_info,	dd 0
	; at multiboot_info.vbe_mode_info,	dw 0
	; at multiboot_info.vbe_interface_seg, dw 0
	;at multiboot_info.vbe_interface_off, dw 0
	;at multiboot_info.vbe_interface_len, dw 0
iend



[bits 32]
StartProtectedMode:	
	[extern _start]	
	;mov ax, [boot_info+multiboot_info.memoryHi]
	;push	dword boot_info
	jmp _start             	      ; Execute Kernel
	add		esp, 4

	
	jmp $
times 2048 - ($ - $$)  db 0
	