#ifndef TIME_DATE_H
#define TIME_DATE_H

#include "inputOutput.h"

typedef struct
{
	unsigned int m_year;
	unsigned char m_second;
	unsigned char m_minute;
	unsigned char m_hour;
	unsigned char m_day;
	unsigned char m_month;
}DATE;

enum {
	cmos_address = 0x70,
	cmos_data = 0x71
};

void readRTC();
static int get_update_in_progress_flag();
static unsigned char get_RTC_register(int reg);

#endif