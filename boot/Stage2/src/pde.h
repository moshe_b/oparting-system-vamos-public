#ifndef PDH_H
#define PDH_H

#include "Typedefs.h"
#include "physicalMemory.h"

//enum pte flags.
//we use macro for easy use
enum PDE_FLAGS
{
	PDE_PRESENT_FLAG = 1,
	PDE_IS_WRITEBLE = 2,
	PDE_IS_USER_MODE = 4,
	//resrvd is not needed because is for intel
	PDE_IS_PAGE_ACCESSED = 0x20,
	PDE_IS_4MB = 0x80,
	PDE_FRAME = 0x7FFFF000 // macro for the frame address
};

//pt_entry is only 32 bits of attributes and frame
typedef uint_32 pd_entry;

//functions decelerations

void 		pd_entry_add_attrib(pd_entry* e, uint_32 attrib);
void 		pd_entry_del_attrib(pd_entry* e, uint_32 attrib);
void 		pd_entry_set_frame(pd_entry*, physical_addr);
int 		pd_entry_is_present(pd_entry e);
int 		pd_entry_is_writable(pd_entry e);
physical_addr	pd_entry_pfn(pd_entry e);
int			pd_entry_is_user(pd_entry e);
int			pd_entry_is_4mb(pd_entry e);

#endif