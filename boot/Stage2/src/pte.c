#include "pte.h"


void pt_entry_add_attrib(pt_entry* e, uint_32 attrib)
{
	*e = *e | attrib;
}

void pt_entry_del_attrib(pt_entry* e, uint_32 attrib)
{
	*e = *e & (~attrib);
}

void pt_entry_set_frame(pt_entry* e, physical_addr frame_address)
{
	*e = frame_address | (*e & (~PTE_FRAME));
}

int pt_entry_is_present(pt_entry e)
{
	return e & PTE_PRESENT_FLAG != 0 ? 1 : 0;
}

int pt_entry_is_writable(pt_entry e)
{
	return e & PTE_IS_WRITEBLE != 0 ? 1 : 0;
}

physical_addr pt_entry_pfn(pt_entry e)
{
	physical_addr address = 0;
	address = e & PTE_FRAME;
	return address;
}
