#include "sound.h"

void io_wait(void)
{
	outb(0x80, 0);
}

void play_sound(uint_32 nFrequence) {
	uint_32 Div;
	uint_8 tmp;

	//And play the sound using the PC speaker
	tmp = inb(0x61);
	if (tmp != (tmp | 3)) {
		outb(0x61, tmp | 3);
	}
}

//make it shutup
void nosound() {
	uint_8 tmp = inb(0x61) & 0xFC;

	outb(0x61, tmp);
}

//Make a beep
void beep() {
	play_sound(1000);
	timer_wait(10);
	nosound();
}