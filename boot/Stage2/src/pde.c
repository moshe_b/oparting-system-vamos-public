#include "pde.h"

void pd_entry_add_attrib(pd_entry* e, uint_32 attrib)
{
	*e = *e | attrib;
}

void pd_entry_del_attrib(pd_entry* e, uint_32 attrib)
{
	*e = *e & (~attrib);
}

void pd_entry_set_frame(pd_entry* e, physical_addr frame_address)
{
	*e = frame_address | (*e & (~PDE_FRAME));
}

int pd_entry_is_present(pd_entry e)
{
	return e & PDE_PRESENT_FLAG != 0 ? 1 : 0;
}

int pd_entry_is_writable(pd_entry e)
{
	return e & PDE_IS_WRITEBLE != 0 ? 1 : 0;
}

physical_addr pd_entry_pfn(pd_entry e)
{
	physical_addr address = 0;
	address = e & PDE_FRAME;
	return address;
}

int pd_entry_is_user(pd_entry e)
{
	return e & PDE_IS_USER_MODE != 0 ? 1 : 0;
}

int pd_entry_is_4mb(pd_entry e)
{
	return e & PDE_IS_4MB != 0 ? 1 : 0;
}
