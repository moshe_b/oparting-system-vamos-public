#ifndef INPUT_OUTPUT
#define INPUT_OUTPUT

#include "Typedefs.h"
/*I/O inline assembly functions to make a port connection with the hardware*/
void outb(uint_16 port, uint_8 val);
uint_8 inb(uint_16 port);

void outw(uint_16 port, uint_16 val);
uint_16 inw(uint_16 port);

#endif // !INPUT_OUTPUT

