#include "physicalMemory.h"

//! different memory regions (in memory_region.type)
char* strMemoryTypes[] = {

	{"Available\0"},			//memory_region.type==0
	{"Reserved\0"},			//memory_region.type==1
	{"ACPI Reclaim\0"},		//memory_region.type==2
	{"ACPI NVS Memory\0"}		//memory_region.type==3
};


/*******************************
	  Interface Functions
/*******************************/

void pmmngr_init(uint_32 memSize, physical_addr bitmap) {

	_mmngr_memory_size = memSize;
	_mmngr_memory_map = (uint_32*)bitmap;
	_mmngr_max_blocks = (pmmngr_get_memory_size() * 1024) / PMMNGR_BLOCK_SIZE;
	_mmngr_used_blocks = pmmngr_get_block_count();

	//! By default, all of memory is in use
	memset(_mmngr_memory_map, 0xff, pmmngr_get_block_count() / PMMNGR_BLOCKS_PER_BYTE);
}

void pmmngr_init_region(physical_addr base, uint_32 size) {

	int align = base / PMMNGR_BLOCK_SIZE;
	int blocks = size / PMMNGR_BLOCK_SIZE;

	for (; blocks > 0; blocks--) {
		mmap_unset(align++);
		_mmngr_used_blocks--;
	}

	mmap_set(0);	//first block is always set. This insures allocs cant be 0
}
void pmmngr_deinit_region(physical_addr base, uint_32 size) {

	int align = base / PMMNGR_BLOCK_SIZE;
	int blocks = size / PMMNGR_BLOCK_SIZE;

	for (; blocks > 0; blocks--) {
		mmap_set(align++);
		_mmngr_used_blocks++;
	}
}

void* pmmngr_alloc_block() {

	if (pmmngr_get_free_block_count() <= 0)
		return 0;	//out of memory

	int frame = mmap_first_free();

	if (frame == -1)
		return 0;	//out of memory

	mmap_set(frame);

	physical_addr addr = frame * PMMNGR_BLOCK_SIZE; //plus header
	_mmngr_used_blocks++;

	return (void*)addr;
}

void* pmmngr_alloc_blocks(uint_32 size) {

	if (pmmngr_get_free_block_count() <= size)
		return 0;	//not enough space

	int frame = mmap_first_free_s(size);

	if (frame == -1)
		return 0;	//not enough space

	for (uint_32 i = 0; i < size; i++)
		mmap_set(frame + i);

	physical_addr addr = frame * PMMNGR_BLOCK_SIZE;
	_mmngr_used_blocks += size;

	return (void*)addr;
}

//! finds first free "size" number of frames and returns its index
int mmap_first_free_s(uint_32 size) {

	if (size == 0)
		return -1;

	if (size == 1)
		return mmap_first_free();

	for (uint_32 i = 0; i < pmmngr_get_block_count() / 32; i++)
		if (_mmngr_memory_map[i] != 0xffffffff)
			for (int j = 0; j < 32; j++) {	//! test each bit in the dword

				int bit = 1 << j;
				if (!(_mmngr_memory_map[i] & bit)) {

					int startingBit = i * 32;
					startingBit += bit;		//get the free bit in the dword at index i

					uint_32 free = 0; //loop through each bit to see if its enough space
					for (uint_32 count = 0; count <= size; count++) {

						if (!mmap_test(startingBit + count))
							free++;	// this bit is clear (free frame)

						if (free == size)
							return i * 4 * 8 + j; //free count==size needed; return index
					}
				}
			}

	return -1;
}

void pmmngr_free_block(void* p) {

	physical_addr addr = (physical_addr)p;
	int frame = addr / PMMNGR_BLOCK_SIZE;
	mmap_unset(frame);

	_mmngr_used_blocks--;
}

void pmmngr_free_blocks(void* p, uint_32 size) {

	physical_addr addr = (physical_addr)p;
	int frame = addr / PMMNGR_BLOCK_SIZE;

	for (uint_32 i = 0; i < size; i++)
		mmap_unset(frame + i);

	_mmngr_used_blocks -= size;
}


/*******************************
	 Implementation Functions
/*******************************/

void mmap_set(int bit) {

	_mmngr_memory_map[bit / 32] |= (1 << (bit % 32));
}

void mmap_unset(int bit) {

	_mmngr_memory_map[bit / 32] &= ~(1 << (bit % 32));
}

int mmap_test(int bit) {

	return _mmngr_memory_map[bit / 32] & (1 << (bit % 32));
}

uint_32 pmmngr_get_block_count()
{
	return _mmngr_max_blocks;
}

int mmap_first_free() {

	// find the first free bit
	for (uint_32 i = 0; i < pmmngr_get_block_count() / 32; i++)
		if (_mmngr_memory_map[i] != 0xffffffff)
			for (int j = 0; j < 32; j++) {		// test each bit in the dword

				int bit = 1 << j;
				if (!(_mmngr_memory_map[i] & bit))
					return i * 4 * 8 + j; // i * size of int * offset from this block by j
			}

	return -1;
}

uint_32 pmmngr_get_memory_size() {

	return _mmngr_memory_size;
}

uint_32 pmmngr_get_use_block_count() {

	return _mmngr_used_blocks;
}

uint_32 pmmngr_get_free_block_count() {

	return _mmngr_max_blocks - _mmngr_used_blocks;
}
