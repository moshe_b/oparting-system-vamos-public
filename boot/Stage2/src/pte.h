#ifndef PTH_H
#define PTH_H

#include "Typedefs.h"
#include "physicalMemory.h"

//enum pte flags.
//we use macro for easy use
enum PTE_FLAGS
{
	PTE_PRESENT_FLAG = 1,
	PTE_IS_WRITEBLE = 2,
	PTE_IS_USER_MODE = 4,
	//resrvd is not needed because is for intel
	PTE_IS_PAGE_ACCESSED = 0x20,
	PTE_IS_PAGE_WRRITEN_TO = 0x40,
	PTE_FRAME = 0x7FFFF000 // macro for the frame address
};

//pt_entry is only 32 bits of attributes and frame
typedef uint_32 pt_entry;

//functions decelerations

void 		pt_entry_add_attrib(pt_entry* e, uint_32 attrib);
void 		pt_entry_del_attrib(pt_entry* e, uint_32 attrib);
void 		pt_entry_set_frame(pt_entry*, physical_addr);
int 		pt_entry_is_present(pt_entry e);
int 		pt_entry_is_writable(pt_entry e);
physical_addr	pt_entry_pfn(pt_entry e);

#endif