

;--------------------------------------------
;	Memory map entry structure
;--------------------------------------------
;[bits 32]
struc	MemoryMapEntry
	 .baseAddress		resq	1
	 .length			resq	1
	 .type				resd	1
	 .acpi_null			resd	1
endstruc


struc	multiboot_info
	.memoryLo		resd	1
	.memoryHi		resd	1
	.bootDevice		resd	1
	.mmap_length	resd	1
	.mmap_addr      resd    1
endstruc


; page directory table
PAGE_DIR	equ		0x9C000

; 0th page table. Address must be 4KB aligned
PAGE_TABLE_0	equ	0x9D000

; 768th page table. Address must be 4KB aligned
PAGE_TABLE_768	equ	0x9E000

; each page table has 1024 entries
PAGE_TABLE_ENTRIES equ	1024

; attributes (page is present;page is writable; supervisor mode)
PRIV equ	3


;---------------------------------------------
;	Get memory map from bios
;	/in es:di->destination buffer for entries
;	/ret bp=entry count
;---------------------------------------------

BiosGetMemoryMap:
	pushad
	xor		ebx, ebx
	xor		bp, bp									; number of entries stored here
	mov		edx, 'PAMS'								; 'SMAP'
	mov		eax, 0xe820
	mov		ecx, 24									; memory map entry struct is 24 bytes
	int		0x15									; get first entry
	jc		.error	
	cmp		eax, 'PAMS'								; bios returns SMAP in eax
	jne		.error
	test	ebx, ebx								; if ebx=0 then list is one entry long; bail out
	je		.error
	jmp		.start
.next_entry:
	mov		edx, 'PAMS'								; some bios's trash this register
	mov		ecx, 24									; entry is 24 bytes
	mov		eax, 0xe820
	int		0x15									; get next entry
.start:
	jcxz	.skip_entry								; if actual returned bytes is 0, skip entry
.notext:
	mov		ecx, [es:di + MemoryMapEntry.length]	; get length (low dword)
	test	ecx, ecx								; if length is 0 skip it
	jne		short .good_entry
	mov		ecx, [es:di + MemoryMapEntry.length + 4]; get length (upper dword)
	jecxz	.skip_entry								; if length is 0 skip it
.good_entry:
	inc		bp										; increment entry count
	add		di, 24									; point di to next entry in buffer
.skip_entry:
	cmp		ebx, 0									; if ebx return is 0, list is done
	jne		.next_entry								; get next entry
	jmp		.done
.error:
	stc
.done:
	popad
	ret

	
;---------------------------------------------
;	Get memory size for >64M configuations
;	ret\ ax=KB between 1MB and 16MB
;	ret\ bx=number of 64K blocks above 16MB
;	ret\ bx=0 and ax= -1 on error
;   
;   routine for getting all the memory size above 1MB 
;   after this routine the size of the memory (in kb) is (1024 + ax + bx*64)
;---------------------------------------------

BiosGetMemorySize64MB:
	push	ecx
	push	edx
	xor		ecx, ecx
	xor		edx, edx
	mov		ax, 0xe801
	int		0x15	
	jc		.error
	cmp		ah, 0x86		;unsupported function
	je		.error
	cmp		ah, 0x80		;invalid command
	je		.error
	jcxz	.use_ax			;bios may have stored it in ax,bx or cx,dx. test if cx is 0
	mov		ax, cx			;its not, so it should contain mem size; store it
	mov		bx, dx

.use_ax:
	pop		edx				;mem size is in ax and bx already, return it
	pop		ecx
	ret

.error:
	mov		ax, -1
	mov		bx, 0
	pop		edx
	pop		ecx
	ret


or_func:
	mov eax, [esp+4]
	mov ebx, [esp + 8]
	or eax, ebx
	; clean the stack
	pop ebx
	pop ebx
	ret
	
EnablePaging:
	pusha										; save stack frame

	;------------------------------------------
	;	idenitity map 1st page table (4MB)
	;------------------------------------------

	mov		eax, PAGE_TABLE_0					; first page table
	mov ebx, 0x0
	or ebx, PRIV
;	mov		ebx, 0x0 | PRIV						; starting physical address of page
	mov		ecx, PAGE_TABLE_ENTRIES				; for every page in table...
.loop:
	mov		dword [eax], ebx					; write the entry
	add		eax, 4								; go to next page entry in table (Each entry is 4 bytes)
	add		ebx, 4096							; go to next page address (Each page is 4Kb)
	loop	.loop								; go to next entry
	;------------------------------------------
	;	set up the entries in the directory table
	;------------------------------------------
	;push PRIV
	;push PAGE_TABLE_0
	;call or_func
	mov eax, PRIV
	or eax, PAGE_TABLE_0
;	mov		eax, eax			; 1st table is directory entry 0
	mov		dword [PAGE_DIR], eax

	;push PRIV
	;push PAGE_TABLE_768
	mov eax, PRIV
	or eax, PAGE_TABLE_768
	;mov		eax, PAGE_TABLE_768 | PRIV			; 768th entry in directory table
	mov		dword [PAGE_DIR+(768*4)], eax

	;------------------------------------------
	;	install directory table
	;------------------------------------------

	mov		eax, PAGE_DIR
	mov		cr3, eax

	;------------------------------------------
	;	enable paging
	;------------------------------------------

	mov		eax, cr0
	or		eax, 0x80000000
	mov		cr0, eax

	;------------------------------------------
	;	map the 768th table to physical addr 1MB
	;	the 768th table starts the 3gb virtual address
	;------------------------------------------
 
	mov		eax, PAGE_TABLE_768				; first page table
	mov ebx, 0x100000
	or ebx, PRIV
	;mov		ebx, 0x100000 | PRIV			; starting physical address of page
	mov		ecx, PAGE_TABLE_ENTRIES			; for every page in table...
.loop2:
	mov		dword [eax], ebx				; write the entry
	add		eax, 4							; go to next page entry in table (Each entry is 4 bytes)
	add		ebx, 4096						; go to next page address (Each page is 4Kb)
	loop	.loop2							; go to next entry

	popa
	ret
	;Global EnablePaging
; call this function from c to install
; the current directory table into cr3 that in x86 architecture 
; is used as PBDR register
loadPBDR:
	;cli
	mov ebx, [esp+4]
	;mov bx, 'A'
	;mov [0xb8000], bx
	;mov bx, 0xF
	;mov [0xb8001], bx 
;	mov ebx, 0x07690748
;	mov [0xb8000], ebx
	;xor ebx, ebx
	mov eax, [ebx]
	mov cr3, eax
	
;	mov eax, cr4                 ; Set the A-register to control register 4.
 ;   or eax, 1 << 5               ; Set the PAE-bit, which is the 6th bit (bit 5).
;    mov cr4, eax  
	;sti
	ret
	Global loadPBDR

enablePaging:
	mov	eax, cr0
	or eax, 0x80000000
	mov	cr0, eax
	
	ret
	;Global enablePaging
	; xor bx, bx
	; mov bx, 1
	; cmp [esp+4], bx
	; je	enable
	; jmp disable 
	; enable:
			; or eax, 0x80000000		;set bit 31
			; ; mov bx, 'B'
			; ; mov [0xb8002], bx
			; ; mov bx, 0xF
			; ; mov [0xb8003], bx 
			; mov ebx, 0x07690748
			; mov [0xb8004], ebx
			; mov	cr0, eax
			; jmp done
	; disable:
			; and eax, 0x7FFFFFFF		;clear bit 31
			; mov	cr0, eax
	; done:
		; ret
	Global enablePaging