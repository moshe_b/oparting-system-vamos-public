#include "disk.h"

/*Ata device struct metadata*/
ATA ATADevices[ATA_DEVICES_MAX];
int count = 0;
uint_16 returned[256];


/*Initialize ATA drivers and their types*/
void ATAinit()
{
	count = 0;
	detectATA();
}

void detectATA()
{
	/*ALL DETECT disk types */
	detectDevtype(0x1F0, 0);  // PRIMARY   MASTER
	detectDevtype(0x1F0, 16); // PRIMARY   SLAVE
	detectDevtype(0x170, 0);  // SECONDERY MASTER
	detectDevtype(0x170, 16); // SECONDARY SLAVE
}

void detectDevtype(int port, int slavebit)
{
	// PRIMARY port - 0x1F0, SECONDARY port - 0x170
	//  [CHECK and TAKE disk]  EXAMPLE- slavebit: (0x10 | 0xA0 = 0xB0) ~ slave.  slavebit: (0x00 | 0xA0 = 0xA0) ~ master
	outb(port + ATA_PRIMARY_DRIVE_HEAD_OFFSET, SLAVE_BIT_CHECK | slavebit);
	outb(port + ATA_PRIMARY_SECCOUNT_OFFSET, 0);
	outb(port + ATA_PRIMARY_LBA_LO_OFFSET, 0);
	outb(port + ATA_PRIMARY_LBA_MID_OFFSET, 0);
	outb(port + ATA_PRIMARY_LBA_HI_OFFSET, 0);

	//Sending IDENTIFY function port - 0xEC

	outb(port + ATA_PRIMARY_COMM_REGSTAT_OFFSET, IDENTIFY_PORT);
	char checker = inb(port + ATA_PRIMARY_COMM_REGSTAT_OFFSET);

	// Wait for finish bit 
	while (checker & 0x8 != 0)
	{
		checker = inb(port + ATA_PRIMARY_COMM_REGSTAT_OFFSET);
		if (checker & 1 != 0) break; // If we got an error we break
	}

	if (checker == 0 || checker == 1) { return; } // Drive is not exists

	uint_16 u = inw(port);
	returned[0] = u;

	for (int i = 1; i < MAX_ATA_META_BUFFER; i++)
		returned[i] = inw(port);

	ATADevices[count].port = port;
	ATADevices[count].slavebit = slavebit;
	if (returned[83] & 1 << 10 != 0)
	{
		//Drive supports LBA 48 ->  2 ^ 48  ~ 128TB
		ATADevices[count].isLBA48Supported = 1;
		/* If returns something like FFFFFF it is supports more than lba48 so it can be buggy...*/
		ATADevices[count].LBA48 = (returned[103] << 48) + (returned[102] << 32) + (returned[101] << 16) + returned[100];
	}
	else
	{
		//From segate: if hard disk doesn't support lba48 it needs to support lba28 
		//Drive supports LBA 28 -> 2 ^ 28 ~ 128GB
		ATADevices[count].isLBA48Supported = 0;
		ATADevices[count].LBA28 = returned[61] << 16 + returned[60];

		if (ATADevices[count].LBA28 == 0) { return; } // Total sectors: 0... disk is not in use
	}
	count++;
}

void ataRead(int driveID, uint_32 LBA, int countSector, char* addr)
{
	if (ATADevices[driveID].isLBA48Supported == 1)
		ataRead48(driveID, LBA, countSector, addr);
	else
		ataRead28(driveID, LBA, countSector, addr);
}

void ataWrite(int driveID, uint_32 LBA, int bufferSize, char* addr)
{

	if (ATADevices[driveID].isLBA48Supported == 1)
		ataWrite48(driveID, LBA, bufferSize, addr);
	else
		ataWrite28(driveID, LBA, bufferSize, addr);


}

void ataRead28(int driveID, uint_32 LBA, int countSector, char* addr)
{
	ATA device = ATADevices[driveID];
	uint_16 port = device.port;
	uint_8 slavebit = device.slavebit;
	outb(port + 6, 0xE0 | (slavebit) | ((LBA >> 24) & 0x0F)); // 0xE0 - LBA28 MODE
	outb(port + 1, 0x00); // ERR CODE 0x00 - save cpu time
	outb(port + 2, countSector);// MAX 256 READ sectors
	outb(port + 3, LBA & 0xFF);
	outb(port + 4, (LBA >> 8) & 0XFF);
	outb(port + 5, (LBA >> 16) & 0xFF);
	outb(port + 7, PIO_SECTOR_READ_CMD); // READ PORT 

	uint_16 dcr = 0x3F6; // Data control register port -> 0x3f6 (FOR PRIMARY DISKS!!!)
	if (port == 0x170) dcr = 0x376; // FOR SECONDERY DISKS!!! 

	// POLL !
	for (int i = 0; i < (TRANSFER_DATA_CHUNCK * (countSector)); i++)
	{
		while (1)
		{
			uint_8 status = inb(port + 7);
			if (status & STAT_DRQ) { break; } // Drive is ready to transfer data
		}
		// 0000000 00000000 
		uint_16 h = inw(port);
		*((uint_8*)addr + i * 2) = h & 0xFF;
		*((uint_8*)addr + i * 2 + 1) = (h >> 8) & 0xFF;
	}
}

void ataWrite28(int driveID, uint_32 LBA, int bufferSize, char* addr)
{
	ATA device = ATADevices[driveID];
	int reachEOB = 0;
	uint_16 port = device.port;
	uint_8 slavebit = device.slavebit;
	int countSector = (bufferSize / (TRANSFER_DATA_CHUNCK + 1)) + 1;
	outb(port + 6, 0xE0 | (slavebit) | ((LBA >> 24) & 0X0F)); // 0xE0 - LBA28 
	outb(port + 2, countSector);// MAX 256 READ sectors
	outb(port + 3, LBA & 0xFF);
	outb(port + 4, (LBA >> 8) & 0XFF);
	outb(port + 5, (LBA >> 16) & 0xFF);
	outb(port + 7, PIO_SECTOR_WRITE_CMD); // WRITE PORT 0X30

	uint_16 dcr = 0x3F6; // Data control register port -> 0x3f6 (FOR PRIMARY DISKS!!!)
	if (port == 0x170) dcr = 0x376; // FOR SECONDERY DISKS!!! 

	// sleep trick
	char x = inb(port + 7);
	while (x & BSY_PORT) // Wait until a busy bit will turn off
		x = inb(port + 7);

	for (int i = 0; i < TRANSFER_DATA_CHUNCK * countSector; i++)
	{
		reachEOB = (i > (bufferSize / 2)) ? 1 : 0;

		// Transfer the data!
		if (!reachEOB)
		{
			outw(port, ((*((uint_8*)addr + i * 2 + 1)) << 8) + (*((uint_8*)addr + i * 2)));
		}
		else
		{
			outw(port, 0);
		}

		// POLL!
		outb(port + 7, 0xE7);
		char x = inb(port + 7);
		while (x & BSY_PORT != 0) x = inb(port + 7); // Wait for busy port until it turns off
	}
}

void ataWrite48(int driveID, uint_64 LBA, int countSector, char* addr)
{
	ATA device = ATADevices[driveID];

	uint_16 port = device.port;
	uint_8 slavebit = device.slavebit;

	outb(port + 6, 0x40 | slavebit); // 0x40 -> LBA48 mode master.. 0x50 
	outb(port + 2, 0);// MAX 256 READ sectors FIRST 8 bit

	outb(port + 3, (LBA >> 24) & 0xFF); // 24 - 31 bit LBA4
	outb(port + 4, (LBA >> 32) & 0XFF); // 32 - 39 bit LBA5
	outb(port + 5, (LBA >> 40) & 0xFF); // 40 - 47 bit LBA6

	outb(port + 2, countSector);// MAX 256 READ sectors 

	outb(port + 3, (LBA >> 0) & 0xFF);  // 0 - 8 bit LBA1
	outb(port + 4, (LBA >> 8) & 0XFF);  // 9 - 15 bit LBA2
	outb(port + 5, (LBA >> 16) & 0xFF); // 16 - 23 bit LBA3

	outb(port + 7, 0x34); // WRITE PORT 0X34 (LBA48)

	uint_16 dcr = 0x3F6; // Data control register port -> 0x3f6 (FOR PRIMARY DISKS!!!)
	if (port == 0x170) dcr = 0x376; // FOR SECONDERY DISKS!!! 

	// sleep trick
	char x = inb(port + 7);
	while (x & 0x80) // Wait until a busy bit will turn off
		x = inb(port + 7);

	for (int i = 0; i < 256 * countSector; i++)
	{
		outw(port, ((*((uint_8*)addr + i * 2 + 1)) << 8) + (*((uint_8*)addr + i * 2)));
		outb(port + 7, 0xE7);
		char x = inb(port + 7);
		while (x & 0x80 != 0) x = inb(port + 7); // Wait for busy port until it turns off
	}
}

void ataRead48(int driveID, uint_64 LBA, int countSector, char* addr)
{
	ATA device = ATADevices[driveID];

	uint_16 port = device.port;
	uint_8 slavebit = device.slavebit;

	outb(port + 6, 0x40 | slavebit); // 0x40 -> LBA48 mode master.. 0x50 
	outb(port + 2, 0);// MAX 256 READ sectors FIRST 8 bit

	outb(port + 3, (LBA >> 24) & 0xFF); // 24 - 31 bit LBA4
	outb(port + 4, (LBA >> 32) & 0XFF); // 32 - 39 bit LBA5
	outb(port + 5, (LBA >> 40) & 0xFF); // 40 - 47 bit LBA6

	outb(port + 2, countSector);// MAX 256 READ sectors 

	outb(port + 3, (LBA >> 0) & 0xFF);  // 0 - 8 bit LBA1
	outb(port + 4, (LBA >> 8) & 0XFF);  // 9 - 15 bit LBA2
	outb(port + 5, (LBA >> 16) & 0xFF); // 16 - 23 bit LBA3

	outb(port + 7, 0x24); // WRITE PORT 0X24 (LBA28)

	uint_16 dcr = 0x3F6; // Data control register port -> 0x3f6 (FOR PRIMARY DISKS!!!)
	if (port == 0x170) dcr = 0x376; // FOR SECONDERY DISKS!!! 

	// 400 nanoseconds sleep trick
	char x = inb(port + 7);
	x = inb(port + 7);
	x = inb(port + 7);
	x = inb(port + 7);

	while (x & 0x80) // Wait until a busy bit will turn off
		x = inb(port + 7);

	for (int i = 0; i < 256 * countSector; i++)
	{
		uint_16 h = inw(port);

		*((uint_8*)addr + i * 2) = h & 0xFF;
		*((uint_8*)addr + i * 2 + 1) = (h >> 8) & 0xFF;
	}
}


