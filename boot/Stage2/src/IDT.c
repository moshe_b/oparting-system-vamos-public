#include "idt.h"
#include "kernel.h"

//CR: may be used in header file
//CR: explain that they are routines from assembly
extern void LoadIDT();
extern uint_32 isr1;

//! describes the structure for the processors idtr register
typedef struct idtr {

	//! size of the interrupt descriptor table (idt)
	uint_16		limit;
	//! base address of idt
	uint_32		base;
}idtr;

//! interrupt descriptor table
extern idt_descriptor	_idt [I86_MAX_INTERRUPTS];
//! idtr structure used to help define the cpu's idtr register
idtr _idtr;
//! default int handler used to catch unregistered interrupts
void i86_default_handler ();

//! installs idtr into processors idtr register
static void idt_install () {
	//CR: code in comment
	//asm volatile("lidt %0" :: "m"(_idtr));
}

//default handler is for the keyboard
void i86_default_handler () {
	
	isrKeyboardHandler();
}

//div zero exception handler
void devided_by_zero()
{
	setCursorPosition(positionFromCoords(0, 17));
	printString("Error, divided by zero!\n\r", BACKGROUND_BLACK | FORGROUND_WHITE);
	asm volatile ("xchgw %bx, %bx");
	outb(0x20, 0x20);
	outb(0xa0, 0x20);
}

//! returns interrupt descriptor
idt_descriptor* i86_get_ir (uint_32 i) {

	if (i>I86_MAX_INTERRUPTS)
		return 0;

	return &_idt[i];
}

//! installs a new interrupt handler
int i86_install_ir (uint_32 i, uint_16 flags, uint_16 sel, I86_IRQ_HANDLER irq) {

	if (i>I86_MAX_INTERRUPTS)
		return 0;

	if (!irq)
		return 0;

	//! get base address of interrupt handler
	uint_64		uiBase = (uint_64)(irq);
	//! store base address into idt
	_idt[i].baseLo		=	(uint_16)((uint_32)&isr1 & 0xffff);
	_idt[i].baseHi		=	(uint_16)(((uint_32)&isr1 >> 16) & 0xffff);
	_idt[i].reserved	=	0;
	_idt[i].flags		=	(uint_8)(flags);
	_idt[i].sel			=	sel;

	return	0;
}

extern uint_32 isrDefaultHandler;
void defaultIdtHandler()
{
	// Now this function do nothing
	return;
}

//! initialize idt
int i86_idt_initialize (uint_16 codeSel) {

	for (int i = 0; i < I86_MAX_INTERRUPTS; i++)
	{
		_idt[i].baseLo = (uint_16)((uint_32)&isrDefaultHandler & 0xffff);
		_idt[i].baseHi = (uint_16)(((uint_32)&isrDefaultHandler >> 16) & 0xffff);
		_idt[i].reserved = 0;
		_idt[i].flags = (uint_8)(I86_IDT_DESC_PRESENT | I86_IDT_DESC_BIT32);
		_idt[i].sel = codeSel;
	}
	
	_idt[33].baseLo = (uint_16)((uint_32)&isr1 & 0xffff);
	_idt[33].baseHi = (uint_16)(((uint_32)&isr1 >> 16) & 0xffff);
	_idt[33].reserved = 0;
	_idt[33].flags = (uint_8)(I86_IDT_DESC_PRESENT | I86_IDT_DESC_BIT32);
	_idt[33].sel = codeSel;
	//! install our idt
	outb(0x21, 0xfd);
	outb(0xa1, 0xff);
	LoadIDT();

	return 0;
}
