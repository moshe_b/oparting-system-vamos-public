#ifndef _IDT_H
#define _IDT_H

#include "kernel.h"
#include "keyboard.h"

//! i86 defines 256 possible interrupt handlers (0-255)
#define I86_MAX_INTERRUPTS		256

//! must be in the format 0D110, where D is descriptor type
#define I86_IDT_DESC_BIT16		0x06	//00000110
#define I86_IDT_DESC_BIT32		0x0E	//00001110
#define I86_IDT_DESC_RING1		0x40	//01000000
#define I86_IDT_DESC_RING2		0x20	//00100000
#define I86_IDT_DESC_RING3		0x60	//01100000
#define I86_IDT_DESC_PRESENT	0x80	//10000000

//! interrupt handler w/o error code
//! Note: interrupt handlers are called by the processor. The stack setup may change
//! so we leave it up to the interrupts' implimentation to handle it and properly return
typedef void (*I86_IRQ_HANDLER)(void);

//! interrupt descriptor
typedef struct idt_descriptor {

	//! bits 0-16 of interrupt routine (ir) address
	uint_16		baseLo;

	//! code selector in gdt
	uint_16		sel;

	//! reserved, shold be 0
	uint_8			reserved;

	//! bit flags. Set with flags above
	uint_8			flags;

	//! bits 16-32 of ir address
	uint_16		baseHi;
}idt_descriptor;

//! returns interrupt descriptor
idt_descriptor* i86_get_ir (uint_32 i);

//! installs interrupt handler. When INT is fired, it will call this callback
int i86_install_ir (uint_32 i, uint_16 flags, uint_16 sel, I86_IRQ_HANDLER);

// initialize basic idt
int i86_idt_initialize (uint_16 codeSel);

#endif
