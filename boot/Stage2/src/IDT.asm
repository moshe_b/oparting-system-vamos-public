
; [extern _idt]

; %macro PUSHALL 0
	; ; push rax
	; ; push rcx
	; ; push rdx
	; ; push r8
	; ; push r9
	; ; push r10
	; ; push r11
; %endmacro

; %macro POPALL 0
	; ; pop r11
	; ; pop r10
	; ; pop r9
	; ; pop r8
	; ; pop rdx
	; ; pop rcx
	; ; pop rax	
; %endmacro


; idtDescriptor:
	; dw 4095
	; dw _idt  ; dq before change to i686
[bits 32]
[extern i86_default_handler]
; [extern devided_by_zero]
; [extern i86_pit_irq]

; general interrupt
isr1:
	cli
;	pusha
	call i86_default_handler
;popa
	sti
	iretd 
	GLOBAL isr1
	
	
[extern defaultIdtHandler]
isrDefaultHandler:
	pusha
	call defaultIdtHandler
	popa
	iretd 
	GLOBAL isrDefaultHandler
	
[extern devided_by_zero]
; devide by zero interrupt
isr2:
	pusha
	call devided_by_zero
	popa
	iretd 
	GLOBAL isr2

[extern i86_pit_irq]
; pit interrupt
isr_pit:
	;add esp, 12
	pusha
	call i86_pit_irq
	popa
	iretd
	GLOBAL isr_pit

	
[extern mouseHandler]	
isrMouseHandler:
	pusha
	call mouseHandler
	popa
	iretd 
	GLOBAL isrMouseHandler

; [extern pageFaultHandler]
; isrPageFaultHandler:
	; call pageFaultHandler
	; Global isrPageFaultHandler
	
[extern _idtr]
[extern _idt]

idtDescriptor:
	dw 4095
	dd _idt


LoadIDT:
	lidt[idtDescriptor]
	sti
	ret
	GLOBAL LoadIDT