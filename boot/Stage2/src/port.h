#ifndef PORT_H
#define PORT_H
#include "stdint.h"

extern void insw(uint16_t port, void *addr, unsigned int count);
extern void outsw(uint16_t port, void *addr, unsigned int count);

#endif