﻿#include "fs.h"

FS m_FS;
extern DATE vamosDate;

int createFileDir(char* path, bool isDirectory)
{
	pathMember pm;
	uint_16 RootClusters[16];

	if (!divPath(path, &pm))
	{
		return NULL;
	}
	
	if (!getClusters(0, RootClusters))
	{
		return NULL;
	}

	int indLastPathMember = getLastPathMember(pm);
	bool isSizeOne = indLastPathMember == 0;
	pathMember pmDir;
	FILE dir;

	fillZeros(&pmDir);
	int i = 0;
	while (pm[i][0] != 0 && i < indLastPathMember)
	{
		int j = 0;
		while (pm[i][j] != 0)
		{
			pmDir[i][j] = pm[i][j];
			j++;
		}

		i++;
	}
	
	if (!isSizeOne && !openFileHelper(&pmDir, 0, RootClusters, &dir))
	{
		printf("failed 1");
		return 0;
	}
	uint_16 fileClusters[30];
	char fileName[MAX_PATH_MEMBER_LEN] = {0};
	strcpy(fileName, pm[indLastPathMember]);

	if (indLastPathMember != 0) //0 mean it's directory/file in the root directory
		getClusters(getCluster(dir.m_firstCluster), fileClusters);
	

	FILE fileToAdd;
	initFile(fileName, "txt", UN_WRITE_FILE, isDirectory, &fileToAdd);

	return addFileToDir(isSizeOne ? RootClusters : fileClusters, &dir, &fileToAdd, false);
}

void initFS()
{
	m_FS.m_fsOEM.m_bytesPerSector = 512;
	m_FS.m_fsOEM.m_numFats = 2;
	m_FS.m_fsOEM.m_numReservedSectors = 0;
	m_FS.m_fsOEM.m_sectorPerFat = 256;
	m_FS.m_fsOEM.m_sectorsPerCluster = 32;
	m_FS.m_baseSectors = 1 + m_FS.m_fsOEM.m_numFats * m_FS.m_fsOEM.m_sectorPerFat;
}

int createFile(char* path)
{
	return createFileDir(path, false);
}

int createDirectory(char* path)
{
	return createFileDir(path, true);
}

int openFile(char* path, FILE* fileSave)
{
	pathMember pm;
	uint_16 clusters[16];

	if (!divPath(path, &pm))
	{
		printf("divPath failed ");
		return NULL;
	}
	if (!getClusters(0, clusters))
	{
		printf("getClusters failed ");
		return NULL;
	}
	//get root directory
	if (strcmp(path, "C://") == 0)
	{
		initFile("C://", "", 0, true, fileSave);
		return 1;
	}
	else
	{
		return openFileHelper(&pm, 0, clusters, fileSave);
	}
}

int openDir(char* path, FILE* dirSave)
{
	return openFile(path, dirSave);
}

int listDir(char* path, FILE* dirFilesArray)
{
	FILE dir;
	if (openDir(path, &dir))
	{
		uint_16 clusters[100];
		if (getClusters(getCluster(dir.m_firstCluster), clusters))
		{
			getFilesInDir(clusters, dirFilesArray);
		}

		return 1;
	}

	return 0;
}

/*
	read 512 bytes - default read amount
*/
int readFile(char* path, char* bufferToSave)
{
	return readFileHelper(path, bufferToSave, m_FS.m_fsOEM.m_bytesPerSector);
}

int readFileHelper(char* path, char* bufferToSave, int lenToRead)
{
	FILE file;

	if (!openFile(path, &file))
		return 0;

	if (isDir(file))
		return 1;

	if (!isAllocated(&file))
	{
		printf("not allocated ");
		return 2;
	}

	uint_16 fileClusters[16];
	if (!getClusters(getCluster(file.m_firstCluster), fileClusters))
		return 3;

	int numClusters = getNumClusters(fileClusters);

	//run on files clusters
	for (int i = 0; i < numClusters; i++)
	{
		//run on every sectors in specific cluster
		for (int j = 0; j < m_FS.m_fsOEM.m_sectorsPerCluster; j++)
		{
			char* buffRead = pmmngr_alloc_block();
			for (int k = 0; k < 4 * 1024; k++)
				buffRead[k] = 0;

			ataRead(0, m_FS.m_baseSectors + fileClusters[i] * m_FS.m_fsOEM.m_sectorsPerCluster + j, 2, buffRead);

			strncpy(bufferToSave + m_FS.m_fsOEM.m_bytesPerSector * j, buffRead, strlen(buffRead) > 512 ? 512 : strlen(buffRead));

			pmmngr_free_block(buffRead);
			//reach end of buffer
			if (i * m_FS.m_fsOEM.m_sectorsPerCluster * m_FS.m_fsOEM.m_bytesPerSector + (j + 1) * m_FS.m_fsOEM.m_bytesPerSector >= lenToRead)
				return 4;
		}
	}

	return 5;
}

uint_16 getHmsValue(uint_16 hour, uint_16 minute, uint_16 second)
{
	uint_16 hms = 0;

	hms |= ((minute << 5) & (0x003f << 5));
	hms |= ((hour	<< 11) & (0x01ff << 11));

	return hms;
}

void setHMS(uint_16 hms, uint_8 seconds, FILE* file)
{
	file->m_createTimeHMS[0] = hms & 0xFF;
	file->m_createTimeHMS[1] = hms >> 8;

	file->m_createTimeSeconds = seconds;
}

uint_16 getHMSFromFile(FILE file)
{
	uint_16 hms = 0;

	hms = file.m_createTimeHMS[0] | (uint_16)file.m_createTimeHMS[1] << 8;
	return hms;
}

uint_16 getDmyValue(uint_16 day, uint_16 month, uint_16 year)
{
	uint_16 dmy = 0;

	dmy |= ((day & 0x00FF));
	dmy |= ((month << 5) & (0x000f << 5));
	dmy |= ((year << 9) & (0x007f << 9));

	return dmy;
}

void setDMY(uint_16 dmy, FILE* file)
{
	file->m_createTimeDMY[0] = dmy & 0xFF;
	file->m_createTimeDMY[1] = dmy >> 8;
}

uint_16 getDMYFromFile(FILE file)
{
	uint_16 dmy = 0;

	dmy = file.m_createTimeDMY[0] | (uint_16)file.m_createTimeDMY[1] << 8;
	return dmy;
}

void getCreateDateFile(FILE file, DATE* fileDate)
{
	uint_16 fileDMY = getDMYFromFile(file);
	uint_16 fileHMS = getHMSFromFile(file);

	fileDate->m_day = (0x001f) & fileDMY;
	fileDate->m_month = (0x000f) & (fileDMY >> 5);
	fileDate->m_year = (0x007f) & (fileDMY >> 9);


	fileDate->m_second = file.m_createTimeSeconds;
	fileDate->m_minute = (0x003f) & (fileHMS >> 5);
	fileDate->m_hour = (0x001f) & (fileHMS >> 11);
}

void recursive_print(char* path, char* prefix) 
{
	FILE files[30] = {0};
	listDir(path, files);
	int size = 0;
	while (files[size].m_fileName[0])
		size++;

	for (int i = 0; i < size; i++) {
		char entry_prefix[250] = {0};
		strcpy(entry_prefix, prefix);
		if (i == size-1)
			strcpy(entry_prefix + strlen(entry_prefix), "-- ");
		else
			strcpy(entry_prefix + strlen(entry_prefix), "|-- ");

		printf("%s %s\n\r", entry_prefix, files[i].m_fileName);

		if (isDir(files[i])) {
			char dir_prefix[250] = { 0 };
			strcpy(dir_prefix, prefix);

			if (i == size - 1)
				strcpy(dir_prefix + strlen(dir_prefix), "    ");
			else
				strcpy(dir_prefix + strlen(dir_prefix), "|   ");

			char newPath[250] = { 0 };
			strcpy(newPath, path);
			if (strcmp(newPath, "C://") != 0)
			{
				strcpy(newPath + strlen(newPath), "/");
			}
			
			strcpy(newPath + strlen(newPath), files[i].m_fileName);
			recursive_print(newPath, dir_prefix);
		}
	}
}

int writeFile(char* path, char* bufferToWrite)
{
	FILE file;

	if (!openFile(path, &file))
		return 0;

	if (isDir(file))
		return 1;

	if (!isAllocated(&file))
	{
		printf("not allocated ");
		return 2;
	}

	uint_16 fileClusters[16];
	if (!getClusters(getCluster(file.m_firstCluster), fileClusters))
		return 3;

	
	int numSectors = strlen(bufferToWrite) / m_FS.m_fsOEM.m_bytesPerSector;
	numSectors = numSectors == 0 ? 1 : numSectors;
	int numClusters = getNumClusters(fileClusters);
	numClusters = numSectors / m_FS.m_fsOEM.m_sectorsPerCluster;
	numClusters = numClusters == 0 ? 1 : numClusters;

	//run on files clusters
	for (int i = 0; i < numClusters; i++)
	{
		//run on every sectors in specific cluster
		for (int j = 0; j < m_FS.m_fsOEM.m_sectorsPerCluster; j++)
		{

			char* buffRead = pmmngr_alloc_block();
			for (int k = 0; k < 4 * 1024; k++)
				buffRead[k] = 0;


			ataRead(0, m_FS.m_baseSectors + fileClusters[i] * m_FS.m_fsOEM.m_sectorsPerCluster + j, 2, buffRead);

			int lenCurrStr = strlen(bufferToWrite + i * m_FS.m_fsOEM.m_sectorsPerCluster * m_FS.m_fsOEM.m_bytesPerSector
				+ j * m_FS.m_fsOEM.m_bytesPerSector);
			strncpy(buffRead, bufferToWrite + i * m_FS.m_fsOEM.m_sectorsPerCluster * m_FS.m_fsOEM.m_bytesPerSector
				+ j * m_FS.m_fsOEM.m_bytesPerSector, lenCurrStr > 512 ? 512 : lenCurrStr);

			ataWrite(0, m_FS.m_baseSectors + fileClusters[i] * m_FS.m_fsOEM.m_sectorsPerCluster + j,
						strlen(buffRead),
						buffRead);

			pmmngr_free_block(buffRead);
			//reach end of buffer
			if (i * m_FS.m_fsOEM.m_sectorsPerCluster * m_FS.m_fsOEM.m_bytesPerSector + (j + 1) * m_FS.m_fsOEM.m_bytesPerSector >= strlen(bufferToWrite))
				return 4;
		}
	}

	return 5;
}

void getDirPath(char* fullPath, char* dirPathSave)
{
	int i = 0;
	for (i = strlen(fullPath); i > 0 && fullPath[i] != '/'; i--);
	strncpy(dirPathSave, fullPath, fullPath[i-1] == '/' ? i+1 : i); 
}

// Example path: C://dir/file
void getFileName(char* fullPath, char* fileName)
{
	int i = 0;
	for (i = strlen(fullPath); i > 0 && fullPath[i] != '/'; i--);

	for (int j = i+1; j < strlen(fullPath); j++)
		fileName[j-i-1] = fullPath[j];
}

int deleteFile(char* path)
{
	rtrim(path);
	if (!isPathExist(path))
	{
		return 0;
	}

	char dirPath[MAX_LEN_PATH] = { 0 };
	getDirPath(path, dirPath);

	FILE dirFiles[16] = {0}; //16 files limit 
	FILE dirFileAfterDel[16] = {0};
	char fileName[11] = { 0 };
	int dirFileCode = listDir(dirPath, dirFiles);

	getFileName(path, fileName);
	int fileIndex = 0;
	for (int i = 0; i < 16; i++)
	{
		int val = strcmp(fileName, dirFiles[i].m_fileName);
		if (val != 0 || strlen(fileName) != strlen(dirFiles[i].m_fileName))
		{
			char* filePtrSrc = (char*)(&dirFiles[i]);
			char* filePtrDest = (char*)(&dirFileAfterDel[fileIndex]);
			
			for (int j = 0; j < sizeof(FILE); j++)
				filePtrDest[j] = filePtrSrc[j];

			fileIndex++;
		}
	}
	int ind = 0;
	while (ind < 16)
	{
		ind++;
	}
	DIR dirObj;
	openDir(dirPath, &dirObj);

	uint_16 clusterDir = getCluster(dirObj.m_firstCluster);
	clusterDir = clusterDir == 0 ? clusterDir + 1 : clusterDir;
	ataWrite(0, m_FS.m_baseSectors + clusterDir * m_FS.m_fsOEM.m_sectorsPerCluster, sizeof(FILE) * 16, (char*)dirFileAfterDel);
	return 1;
}

bool isPathExist(char* path)
{
	FILE file;
	return openFile(path, &file);
}

int divPath(char* path, pathMember* pm)
{
	fillZeros(pm);

	if (strncmp(path, "C://", 4))
	{
		// Not equal Path
		return 0;
	}

	fillZeros(pm);

	int index = 4;
	int lenPath = strlen(path);
	int pathMemberIndex = 0;
	int indexSlash = index + find(path + index, '/') + 1;
	
	while (index < lenPath)
	{
		int indexSlash = find(path + index, '/');
		indexSlash = indexSlash == -1 ? strlen(path) : index + indexSlash;
		strncpy((*pm)[pathMemberIndex], path + index, indexSlash - index);
		index = indexSlash + 1;
		pathMemberIndex++;
	}
	return 1;
}

void fillZeros(pathMember* pm)
{
	for (int i = 0; i < NUM_PATH_MEMBERS; i++)
	{
		for (int j = 0; j < MAX_PATH_MEMBER_LEN; j++)
		{
			(*pm)[i][j] = 0;
		}
	}
}

int getLastPathMember(pathMember* pm)
{
	int ind = 0;
	while ((*pm)[ind][0] != 0)
	{
		ind++;
	}
	return ind-1;
}

int getFileFromClusters(char* fileName, uint_16* clusters, FILE* fileSave)
{
	int indexClustersSource = 0;
	while (clusters[indexClustersSource] != 0) //run on the clusters
	{
		//run 32 times (the number of sectors in cluster)
		for (int i = 0; i < 32; i++)
		{
			char* buffDir = pmmngr_alloc_block();

			//read cluster. cluster = 32 sectors
			ataRead(0, m_FS.m_baseSectors + (clusters[indexClustersSource]) * 32 + i, 2, buffDir);

			for (int j = 0; j < 512 / sizeof(FILE); j++)
			{
				FILE file;
				char* fPtr = (char*)&file;
				
				for (int i = 0; i < sizeof(FILE); i++)
				{
					*(fPtr + i) = buffDir[sizeof(FILE) * j + i];
				}
				
				if (strncmp(file.m_fileName, fileName, strlen(fileName)) == 0) //found the specific file
				{
					for (i = 0; i < sizeof(FILE); i++)
					{
						((char*)(fileSave))[i] = ((char*)(fPtr))[i];
					}

					pmmngr_free_block(buffDir);
					return 1; //success
				}
			}

			pmmngr_free_block(buffDir);
		}

		indexClustersSource++;
	}

	return 0;
}

void initFile(char* fileName, char* ext, uint_16 firstCluster, bool isDir, FILE* file)
{
	//fill the buffer with zeros
	char* filePtr = (char*)file;
	for (int i = 0; i < sizeof(FILE); i++)
	{
		*(filePtr + i) = 0;
	}

	strncpy(file->m_fileName, fileName, 8);
	setHMS(getHmsValue((uint_16)vamosDate.m_hour, (uint_16)vamosDate.m_minute, (uint_16)vamosDate.m_second), 
									vamosDate.m_second, file);
	setDMY(getDmyValue((uint_16)vamosDate.m_day, (uint_16)vamosDate.m_month, (uint_16)vamosDate.m_year-1980), file);
	strncpy(file->m_fileExt, ext, 3);
	initFirstCluster(file, firstCluster);
	file->m_fileAttr = isDir ?  ATTRB_SUB_DIRECTORY : 0;
}

int getClustersOfFile(char* fileName, uint_16* clustersSource, uint_16* clustersDest)
{

	FILE file;

	if (!getFileFromClusters(fileName, clustersSource, &file))
		return NULL;

	//check dir not allocated yet
	if (!isAllocated(&file))
		allocatedCluster(clustersSource, &file);

	if (!getClusters(getCluster(file.m_firstCluster), clustersDest))
		return NULL;

	return 1;
}

int getClusters(uint_16 firstCluster, uint_16* clustersDest)
{
	int currClusterIndex = 0;
	uint_16 currCluster = firstCluster;

	//need to allocate cluster
	if (firstCluster == UN_WRITE_FILE)
		return 0;

	for (int i = 0; i < 16; i++)
		clustersDest[i] = 0;

	char* buffFat = pmmngr_alloc_block();  // 8 sectors 

	for (int i = 0; i < 32; i++)
	{
		/*count sector * 256  = total_read.
		16 sectors but you get 8 sectors.
		the reason is that we read in sectors that equal to 256
		*/
		ataRead(0, 1 + i*8, 16, buffFat); //4096 
		int countIndexes = ((i + 1) * 1024 * 4) / 2; //divide by two because every fat entry is 16 bits
		while (currCluster != 0xFFF8)
		{
			//get fat entry
			clustersDest[currClusterIndex] = currCluster == 0 ? 1 : currCluster;
			int index = currCluster;
			uint_16* entry = buffFat;
			currCluster = buffFat[currCluster * 2] | (uint_16)buffFat[currCluster * 2 + 1] << 8;
			currClusterIndex++;
		}
	}
	
	pmmngr_free_block(buffFat);

	return 1;
}

int openFileHelper(pathMember* pm, int currIndex, uint_16* clusters, FILE* fileSave)
{
	uint_16 clustersDest[20];
	for (int i = 0; i < 20; i++) clustersDest[i] = 0;

	//check that next file/dir is exist
	if (getClustersOfFile((*pm)[currIndex], clusters, clustersDest))
	{
		uint_16 lenPathMembers = getLenPathMembers(pm);
		if (currIndex + 1 < lenPathMembers - 1) //change it to the len-1 of the path members 
			openFileHelper(pm, currIndex + 1, clustersDest, fileSave);
		else
		{
			int isFileExist = getFileFromClusters(lenPathMembers > 1 ? (*pm)[currIndex + 1] : (*pm)[0], lenPathMembers == 1 ? clusters : clustersDest, fileSave);
			if (isFileExist)
			{
				if (!isAllocated(fileSave))
					allocatedCluster(lenPathMembers == 1 ? clusters : clustersDest, fileSave);
			}
			return isFileExist;
		}
	}
	else
		//return 0 - failed to open file. 1 -  success to open file
		return getFileFromClusters((*pm)[currIndex + 1], clustersDest, fileSave);
}

int addFileToDir(uint_16* dirClusters, FILE* dir, FILE* file, bool overrideFile)
{
	int lastClusterInd = 0;
	while (dirClusters[lastClusterInd])
		lastClusterInd++;

	lastClusterInd--;
	
	FILE dirFiles[16] = {0}; // 512 / 16 = 32 (size of FILE) 
	char* f_ptr = dirFiles;
	char buff[512] = {0};
	ataRead(0, m_FS.m_baseSectors +  dirClusters[lastClusterInd] * 32, 2, buff); //need to read all 32 sectors not only one

	for (int i = 0; i < sizeof(FILE)*16; i++)
	{
		*(f_ptr + i) = buff[i];
	}


	//check if file already exist
	if (!overrideFile)
	{
		for (int i = 0; i < 16; i++)
		{
			if (strcmp(dirFiles[i].m_fileName, file->m_fileName) == 0)
			{
				return ErrorfileAlreadyExist;
			}
		}
	}

	int lastIndFile = 0;
	for (lastIndFile; lastIndFile < 16 && dirFiles[lastIndFile].m_fileName[0]; lastIndFile++);
	int i = 0;
	for (i = 0; i < 16 && strncmp(dirFiles[i].m_fileName, file->m_fileName, strlen(file->m_fileName)) != 0; i++);

	lastIndFile = i != 16 ? i : lastIndFile;

	for (i = 0; i < sizeof(FILE); i++)
	{
		((char*)(&dirFiles[lastIndFile]))[i] = ((char*)(file))[i];
	}

	ataWrite(0, m_FS.m_baseSectors + (dirClusters[lastClusterInd]) * 32, sizeof(FILE)*16, (char*)dirFiles);
	return 0;
}

bool isAllocated(FILE* file)
{
	uint_16 firstCluster = file->m_firstCluster[1] |  (file->m_firstCluster[0] << 8);
	return firstCluster != UN_WRITE_FILE;
}

void allocatedCluster(uint_16* clustersSource, FILE* file)
{
	int indexClustersSource = 0;
	while (clustersSource[indexClustersSource] != 0) //run on the clusters
	{
		//run 32 times (the number of sectors in cluster)
		for (int i = 0; i < 32; i++)
		{
			char* buffDir = pmmngr_alloc_block();
			//read cluster. cluster = 32 sectors
			ataRead(0, m_FS.m_baseSectors + (clustersSource[indexClustersSource]) * 32 + i, 2, buffDir);

			FILE dirFiles[16] = {0};
			char* dirPtr = (char*)dirFiles;
			for (int k = 0; k < 16*sizeof(FILE); k++)
			{
				*(dirPtr + k) = buffDir[k];
			}

			for (int j = 0; j < 16; j++)
			{
				if (strncmp(dirFiles[j].m_fileName, file->m_fileName, strlen(file->m_fileName)) == 0
					&& strlen(dirFiles[j].m_fileName) == strlen(file->m_fileName))
				{
					uint_16 fileCluster = getFreeCluster();
					initFirstCluster(file, fileCluster);

					for (int k = 0; k < sizeof(FILE); k++)
					{
						((char*)(&dirFiles[j]))[k] = ((char*)(file))[k];
					}
					ataWrite(0, m_FS.m_baseSectors + (clustersSource[indexClustersSource]) * 32 + i, 16 * sizeof(FILE), (char*)dirPtr);
				}
			}
			pmmngr_free_block(buffDir);
		}

		indexClustersSource++;
	}
}

uint_16 getFreeCluster()
{
	uint_16* indCluster = 0;
	uint_16 ind = 0;
	char* fatBuff = pmmngr_alloc_block();
	ataRead(0, 1, 16, fatBuff);
	indCluster = fatBuff;

	while (*indCluster != 0)
	{
		ind++;
		indCluster++;
	}

	*indCluster = 0xFFF8;
	ataWrite(0, 1, 16 * 256, fatBuff);
	pmmngr_free_block(fatBuff);
	return ind;
}

void initFirstCluster(FILE* file, uint_16 firstCluster)
{
	file->m_firstCluster[0] = firstCluster >> 8;
	file->m_firstCluster[1] = firstCluster;
}

uint_16 getCluster(uint_8* cluster)
{
	return cluster[1] | (cluster[0] << 8);
}

uint_16 getLenPathMembers(pathMember* pm)
{
	return getLastPathMember(pm) + 1;
}

void getFilesInDir(uint_16* clusters, FILE* dirFiles)
{
	int indexClustersSource = 0;
	char* dirPtr = (char*)dirFiles;

	while (clusters[indexClustersSource] != 0) //run on the clusters
	{
		//run 32 times (the number of sectors in cluster)
		for (int i = 0; i < 32; i++)
		{
			char* buffDir = pmmngr_alloc_block();

			//read cluster. cluster = 32 sectors
			ataRead(0, m_FS.m_baseSectors + (clusters[indexClustersSource]) * 32 + i, 2, buffDir);

			for (int k = 0; k < sizeof(FILE)*16; k++)
			{
				*(dirPtr + indexClustersSource*32 + k) = buffDir[k];
			}

			if (dirFiles[indexClustersSource * 32 * 16 + 15].m_fileName[0] == 0) //reach to end of dir
			{
				pmmngr_free_block(buffDir);
				return;
			}

			pmmngr_free_block(buffDir);
			
		}

		indexClustersSource++;
	}
}

bool isDir(FILE file)
{
	return file.m_fileAttr & ATTRB_SUB_DIRECTORY;
}

int getNumClusters(uint_16* clusters)
{
	int len = 0;;
	for (int i = 0; clusters[i] != 0; i++)
		len++;

	return len;
}
