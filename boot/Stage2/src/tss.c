#include "tss.h"

tssEntry _tss;

void createTss(uint_32 indexGdt, uint_16 ss0, uint_32 esp0)
{
	uint_32 base = (uint_32)&_tss;
	uint_32 limit = base + sizeof(_tss);


	memset(&_tss, 0, sizeof(_tss));
	
	gdt_set_descriptor(indexGdt, base, limit, I86_GDT_DESC_ACCESS | I86_GDT_DESC_EXEC_CODE | I86_GDT_DESC_DPL | I86_GDT_DESC_MEMORY, 
		0x00);


	_tss.ss0 = ss0;  // Set the kernel stack segment.
	_tss.esp0 = esp0; // Set the kernel stack pointer.

	// Here we set the cs, ss, ds, es, fs and gs entries in the TSS. These specify what
	// segments should be loaded when the processor switches to kernel mode. Therefore
	// they are just our normal kernel code/data segments - 0x08 and 0x10 respectively,
	// but with the last two bits set, making 0x0b and 0x13. The setting of these bits
	// sets the RPL (requested privilege level) to 3, meaning that this TSS can be used
	// to switch to kernel mode from ring 3.
	_tss.cs = 0x0b;
	_tss.ss = _tss.ds = _tss.es = _tss.fs = _tss.gs = 0x13;
}