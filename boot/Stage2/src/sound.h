#ifndef SOUND_H
#define SOUND_H

#include "Typedefs.h"
#include "inputOutput.h"

void io_wait(void);
void play_sound(uint_32 nFrequence);
void nosound();
void beep();

#endif // !SOUND_H