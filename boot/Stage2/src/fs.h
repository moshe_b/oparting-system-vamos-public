#ifndef FS_H
#define FS_H

//includes
#include "string.h"
#include "vga.h"
#include "disk.h"
#include "Typedefs.h"
#include "physicalMemory.h"
#include "timeDate.h"

//defines/contants
#define NUM_PATH_MEMBERS 20
#define MAX_PATH_MEMBER_LEN 50
#define MAX_LEN_PATH (NUM_PATH_MEMBERS * MAX_PATH_MEMBER_LEN)

#define UN_WRITE_FILE 31

/**** macros ****/
#define ATTRB_READ_ONLY 1
#define ATTRB_HIDDEN 2
#define ATTRB_SYSTEM 4
#define ATTRB_VOLUME 8
#define ATTRB_SUB_DIRECTORY 16
#define ATTRB_ARCHIVE 32
#define ATTRB_DEVICE 64

//date/time strcuts


//structs

typedef struct
{
	uint_8 m_fileName[8];
	uint_8 m_fileExt[3];
	uint_8 m_fileAttr;
	uint_8 m_unused;
	uint_8 m_createTimeSeconds;
	uint_8 m_createTimeHMS[2];
	uint_8 m_createTimeDMY[2];
	uint_8 m_lastAccessDate[2];
	uint_8 m_numFilesInDir[2];
	uint_8 m_lastModifyTime[2];
	uint_8 m_lastModifyDate[2];
	uint_8 m_firstCluster[2];
	uint_8 m_fileSize[4];
}FILE;


typedef FILE DIR;

typedef struct
{
	uint_16 m_bytesPerSector;
	uint_16 m_sectorsPerCluster;
	uint_16 m_numFats;
	uint_16 m_sectorPerFat;
	uint_16 m_numReservedSectors;
}OEM;

typedef struct
{
	OEM m_fsOEM;
	FILE m_rootDir;
	uint_32 m_baseSectors;
}FS;
//typedef 
typedef char pathMember[NUM_PATH_MEMBERS][MAX_PATH_MEMBER_LEN];

typedef enum {
	ErrorfileAlreadyExist=100,
}errorFileCodes;

void initFS();

//public functions

int createFile(char* path);

int createDirectory(char* path);

int openFile(char* path, FILE* fileSave);

int openDir(char* path, FILE* dirSave);

int listDir(char* path, FILE* dirFiles);

int readFile(char* path, char* bufferToSave);

int writeFile(char* path, char* bufferToWrite);

int deleteFile(char* path);

bool isPathExist(char* path);

bool isDir(FILE file);

void getDirPath(char* fullPath, char* dirPathSave);

void getFileName(char* fullPath, char* fileName);

void recursive_print(char* path, char* prefix);

void initFile(char* fileName, char* ext, uint_16 firstCluster, bool isDir, FILE* file);

void getCreateDateFile(FILE file, DATE* fileDate);

//private functions (static)
static int createFileDir(char* path, bool isDirectory);

static int divPath(char* path, pathMember* pm);

static void fillZeros(pathMember* pm);

static int getLastPathMember(pathMember* pm);

static int getFileFromClusters(char* fileName, uint_16* clusters, FILE* fileSave);

/*
get the clusters of file when we have his parent directory clusters (clusters source)
*/
static int getClustersOfFile(char* fileName, uint_16* clustersSource, uint_16* clustersDest);

int getClusters(uint_16 firstClusters, uint_16* clustersDest);

//recursive function
//static void createFileHelper(pathMember* pm, int pathMemberIndex, int* clusters);

static int openFileHelper(pathMember* pm, int currIndex, uint_16* clusters, FILE* fileSave);

int addFileToDir(uint_16* dirClusters, FILE* dir, FILE* file, bool overrideFile);

//helper functions
static bool isAllocated(FILE* file);

void allocatedCluster(uint_16* clustersSource, FILE* file);

uint_16 getFreeCluster();

void initFirstCluster(FILE* file, uint_16 firstCluster);

uint_16 getCluster(uint_8* cluster);

static uint_16 getLenPathMembers(pathMember* pm);

static void getFilesInDir(uint_16* clusters, FILE* dirFiles);

static int getNumClusters(uint_16* clusters);

static int readFileHelper(char* path, char* bufferToSave, int lenToRead);


//time helper functions

static uint_16 getHmsValue(uint_16 hour, uint_16 minute, uint_16 second);
static void setHMS(uint_16 hms, uint_8 seconds, FILE* file);
static uint_16 getHMSFromFile(FILE file);

static uint_16 getDmyValue(uint_16 daye, uint_16 month, uint_16 year);
static void setDMY(uint_16 dmy, FILE* file);
static uint_16 getDMYFromFile(FILE file);

#endif