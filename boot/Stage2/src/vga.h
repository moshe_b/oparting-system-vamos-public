#ifndef VGA_H
#define VGA_H

#include "vga_codes.h"
#include "Typedefs.h"
#include "va_list.h"
#include "stdarg.h"
#include "string.h"


//vga constants
#define VGA_MEMORY_ADDRESS (uint_8*)0xb8000
#define VGA_MEMORY_ADDRESS_UINT_16 (uint_16)0xb8000
#define BUFSIZE 2200
#define VGA_WIDTH 80  
#define VGA_HEIGHT 25
#define START_USER_VGA_INDEX (7*80 + 8)

//FS DEFINE
#define NUM_PATH_MEMBERS 20
#define MAX_PATH_MEMBER_LEN 50
#define MAX_LEN_PATH (NUM_PATH_MEMBERS * MAX_PATH_MEMBER_LEN)

#define NUM_COLORS 32

#define BochsConsolePrintChar(c) outb(0xe9, c)

typedef struct
{
	uint_8 m_forColor;
	uint_8 m_backColor;
}COLOR;

//functions
void setOsColor(uint_8 forColor, uint_8 backColor);
void changeScreenColor(uint_8 forColor, uint_8 backColor);
void printString(const char* str, uint_8 color);
void printInt(int num);
void colorScreen(unsigned long long clearColor);
uint_16 vga_entry(unsigned char ch, uint_8 fore_color, uint_8 back_color);
void init_vga(uint_8 fore_color, uint_8 back_color);
void clear_vga_buffer(uint_16** buffer, uint_8 fore_color, uint_8 back_color);
void newline();
void print_char(char ch);
void setCursorPosition(uint_16 pos);
void printBackspace(int sizeOfPrefix, bool hasPrefix, int offset);
void createVGAbuffer(char* VGAbuffer, int startOffset);
uint_16 positionFromCoords(uint_8 x, uint_8 y);
void screenScroll(int numberOfLines);
void screenScrollEnter(int numberOfLines);
int getLineSizeOfString(const char* str);
const char* hexToString(uint_8 value);
void printCharInLoc(char chr, uint_8 color);
void printMouse(char chr, uint_8 color);
void setLineIndex(uint_32 newLineIndex);

//printf
int printf(const char* format, ...);

#endif // !VGA_H

