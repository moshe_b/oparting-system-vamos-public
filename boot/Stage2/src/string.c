#include "string.h"

int isEmptyString(const char* s)
{
    int i = 0;
    for (i = 0; *(s + i) && (*(s + i) == '\n' || *(s + i) == '\r' || *(s + i) == ' '); i++);
    return *(s + i) == 0;
}


int strCmp(const char* s1, const char* s2)
{
    const unsigned char* p1 = (const unsigned char*)s1;
    const unsigned char* p2 = (const unsigned char*)s2;

    while (*p1 && *p1 == *p2) ++p1, ++p2;

    return (*p1 > *p2) - (*p2 > *p1);
}

uint_32 strlen(const char* str)
{
    uint_32 length = 0;
    while (str[length])
        length++;
    return length;
}

uint_32 digit_count(int num)
{
    uint_32 count = 0;
    if (num == 0)
        return 1;
    while (num > 0) {
        count++;
        num = num / 10;
    }
    return count;
}

void strcpy(char* dest, const char* source)
{
    for (int i = 0; i < strlen(source) && *(source + i); i++)
    {
        *(dest + i) = *(source + i);
    }
}


void swap(char* p1, char* p2)
{
    char temp = *p1;
    *p1 = *p2;
    *p2 = temp;
}

/* A utility function to reverse a string  */
void reverse(char str[], int length)
{
    int start = 0;
    int end = length - 1;
    while (start < end)
    {
        swap((str + start), (str + end));
        start++;
        end--;
    }
}
char* itoa(int num, char* str, int base)
{
    int i = 0;
    int isNegative = 0;

    /* Handle 0 explicitly, otherwise empty string is printed for 0 */
    if (num == 0)
    {
        str[i++] = '0';
        str[i] = '\0';
        return str;
    }

    // In standard itoa(), negative numbers are handled only with
    // base 10. Otherwise numbers are considered unsigned.
    if (num < 0 && base == 10)
    {
        isNegative = 1;
        num = -num;
    }

    // Process individual digits
    while (num != 0)
    {
        int rem = num % base;
        str[i++] = (rem > 9) ? (rem - 10) + 'a' : rem + '0';
        num = num / base;
    }

    // If number is negative, append '-'
    if (isNegative)
        str[i++] = '-';

    str[i] = '\0'; // Append string terminator

    // Reverse the string
    reverse(str, i);

    return str;
}

void* memset(void* s, int c, uint_32 count) {
    char* xs = (char*)s;

    while (count--)
        *xs++ = c;

    return s;
}

int find(char* s, char chr)
{
    for (int i = 0; i < strlen(s); i++)
    {
        if (s[i] == chr)
            return i;
    }

    return -1;//chr not exist in s
}

void strncpy(char* s1, char* s2, int n)
{
    for (int i = 0; i < strlen(s2) && i < n; i++)
        s1[i] = s2[i];
}

int isspace(char ch)
{
    return ch == ' ' || ch == '\t' || ch == '\n' || ch == '\v' || ch == '\f' || ch == '\r';
}

int findFirstNotSpace(char* str)
{
    int i = 0;
    for (; i < strlen(str) && isspace(str[i]); i++);
    return i == strlen(str) ? NPOS : i;
}


int strcmp(char* s1, char* s2)
{
    if (strlen(s1) != strlen(s2))
        return s2 - s1;
    return strncmp(s1, s2, strlen(s1)); //It doesn't matter what the n is at this point - they should be the same length anyways
}

int strncmp(char* s1, char* s2, uint_32 n)
{
    unsigned int count = 0;
    while (count < n)
    {
        if (s1[count] == s2[count])
        {
            if (s1[count] == '\0') //quit early because null-termination found
                return 0;
            else
                count++;
        }
        else
            return s1[count] - s2[count];
    }

    return 0;
}

int atoi(char* strNum)
{
    bool isNeg = strNum[0] == '-';
    int indStart = isNeg ? 1 : 0;

    int len = strlen(strNum);
    int num = 0;
    for (int i = indStart; i < len; i++)
    {
        if (strNum[i] >= '0' && strNum[i] <= '9')
        {
            num *= 10;
            num += (strNum[i] - '0');
        }
        else
            return -1;
    }

    return isNeg ? num * -1 : num;
}

void rtrim(char* str)
{
    for (int i = strlen(str) - 1; i > 0 && str[i] == ' '; i--)
    {
        str[i] = 0;
    }
}

