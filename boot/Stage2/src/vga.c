#include "vga.h"

uint_32 vga_index;
static uint_32 next_line_index = 1;
uint_8 g_fore_color = FORGROUND_WHITE, g_back_color = BACKGROUND_BLACK;
uint_16* vga_buffer;
char hexToStringOutput[128];
unsigned short cursorPosition;
COLOR m_OsColor;
int cPositionPrinting = -1;

uint_32 colors[NUM_COLORS + 1];

void setCursorPosition(uint_16 pos)
{
	outb(0x3D4, 0x0F);
	outb(0x3D5, (uint_8)(pos & 0xFF));
	outb(0x3D4, 0x0E);
	outb(0x3D5, (uint_8)((pos >> 8) & 0xFF));
	cursorPosition = pos;
}

uint_16 positionFromCoords(uint_8 x, uint_8 y)
{
	return y * VGA_WIDTH + x;
}

void colorScreen(unsigned long long clearColor)
{
	unsigned long long value = 0;
	value += clearColor << 8;
	value += clearColor << 24;
	value += clearColor << 40;
	value += clearColor << 56;

	for (unsigned long long* i = (unsigned long long*)VGA_MEMORY_ADDRESS; i < (unsigned long long*)(VGA_MEMORY_ADDRESS + 4000); i++)
	{
		*i = value;
	}
}

/*
16 bit video buffer elements(register ax)
8 bits(ah) higher :
  lower 4 bits - forec olor
  higher 4 bits - back color

8 bits(al) lower :
  8 bits : ASCII character to print
*/
uint_16 vga_entry(unsigned char ch, uint_8 fore_color, uint_8 back_color)
{
	uint_16 ax = 0;
	uint_8 ah = 0, al = 0;

	ah = back_color;
	ah <<= 4;
	ah |= fore_color;
	ax = ah;
	ax <<= 8;
	al = ch;
	ax |= al;

	return ax;
}

//initialize vga buffer
void init_vga(uint_8 fore_color, uint_8 back_color)
{
	vga_buffer = (uint_16*)VGA_MEMORY_ADDRESS;
	clear_vga_buffer(&vga_buffer, fore_color, back_color);
	g_fore_color = fore_color;
	g_back_color = back_color;
	vga_index = vga_index + START_USER_VGA_INDEX; // Starting Point of print User Shell 
	cursorPosition = 0;
	m_OsColor.m_backColor = BACKGROUND_BLACK;
	m_OsColor.m_forColor = FORGROUND_WHITE;

	for (int i = 0; i < 16; i++)
	{
		colors[i] = i;
	}

	for (int i = 16; i < NUM_COLORS; i++)
	{
		colors[i] = 0x10 * (i - 16 + 1);
	}
}

//clear video buffer array
void clear_vga_buffer(uint_16** buffer, uint_8 fore_color, uint_8 back_color)
{
	uint_32 i;
	for (i = 0; i < BUFSIZE; i++) {
		(*buffer)[i] = vga_entry(NULL, fore_color, back_color);
	}
	next_line_index = 1;
	vga_index = 0;
}

/*
increase vga_index by width of row(80)
*/
void newline()
{
	if (next_line_index >= 55)
	{
		next_line_index = 0;
		clear_vga_buffer(&vga_buffer, g_fore_color, g_back_color);
	}
	vga_index = 80 * next_line_index;
	next_line_index++;

	cursorPosition = vga_index;
	setCursorPosition(cursorPosition);
}

int checkLastLine()
{
	for (int i = 0; i < VGA_WIDTH; i++)
	{
		if (*(VGA_MEMORY_ADDRESS + (VGA_WIDTH * (VGA_HEIGHT - 1)) + i) != 0)
			return 1;
	}
	return 0;
}
//assign ascii character to video buffer
void print_char(char ch)
{

	int index = cPositionPrinting == -1 ? cursorPosition : cPositionPrinting;
	switch (ch)
	{
	case 10: //\n
		index += VGA_WIDTH;
		//	printf("new line1");
		break;
	case 13: //\r
		index -= cursorPosition % VGA_WIDTH;
		//printf("carrgie return");
		break;
	default:
	{
		*(VGA_MEMORY_ADDRESS + index * 2) = ch; //double by two because in vga format is character and is font formating
		*(VGA_MEMORY_ADDRESS + index * 2 + 1) = m_OsColor.m_backColor | m_OsColor.m_forColor;
		index++;
	}
	}

	if (cPositionPrinting == -1)
		setCursorPosition(index);
	else
		cPositionPrinting = index;
}

void printBackspace(int sizeOfPrefix, bool hasPrefix, int offset)
{
	char ch = ' ';

	bool isValidBackSpace = true;

	if (hasPrefix)
	{
		if (cursorPosition % 80 - 4 > sizeOfPrefix)
			isValidBackSpace = true;
		else
			isValidBackSpace = false;
	}

	if (cursorPosition - 1 <= offset)
		isValidBackSpace = false;

	if (isValidBackSpace)
	{
		cursorPosition -= 2;
		vga_buffer[cursorPosition] = vga_entry(ch, g_fore_color, g_back_color);
		setCursorPosition(cursorPosition);
	}
}
void createVGAbuffer(char* VGAbuffer, int startOffset)
{
	uint_16* vga_buffer = (uint_16*)VGA_MEMORY_ADDRESS;
	uint_32 index, j = 0;
	if (startOffset > VGA_HEIGHT * VGA_WIDTH * 2 || startOffset < 0)
		return;
	for (index = 0; index < VGA_HEIGHT * VGA_WIDTH; index++)
	{
		if ((*((VGA_MEMORY_ADDRESS + startOffset + index * 2))) == '\0')
		{
			VGAbuffer[j] = ' ';
		}
		else
		{
			VGAbuffer[j] = (*((VGA_MEMORY_ADDRESS + startOffset + index * 2)));
		}
		j++;
	}
}


int getLineSizeOfString(const char* str)
{
	int counter = 0;
	for (int i = 0; i < strlen(str); i++)
	{
		if (*(str + i) == '\n')
			counter++;
	}
	return counter;
}
void setOsColor(uint_8 forColor, uint_8 backColor)
{
	m_OsColor.m_backColor = backColor;
	m_OsColor.m_forColor = forColor;
}
void changeScreenColor(uint_8 forColor, uint_8 backColor)
{
	m_OsColor.m_forColor = forColor;
	m_OsColor.m_backColor = backColor;

	uint_16 cursorPositionTemp = cursorPosition;
	setCursorPosition(0);

	for (int i = 0; i < 80 * 25; i++)
	{
		print_char(*(VGA_MEMORY_ADDRESS + i * 2));
	}

	setCursorPosition(cursorPositionTemp);

}
//print string by calling print_char
void printString(const char* str, uint_8 color)
{

	if ((cursorPosition / VGA_WIDTH) >= VGA_HEIGHT - (getLineSizeOfString(str) + 3))
	{
		screenScrollEnter(getLineSizeOfString(str) + 2);
	}
	uint_8* charPtr = (uint_8*)str;
	uint_16 index = cursorPosition; //for moving the cursor with the text
	int i = 0;
	while (*charPtr != 0)
	{
		print_char(*charPtr);
		charPtr++;
		i++;
	}



}

//print int by converting it into string
//& then printing string
void printInt(int num)
{
	char str_num[digit_count(num) + 1];
	itoa(num, str_num, 10);
	printString(str_num, g_back_color | g_fore_color);
}

void screenScrollEnter(int numberOfLines)
{
	for (int i = 0; i < numberOfLines; i++)
		screenScroll(i);

	cursorPosition -= VGA_WIDTH * numberOfLines;
	setCursorPosition(cursorPosition);
}

void screenScroll(int numberOfLines)
{
	uint_16* i = 0;
	for (i = (uint_16*)VGA_MEMORY_ADDRESS; i < (uint_16*)(VGA_MEMORY_ADDRESS + 4000); i++)
	{
		*i = *(i + 80);
	}
	for (i; i < (uint_16*)(VGA_MEMORY_ADDRESS + 4000); i++)
	{
		*i = vga_entry(NULL, g_fore_color, g_back_color);
	}
}

const char* hexToString(uint_8 value) {
	uint_8* valPtr = &value;
	uint_8* ptr;
	uint_8 temp;
	uint_8 size = (sizeof(uint_8) * 2 - 1);
	uint_8 i;
	for (i = 0; i < size; i++)
	{
		ptr = ((uint_8*)valPtr + i);
		temp = ((*ptr & 0xF0) >> 4);
		hexToStringOutput[size - (i * 2 + 1)] = temp + (temp > 9 ? 55 : 48);
		temp = ((*ptr & 0x0F));
		hexToStringOutput[size - (i * 2 + 0)] = temp + (temp > 9 ? 55 : 48);
	}

	hexToStringOutput[size + 1] = 0;
	return hexToStringOutput;
}

int printf(const char* format, ...)
{
	int zeroIndex = -1;
	int numZeros = -1;
	va_list ap;
	va_start(ap, format);

	int i = 0;
	for (i = 0; i < strlen(format); i++) // @TODO ->
	{
		if (format[i] == '%')
		{
			switch (format[i + 1])
			{
			case 'd':
			{
				uint_32 value = va_arg(ap, uint_32);
				char strNum[100] = { 0 };
				itoa(value, strNum, 10);

				if (numZeros != -1)
				{
					int numRealZeros = numZeros - strlen(strNum);
					for (int i = 0; i < numRealZeros; i++)
						print_char('0');
					numZeros = -1;
				}

				printInt(value);
				i++;
				break;
			}
			case 'c':
			{
				print_char(va_arg(ap, char));
				i++;
				break;
			}
			case 's':
			{
				int* c = (int*)va_arg(ap, char*);
				char str[512] = { 0 };
				strcpy(str, (const char*)c);
				printString(str, BACKGROUND_BLACK | FORGROUND_WHITE);
				i++;
				break;
			}
			case 'x':
			case 'X':
			{
				int hexValue = va_arg(ap, int);
				char str[32] = { 0 };
				itoa(hexValue, str, 16);
				printString(str, BACKGROUND_BLACK | FORGROUND_WHITE);
				i++;		// go to next character
				break;
			}
			case '0':
			{
				zeroIndex = i+1;
				i += 2;

				// 0 - i+1
				// 2 - i+2
				// d - i+3

				if (format[i] >= '1' && format[i] <= '9')
				{
					numZeros = format[i] - '0';
					zeroIndex = -1;
				}
				
				i++;
				if (format[i] == 'd')
				{
					uint_32 value = va_arg(ap, uint_32);
					char strNum[100] = { 0 };
					itoa(value, strNum, 10);

					if (numZeros != -1)
					{
						int numRealZeros = numZeros - strlen(strNum);
						for (int i = 0; i < numRealZeros; i++)
							print_char('0');
						numZeros = -1;
					}

					printInt(value);
				}

				break;

			}
			default:
			{
				printInt(i);
				va_end(ap);
				return -1; // wrong sytax
			}
			}
		}
		else
		{
			print_char(format[i]);
		}
	}

	va_end(ap);
	return i;
}


extern Point prevMousePos;
extern Point mousePos;
extern char prevChars[5];

void printMouse(char chr, uint_8 color)
{
	uint_32 pos = positionFromCoords(mousePos.x, mousePos.y);
	//for not override existing data
	uint_32 prevPos = positionFromCoords(prevMousePos.x, prevMousePos.y);
	*(VGA_MEMORY_ADDRESS + prevPos * 2) = prevChars[0]; //double by two because in vga format is character and is font formating
	*(VGA_MEMORY_ADDRESS + prevPos * 2 + 1) = color;

	*(VGA_MEMORY_ADDRESS + (prevPos + 1) * 2) = prevChars[1]; //double by two because in vga format is character and is font formating
	*(VGA_MEMORY_ADDRESS + (prevPos + 1) * 2 + 1) = color;

	*(VGA_MEMORY_ADDRESS + (prevPos + 80) * 2) = prevChars[2]; //double by two because in vga format is character and is font formating
	*(VGA_MEMORY_ADDRESS + (prevPos + 80) * 2 + 1) = color;

	*(VGA_MEMORY_ADDRESS + (prevPos + 81) * 2) = prevChars[3]; //double by two because in vga format is character and is font formating
	*(VGA_MEMORY_ADDRESS + (prevPos + 81) * 2 + 1) = color;

	prevChars[0] = *(VGA_MEMORY_ADDRESS + pos * 2);
	prevChars[1] = *(VGA_MEMORY_ADDRESS + (pos + 1) * 2);
	prevChars[2] = *(VGA_MEMORY_ADDRESS + (pos + 80) * 2);
	prevChars[3] = *(VGA_MEMORY_ADDRESS + (pos + 81) * 2);


	*(VGA_MEMORY_ADDRESS + pos * 2) = chr; //double by two because in vga format is character and is font formating
	*(VGA_MEMORY_ADDRESS + pos * 2 + 1) = color;

	*(VGA_MEMORY_ADDRESS + (pos + 1) * 2) = chr; //double by two because in vga format is character and is font formating
	*(VGA_MEMORY_ADDRESS + (pos + 1) * 2 + 1) = color;

	*(VGA_MEMORY_ADDRESS + (pos + 80) * 2) = chr; //double by two because in vga format is character and is font formating
	*(VGA_MEMORY_ADDRESS + (pos + 80) * 2 + 1) = color;

	*(VGA_MEMORY_ADDRESS + (pos + 81) * 2) = chr; //double by two because in vga format is character and is font formating
	*(VGA_MEMORY_ADDRESS + (pos + 81) * 2 + 1) = color;


}

void setLineIndex(uint_32 newLineIndex)
{
	next_line_index = newLineIndex;
}

void printCharInLoc(char chr, uint_8 color)
{
	uint_32 pos = positionFromCoords(mousePos.x, mousePos.y);
	pos -= 80;
	*(VGA_MEMORY_ADDRESS + pos * 2) = chr; //double by two because in vga format is character and is font formating
	*(VGA_MEMORY_ADDRESS + pos * 2 + 1) = color;
}



