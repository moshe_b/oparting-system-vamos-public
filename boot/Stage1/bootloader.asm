bits    16                          ; We are still in 16 bit Real Mode
 
[org     0x7c00]                      ; We are loaded by BIOS at 0x7C00
 
start:          jmp main                  ; jump over OEM block

PROGRAM_SPACE equ 0x8000 ; the address to jump after read sectors of stage2
;*************************************************;
;   OEM Parameter block / BIOS Parameter Block
;*************************************************;
 

bpbBytesPerSector:      DW 512
bpbSectorsPerCluster:   DB 32 
bpbReservedSectors:     DW 1 ; the bootloader sector (first sector on the disk)
bpbNumberOfFATs:    DB 2
bpbRootEntries:     DW 65536 ; (should be 65536 = 2^16) number of root entries(32 bytes per entry). It's equale to 4096 sectors or 128 clusters
bpbTotalSectors:    DW 2096640
bpbMedia:           DB 0xF0
bpbSectorsPerFAT:   DW 256 ; (256 * 512 * 8) / 16 == number of total clusters 
bpbSectorsPerTrack:     DW 63 ; from bochs geometry
bpbHeadsPerCylinder:    DW 16 ; from bochs geometry. 
                               ;bpbHeadsPerCylinder simply represents that there are 16 heads that represents a cylinder
bpbHiddenSectors:       DD 0
bpbTotalSectorsBig:     DD 0
bsDriveNumber:          DB 0x80 ; 0x80 is hard drive number. 0 is for floppy drives
bsUnused:           DB 0
bsExtBootSignature:     DB 0x29
bsSerialNumber:         DD 0xa0a1a2a3
bsVolumeLabel:          DB "MOS DRIVE  "
bsFileSystem:           DB "FAT16   "


; data variables 
absoluteSector db 0x00
absoluteHead   db 0x00
absoluteTrack  db 0x00
     
datasector  dw 0x0000
cluster     dw 0x0000
ImageName   db "STAGE2  SYS"
msgLoading  db "Loading Boot Image ", 0x0D, 0x0A, 0x00
msgCRLF     db 0x0D, 0x0A, 0x00
msgProgress db ".", 0x00
msgFailure  db 0x0D, 0x0A, "ERROR : Press Any Key to Reboot", 0x0A, 0x00
saveVal dd 65536
message3 db "ZERO3 ", 0x0D, 0x0A, 0x00
message db "Finished ", 0x0D, 0x0A, 0x00
;***************************************
;  Prints a string
;  DS=>SI: 0 terminated string
;***************************************
 
Print:
            lodsb                   ; load next byte from string from SI to AL
           ;  mov al, [si] 
            or          al, al      ; Does AL=0?
            jz          PrintDone   ; Yep, null terminator found-bail out
           ; inc si
            mov         ah, 0eh ; Nope-Print the character
            int         10h
            ;inc si
            jmp         Print       ; Repeat until null terminator found
PrintDone:
            ret                 ; we are done, so return

PrintNum:
    xor dx, dx
    xor cx, cx
    mov cx, 10
    div cx
    add dx, '0'
    push ax
    mov al, dl
    mov ah, 0eh
    int 10h
    xor dx, dx
    pop ax
    cmp ax, 0
    jg PrintNum
    pop ax
    ret

    
 
;************************************************;
; Reads a series of sectors
; CX=>Number of sectors to read
; AX=>Starting sector
; ES:BX=>Buffer to read to
;************************************************;

ReadSectors:
     .MAIN
          mov     di, 0x0005                          ; five retries for error
     .SECTORLOOP
          push    ax
          push    bx
          push    cx
         ; call    LBACHS                              ; convert starting sector to CHS
          mov     ah, 0x02                            ; BIOS read sector
          mov     al, 0x01                           ; read one sector
          ; mov     ch, BYTE [absoluteTrack]            ; track
          ; mov     cl, BYTE [absoluteSector]           ; sector
          ; mov     dh, BYTE [absoluteHead]             ; head
		  mov     ch, 0x00         ; track
          mov     cl, 0x02          ; sector
          mov     dh, 0x00
          ;mov     dl, BYTE [bsDriveNumber]            ; drive
          mov dl, 0x80
          int     0x13                                ; invoke BIOS
          jnc     .SUCCESS                            ; test for read error
          xor     ax, ax                              ; BIOS reset disk
          int     0x13                                ; invoke BIOS
          dec     di                                  ; decrement error counter
          pop     cx
          pop     bx
          pop     ax
          jnz     .SECTORLOOP                         ; attempt to read again
          int     0x18
     .SUCCESS
          pop     cx
          pop     bx
          pop     ax
         ; add     bx, WORD [bpbBytesPerSector]        ; queue next buffer
          add bx, 512 ; need to delete
		  inc     ax                                  ; queue next sector
          loop    .MAIN                               ; read next sector
          ret

;************************************************;
;Convert CHS to LBA
;LBA = (cluster - 2) * sectors per cluster
;************************************************;

ClusterLBA:
          sub     ax, 0x0002                          ; zero base cluster number
          xor     cx, cx
          mov     cl, BYTE [bpbSectorsPerCluster]     ; convert byte to word
          mul     cx
          add     ax, WORD [datasector]               ; base data sector
          ret
     
;************************************************;
; Convert LBA to CHS
; AX=>LBA Address to convert
;
; absolute sector = (logical sector / sectors per track) + 1
; absolute head   = (logical sector / sectors per track) MOD number of heads
; absolute track  = logical sector / (sectors per track * number of heads)
;
;************************************************;

LBACHS:
          xor     dx, dx                              ; prepare dx:ax for operation
          div     WORD [bpbSectorsPerTrack]           ; calculate  73
          inc     dl                                  ; adjust for sector 0 
          mov     BYTE [absoluteSector], dl  ; 11
          xor     dx, dx                              ; prepare dx:ax for operation
          div     WORD [bpbHeadsPerCylinder]   ;bpbHeadsPerCylinder = 16       ; calculate  
          mov     BYTE [absoluteHead], dl ; 9
          mov     BYTE [absoluteTrack], al ; 4
          ret

;*********************************************
;   Bootloader Entry Point
;*********************************************
 
main:
  ;  mov     ax, 0x07C0                ; setup registers to point to our segment
   ; mov     ds, ax
   ; mov     es, ax
   ; mov     fs, ax
   ; mov     gs, ax
          
    ;----------------------------------------------------
    ; create stack
    ;----------------------------------------------------
    cli 
    mov     ax, 0x0000                ; set the stack
    mov     ss, ax
    mov     sp, 0xFFFF
    sti                       ; restore interrupts     
          
    mov ah, 0x00
    mov dl, 0x80
    int 0x13
    
	;cld
    xor cx, cx
    mov cx, 2
    mov bx, PROGRAM_SPACE              ; we are going to read sector into address 0x8000:0
	xor ax,ax
    mov es, ax
    xor ax, ax
    mov ax, 1000
	call    LBACHS                              ; convert starting sector to CHS
    mov     ah, 0x02                            ; BIOS read sector
    mov     al, 10                          ; read one sector
    mov     ch, BYTE [absoluteTrack]            ; track
	mov     cl, BYTE [absoluteSector]            ; sector
	mov     dh, BYTE [absoluteHead]             ; head
    mov dl, 0x80
    int     0x13 
	
	; mov ax, 0x200
	; mov bx, 60
	; mul bx
	; mov bx, ax ;  store the amount of bytes we read in last read

	; add bx, PROGRAM_SPACE ; bx = 0x8000 + 62*512 
	; xor ax, ax
	; mov es, ax
	
	; mov ax, 1000
	; add ax, 50 ; ax - the sector to read  - sector number 1000 + 62
	; call    LBACHS                              ; convert starting sector to CHS
    ; mov     ah, 0x02                            ; BIOS read sector
    ; mov     al, 20                             ; read one sector
    ; mov     ch, BYTE [absoluteTrack]            ; track
	; mov     cl, BYTE [absoluteSector]           ; sector
	; mov     dh, BYTE [absoluteHead]             ; head
    ; mov dl, 0x80
    ; int     0x13 

	; mov bx, 0xb8002
	; mov cx, 0x0000
	; mov es, cx
	; mov al, [0x8000]
	; mov ah,0Eh  ; bios function 0eh to teletype character
    ; mov cx,1    ; cx is the number of times the character has to be printed. 
    ; int 10h
	
	; jmp $
    push 0x8000
	ret     
    

     ; ;----------------------------------------------------
     ; ; Display loading message
     ; ;----------------------------------------------------
     
          
     ; ;----------------------------------------------------
     ; ; Load root directory table
     ; ;----------------------------------------------------

     ; LOAD_ROOT:
     
     ; ; compute size of root directory and store in "cx"
         
     ; ;;;;;    xor     cx, cx
     ; ;;;;;    xor     dx, dx
     ; ;;;;;    mov     ax, 0x0020                           ; 32 byte directory entry
         ; ; mul     WORD [bpbRootEntries]                ; total size of directory
         ; ; div     WORD [bpbBytesPerSector]             ; sectors used by directory
          
          ; mov ax, 4096
          ; xchg    ax, cx
          
     ; ; compute location of root directory and store in "ax"
     
     ; ;;;;;    mov     al, BYTE [bpbNumberOfFATs]            ; number of FATs
     ; ;;;;;     mul     WORD [bpbSectorsPerFAT]               ; sectors used by FATs
     ; ;;;;     add     ax, WORD [bpbReservedSectors]         ; adjust for bootsector
     ; ;;;        mov     WORD [datasector], ax                 ; base of root directory
     ; ;;;    add     WORD [datasector], cx
         ; ; mov ax, [datasector]
         ; ; push ax
         ; ; call PrintNum
     ; ; read root directory into memory (7C00:0200)
     
     ; ;;;     mov bx, 0x0200                            ; copy root dir above bootcode
     ; ;;;;    mov cx, 1
     ; ;;;     call ReadSectors
          
       ; ;   mov si, message
      ; ;    call Print
           ; ;----------------------------------------------------
     ; ; Find stage 2
     ; ;----------------------------------------------------

     ; ; browse root directory for binary image
          ; mov     cx, WORD [bpbRootEntries]             ; load loop counter
        ; ;  mov cx, 5
        ; ;  mov ax, 126
        ; ; push ax
        ; ;  call PrintNum
          ; mov     di, 0x0200                            ; locate second root entry
          ; mov cx, 10 
     ; .LOOP:
          ; push    cx
          ; mov     cx, 0x000B                            ; eleven character name
          ; mov     si, ImageName                         ; image name to find
          ; push    di
     ; rep  cmpsb                                         ; test for entry match
          ; pop     di
          ; je      LOAD_FAT
          ; pop     cx
          ; add     di, 0x0020                            ; queue next directory entry
       ; ;   mov si, message3
       ; ;  call Print
          ; loop    .LOOP
          ; jmp     FAILURE
          ; ; after two iteretions        
     ; ;----------------------------------------------------
     ; ; Load FAT
     ; ;----------------------------------------------------

     ; LOAD_FAT:
    
      ; ; mov si, di
      ; ;call Print
       ; ;mov ax, cx
        ; ;add ax, 20
       ; ; push ax
       ; ; call PrintNum
     ; ; save starting cluster of boot image
     ; ;mov si, di
     ; ;    call Print
     ; ;    mov     si, msgCRLF
     ; ;    call    Print
          ; mov     dx, WORD [di + 0x001A]
          ; mov     WORD [cluster], dx                  ; file's first cluster
        ; ;  mov ax, dx
         ; ; push ax
         ; ; call PrintNum
     ; ; compute size of FAT and store in "cx"
     
          ; xor     ax, ax
          ; ;mov     al, BYTE [bpbNumberOfFATs]          ; number of FATs
          ; ;mul     WORD [bpbSectorsPerFAT]             ; sectors used by FATs
          ; ;mov     cx, ax
          ; xor cx, cx
          ; mov cx, 1 ; read only one sector from the fat

     ; ; compute location of FAT and store in "ax"

           ; add     ax, WORD [bpbReservedSectors]       ; adjust for bootsector
          
     ; ; read FAT into memory (7C00:0200)
          
          ; ;mov ax, dx
          ; ; push ax
          ; ;call PrintNum
          ; mov     bx, 0x0200                          ; copy FAT above bootcode
          ; call    ReadSectors
     ; ; read image file into memory (0050:0000)
     
          ; mov     ax, 0x0050
          ; mov     es, ax                              ; destination for image
          ; mov     bx, 0x0000                          ; destination for image
          ; push    bx
        
     ; ;----------------------------------------------------
     ; ; Load Stage 2
     ; ;----------------------------------------------------

     ; LOAD_IMAGE:
        ; ;mov dx, 1 
         ; ;  L:
  ; ;        push dx
          ; mov     ax, WORD [cluster]                  ; cluster to read
          ; pop     bx                                  ; buffer to read into
          ; call    ClusterLBA                          ; convert cluster to LBA
          ; ;sub ax, dx
          ; ;inc dx
         ; ; push ax
         ; ; call PrintNum
          ; ;xor     cx, cx
          ; ; mov     cl, BYTE [bpbSectorsPerCluster]     ; sectors to read
          ; xor cx, cx
          ; mov cl, 1
          ; call    ReadSectors
          ; pop dx
       ; ;   mov ax, [bx]
          
          ; ;;;;;;;check if we reach to stage2
          ; mov ax, 0x0050
          ; mov es, ax
          ; mov bx, 0x0000
          ; mov ax, [es:bx]
          ; ;cmp ax, 0
          ; ;je L
          ; ;mov si, message3
          ; ;push ax
         ; ; call PrintNum
          ; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
          
          
          ; push    bx
     ; ; compute next cluster
     
          ; mov     ax, WORD [cluster]                  ; identify current cluster
          ; mov     cx, ax                              ; copy current cluster
          ; mov     dx, ax                              ; copy current cluster
          ; shr     dx, 0x10                          ; divide by two
          ; add     cx, dx                              ; sum for (3/2)
          ; mov     bx, 0x0200                          ; location of second entry in FAT in memory
          ; add     bx, cx                              ; index into FAT
          ; mov     dx, WORD [bx]                       ; read two bytes from FAT
      ; ;   .DONE:
     
      ; ;   mov     WORD [cluster], dx                  ; store new cluster
      ; ;   cmp     dx, 0x0FF0                          ; test for end of file
      ; ;   jb      LOAD_IMAGE
          
     ; DONE:
          ; push    WORD 0x0050
          ; push    WORD 0x0000
          ; ;jmp 0050:0000
          ; retf
          
     ; FAILURE:
     
          ; mov     si, msgFailure
          ; call    Print
          ; mov     ah, 0x00
          ; int     0x16                                ; await keypress
          ; int     0x19                                ; warm boot computer
          
          ; ending:  
         ; jmp $
 

        
times 510 - ($-$$) db 0                     ; We have to be 512 bytes. Clear the rest of the bytes with 0
 
dw 0xAA55                           ; Boot Signiture
 
; End of sector 1, beginning of sector 2 ---------------------------------
